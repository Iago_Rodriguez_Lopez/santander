# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180605071618) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "egresos", force: :cascade do |t|
    t.integer "empleado_id"
    t.string  "periodo"
    t.string  "periodo_anualizado"
    t.string  "tipo_egreso"
    t.string  "rut"
    t.string  "division"
    t.string  "gerencia"
    t.string  "piramide"
    t.string  "tipo_contrato"
    t.boolean "es_plantilla",       default: true
    t.string  "tipo_renta"
    t.string  "cargo"
    t.string  "estado_civil"
    t.integer "num_hijos"
  end

  create_table "empleados", force: :cascade do |t|
    t.string   "rut"
    t.string   "sexo"
    t.date     "fecha_ingreso_grupo"
    t.date     "fecha_nacimiento"
    t.date     "fecha_renuncia"
    t.string   "tipo_renuncia"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "motivo_renuncia"
    t.string   "nombre"
    t.string   "apellidos"
  end

  create_table "formaciones", force: :cascade do |t|
    t.string   "materia"
    t.string   "nombre_del_curso"
    t.integer  "horas"
    t.string   "periodo"
    t.string   "modalidad"
    t.integer  "empleado_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "rut"
    t.string   "periodo_anualizado"
    t.string   "asistencia"
  end

  create_table "licencias", force: :cascade do |t|
    t.date     "fecha"
    t.string   "glosa_tipo_licencia"
    t.integer  "empleado_id"
    t.date     "fecha_inicio"
    t.date     "fecha_termino"
    t.integer  "dias_totales"
    t.integer  "dias_corridos"
    t.integer  "dias_habiles"
    t.string   "periodo"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "rut"
    t.string   "periodo_anualizado"
    t.string   "tipo_licencia"
  end

  create_table "movimientos", force: :cascade do |t|
    t.integer  "empleado_id"
    t.string   "periodo"
    t.string   "tipo_movimiento"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "rut"
    t.string   "periodo_anualizado"
    t.integer  "posicion_origen_id"
  end

  create_table "posiciones", force: :cascade do |t|
    t.string   "piramide"
    t.string   "tipo_contrato"
    t.string   "glosa_contrato"
    t.string   "division"
    t.string   "gerencia"
    t.string   "area"
    t.string   "subarea"
    t.string   "periodo"
    t.integer  "empleado_id"
    t.datetime "created_at",                                                             null: false
    t.datetime "updated_at",                                                             null: false
    t.string   "rut"
    t.string   "nombre_empresa"
    t.boolean  "es_plantilla",                                            default: true
    t.string   "tipo_renta"
    t.string   "cargo"
    t.string   "estado_civil"
    t.integer  "num_hijos"
    t.decimal  "saldo_vacaciones",               precision: 10, scale: 2
    t.integer  "periodos_pendientes_vacaciones"
  end

  create_table "prezos", force: :cascade do |t|
    t.integer  "empleado_id"
    t.string   "periodo"
    t.decimal  "pre",         precision: 10, scale: 2
    t.string   "zona"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.string   "rut"
  end

  create_table "programa_talentos", force: :cascade do |t|
    t.integer  "empleado_id"
    t.string   "periodo"
    t.string   "nombre"
    t.string   "estado"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "rut"
  end

  create_table "retribuciones", force: :cascade do |t|
    t.integer  "empleado_id"
    t.string   "periodo"
    t.string   "periodo_anualizado"
    t.string   "tipo_movimiento"
    t.decimal  "resto_año",          precision: 10, scale: 2
    t.decimal  "anual",              precision: 10, scale: 2
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.string   "rut"
    t.decimal  "porcentaje",         precision: 5,  scale: 2
  end

  create_table "user_logs", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "period"
    t.string   "division"
    t.string   "gerencia"
    t.string   "source"
    t.integer  "user_id"
  end

  add_index "user_logs", ["user_id"], name: "index_user_logs_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.boolean  "invalid"
    t.integer  "company_id"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "name"
    t.integer  "role"
    t.string   "user_image"
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.integer  "invitations_count",      default: 0
    t.jsonb    "metadata",               default: {},    null: false
    t.integer  "rol",                    default: 0,     null: false
    t.string   "accesos",                default: [],                 array: true
    t.datetime "locked_at"
    t.string   "unlock_token"
    t.integer  "failed_attempts",        default: 0,     null: false
    t.string   "rut"
    t.boolean  "view_gerentes",          default: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
  add_index "users", ["invitations_count"], name: "index_users_on_invitations_count", using: :btree
  add_index "users", ["invited_by_id"], name: "index_users_on_invited_by_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["rut"], name: "index_users_on_rut", unique: true, using: :btree

  create_table "vacaciones", force: :cascade do |t|
    t.decimal  "consumidos",          precision: 10, scale: 2
    t.decimal  "saldo",               precision: 10, scale: 2
    t.integer  "empleado_id"
    t.string   "periodo"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.string   "rut"
    t.string   "periodo_anualizado"
    t.boolean  "dias_10"
    t.integer  "periodos_pendientes"
  end

  create_table "vacantes", force: :cascade do |t|
    t.string   "division"
    t.string   "gerencia"
    t.integer  "vacantes"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "empleados"
    t.string   "tipo_area"
    t.boolean  "activo"
    t.string   "periodo"
    t.integer  "division_order"
    t.integer  "gerencia_order"
  end

  add_foreign_key "user_logs", "users"
end
