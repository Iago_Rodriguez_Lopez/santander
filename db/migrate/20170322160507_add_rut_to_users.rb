class AddRutToUsers < ActiveRecord::Migration
  def change
    add_column :users, :rut, :string
    add_index :users, :rut, unique: true
  end
end
