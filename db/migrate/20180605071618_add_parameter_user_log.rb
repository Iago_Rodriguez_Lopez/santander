class AddParameterUserLog < ActiveRecord::Migration
  def change
    add_column :user_logs, :period, :string
    add_column :user_logs, :division, :string
    add_column :user_logs, :gerencia, :string
    add_column :user_logs, :source, :string
    add_reference :user_logs, :user, foreign_key: true, index: true
  end
end
