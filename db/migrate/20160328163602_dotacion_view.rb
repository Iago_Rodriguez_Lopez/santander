class DotacionView < ActiveRecord::Migration
  def up
    execute <<-SQL
      CREATE OR REPLACE VIEW dotacion_view AS


        SELECT
                    A.id,
                    A.rut,
                    A.sexo,
                    A.fecha_ingreso_grupo,
                    A.fecha_nacimiento,
                    A.fecha_renuncia,
                    A.tipo_renuncia,
                    P.division,
                    P.gerencia,
                    P.periodo,
                    P.piramide,
                    P.tipo_contrato,
                    P.nombre_empresa,
                    V.tipo_area,
                    P.es_plantilla,
                    PT.nombre   AS nombre_programa,
                    PT.estado   AS estado_programa,
                    PT.periodo AS periodo_programa

         FROM empleados A
    LEFT JOIN posiciones        P  ON P.empleado_id  = A.id
    LEFT JOIN vacantes        V ON P.gerencia = V.gerencia
    LEFT JOIN programa_talentos        PT  ON PT.empleado_id  = A.id AND (PT.periodo = P.periodo OR PT.periodo = null)
    ;
    SQL
  end

  def down
    execute 'DROP VIEW dotacion_view'
  end

end


