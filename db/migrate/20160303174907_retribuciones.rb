class Retribuciones < ActiveRecord::Migration
  def change
    create_table :retribuciones do |t|
      t.integer :empleado_id
      t.string :periodo
      t.string :periodo_anualizado
      t.string :tipo_movimiento
      t.decimal :resto_año, precision: 10, scale: 2
      t.decimal :anual, precision: 10, scale: 2

      t.timestamps null: false
    end
  end
end
