class CreatePrezos < ActiveRecord::Migration
  def change
    create_table :prezos do |t|
      t.integer :empleado_id
      t.string :periodo
      t.decimal :pre, precision: 10, scale: 2
      t.string :zona

      t.timestamps null: false
    end
  end
end
