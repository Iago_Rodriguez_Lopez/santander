class AddNameSurnameToRest < ActiveRecord::Migration

  def up

    if ActiveRecord::Base.connection.table_exists? 'egreso_view'
      execute 'DROP VIEW egreso_view'
    end
    if ActiveRecord::Base.connection.table_exists? 'retribucion_view'
      execute 'DROP VIEW retribucion_view'
    end
    if ActiveRecord::Base.connection.table_exists? 'movimiento_view'
      execute 'DROP VIEW movimiento_view'
    end
    if ActiveRecord::Base.connection.table_exists? 'formacion_view'
      execute 'DROP VIEW formacion_view'
    end





    execute <<-SQL
      CREATE OR REPLACE VIEW egreso_view AS


        SELECT
                    P.id,
                    A.rut,
                    A.sexo,
                    A.fecha_ingreso_grupo,
                    A.fecha_nacimiento,
                    A.nombre,
                    A.apellidos,
                    P.division,
                    P.gerencia,
                    P.periodo,
                    P.periodo_anualizado AS periodo_egreso,
                    P.piramide,
                    P.tipo_contrato,
                    P.es_plantilla,
                    P.tipo_renta,
                    P.cargo,
                    P.estado_civil,
                    P.num_hijos,
                    P.tipo_egreso

         FROM egresos P
    LEFT JOIN empleados        A  ON P.empleado_id  = A.id
    ;
    SQL



    execute <<-SQL
      CREATE OR REPLACE VIEW retribucion_view AS


        SELECT
                    M.id,
                    A.rut,
                    A.sexo,
                    A.fecha_ingreso_grupo,
                    A.fecha_nacimiento,
                    A.fecha_renuncia,
                    A.tipo_renuncia,
                    A.nombre,
                    A.apellidos,
                    P.division,
                    P.gerencia,
                    P.periodo,
                    P.piramide,
                    P.tipo_contrato,
                    P.nombre_empresa,
                    P.es_plantilla,
                    P.tipo_renta,
                    P.cargo,
                    P.estado_civil,
                    P.num_hijos,
                    M.periodo_anualizado AS periodo_retribucion,
                    M.tipo_movimiento,
                    M.anual AS retribucion_anual

         FROM retribuciones       M
    LEFT JOIN empleados         A   ON M.empleado_id = A.id
    LEFT JOIN posiciones        P   ON P.empleado_id  = A.id AND (M.periodo = P.periodo OR P.periodo = null)
    ;
    SQL



    execute <<-SQL
      CREATE OR REPLACE VIEW movimiento_view AS


        SELECT
                    M.id,
                    A.rut,
                    A.sexo,
                    A.fecha_ingreso_grupo,
                    A.fecha_nacimiento,
                    A.fecha_renuncia,
                    A.tipo_renuncia,
                    A.nombre,
                    A.apellidos,
                    P.division,
                    P.gerencia,
                    P.piramide,
                    P.tipo_contrato,
                    P.nombre_empresa,
                    P.es_plantilla,
                    P.tipo_renta,
                    P.cargo,
                    P.estado_civil,
                    P.num_hijos,
                    M.periodo,
                    M.periodo_anualizado AS periodo_movimiento,
                    M.tipo_movimiento
         FROM movimientos       M
    LEFT JOIN empleados         A   ON M.empleado_id = A.id
    LEFT JOIN posiciones        P   ON P.empleado_id  = A.id AND (M.periodo = P.periodo OR P.periodo = null)
    ;
    SQL



    execute <<-SQL
      CREATE OR REPLACE VIEW formacion_view AS



        SELECT
                    M.id,
                    A.rut,
                    A.sexo,
                    A.fecha_ingreso_grupo,
                    A.fecha_nacimiento,
                    A.fecha_renuncia,
                    A.tipo_renuncia,
                    A.nombre,
                    A.apellidos,
                    P.division,
                    P.gerencia,
                    P.periodo,
                    P.piramide,
                    P.tipo_contrato,
                    P.nombre_empresa,
                    P.tipo_renta,
                    P.cargo,
                    P.estado_civil,
                    P.num_hijos,
                    M.periodo_anualizado AS periodo_formacion,
                    M.materia,
                    M.horas,
                    M.nombre_del_curso,
                    M.modalidad,
                    M.asistencia,
                    P.es_plantilla


         FROM formaciones       M
    LEFT JOIN empleados         A   ON M.empleado_id = A.id
    LEFT JOIN posiciones        P   ON P.empleado_id  = A.id AND (M.periodo = P.periodo OR P.periodo = null)
    ;
    SQL
  end

  def down
    execute 'DROP VIEW egreso_view'
    execute 'DROP VIEW retribucion_view'
    execute 'DROP VIEW movimiento_view'
    execute 'DROP VIEW formacion_view'

  end



end
