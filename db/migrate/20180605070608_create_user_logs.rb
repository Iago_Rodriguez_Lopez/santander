class CreateUserLogs < ActiveRecord::Migration
  def change
    create_table :user_logs do |t|

      t.timestamps null: false
    end
  end
end
