class CreateRetribucionesView < ActiveRecord::Migration
  def up
    execute <<-SQL
      CREATE OR REPLACE VIEW retribucion_view AS


        SELECT
                    A.id,
                    A.rut,
                    A.sexo,
                    A.fecha_ingreso_grupo,
                    A.fecha_nacimiento,
                    A.fecha_renuncia,
                    A.tipo_renuncia,
                    P.division,
                    P.gerencia,
                    P.periodo,
                    P.piramide,
                    P.tipo_contrato,
                    P.nombre_empresa,
                    P.es_plantilla,
                    V.tipo_area,
                    M.periodo_anualizado AS periodo_retribucion,
                    M.tipo_movimiento,
                    M.anual AS retribucion_anual,
                    PT.nombre   AS nombre_programa,
                    PT.estado   AS estado_programa,
                    PT.periodo AS periodo_programa

         FROM retribuciones       M
    LEFT JOIN empleados         A   ON M.empleado_id = A.id
    LEFT JOIN posiciones        P   ON P.empleado_id  = A.id AND (M.periodo = P.periodo OR P.periodo = null)
    LEFT JOIN vacantes          V   ON P.gerencia = V.gerencia
    LEFT JOIN programa_talentos PT  ON PT.empleado_id  = A.id AND (PT.periodo = M.periodo OR PT.periodo = null)
    ;
    SQL
  end

  def down
    execute 'DROP VIEW retribucion_view'
  end

end

