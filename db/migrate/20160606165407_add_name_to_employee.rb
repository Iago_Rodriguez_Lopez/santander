class AddNameToEmployee < ActiveRecord::Migration
  def change
    add_column :empleados, :nombre, :string
    add_column :empleados, :apellidos, :string
  end
end
