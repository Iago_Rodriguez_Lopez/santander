class CreateEgreso < ActiveRecord::Migration
  def change
    create_table :egresos do |t|
      t.integer :empleado_id
      t.string :periodo
      t.string :periodo_anualizado
      t.string :tipo_egreso
      t.string :rut
      t.string :division
      t.string :gerencia
      t.string :piramide
      t.string :tipo_contrato
      t.boolean :es_plantilla, default: true
    end
  end
end
