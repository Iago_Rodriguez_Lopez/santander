class ChangePeriodoPosicion < ActiveRecord::Migration
  def change
    change_column :posiciones, :periodo,  :string
  end
end
