class CreateLicencias < ActiveRecord::Migration
  def change
    create_table :licencias do |t|
      t.date :fecha
      t.string :glosa_tipo_licencia
      t.integer :empleado_id
      t.date :fecha_inicio
      t.date :fecha_termino
      t.integer :dias_totales
      t.integer :dias_corridos
      t.integer :dias_habiles
      t.string :periodo
      t.timestamps null: false
    end
  end
end

