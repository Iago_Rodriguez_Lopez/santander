class AddFailedAttempts < ActiveRecord::Migration
  def change
    add_column :users, :unlock_token, :string
    add_column :users, :failed_attempts, :integer, default: 0,  null: false

  end
end
