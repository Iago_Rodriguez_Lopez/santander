class CreateProgramaTalentos < ActiveRecord::Migration
  def change
    create_table :programa_talentos do |t|
      t.integer :empleado_id
      t.string :periodo
      t.string :nombre
      t.string :estado

      t.timestamps null: false
    end
  end
end
