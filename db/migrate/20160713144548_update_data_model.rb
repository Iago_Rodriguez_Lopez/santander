class UpdateDataModel < ActiveRecord::Migration
  def change
    add_column :posiciones, :tipo_renta, :string
    add_column :posiciones, :cargo, :string
    add_column :posiciones, :estado_civil, :string
    add_column :posiciones, :num_hijos, :integer
    add_column :egresos, :tipo_renta, :string
    add_column :egresos, :cargo, :string
    add_column :egresos, :estado_civil, :string
    add_column :egresos, :num_hijos, :integer

  end
end

