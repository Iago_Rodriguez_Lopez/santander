class CreateEgresoView < ActiveRecord::Migration

  def up
    execute <<-SQL
      CREATE OR REPLACE VIEW egreso_view AS


        SELECT
                    A.id,
                    A.rut,
                    A.sexo,
                    A.fecha_ingreso_grupo,
                    A.fecha_nacimiento,
                    P.division,
                    P.gerencia,
                    P.periodo,
                    P.periodo_anualizado AS periodo_egreso,
                    P.piramide,
                    P.tipo_contrato,
                    P.es_plantilla,
                    V.tipo_area,
                    P.tipo_egreso

         FROM egresos P
    LEFT JOIN empleados        A  ON P.empleado_id  = A.id
    LEFT JOIN vacantes        V ON P.gerencia = V.gerencia
    ;
    SQL
  end

  def down
    execute 'DROP VIEW egreso_view'
  end

end
