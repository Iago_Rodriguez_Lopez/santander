class CreateFormaciones < ActiveRecord::Migration
  def change
    create_table :formaciones do |t|
      t.string :materia
      t.string :nombre_del_curso
      t.date :fecha_inicio
      t.date :fecha_fin
      t.integer :horas
      t.string :periodo
      t.string :modalidad
      t.integer :empleado_id

      t.timestamps null: false
    end
  end
end
