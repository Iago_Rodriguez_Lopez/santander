class FinalFixesGenerales < ActiveRecord::Migration
  def change
    remove_column :movimientos, :resto_año
    remove_column :movimientos, :anual
    remove_column :vacaciones, :uso_10_dias_contiguos
    remove_column :vacaciones, :provision
    remove_column :vacaciones, :fecha_inicio
    remove_column :vacaciones, :fecha_fin
    remove_column :empleados, :dias_vacaciones
    remove_column :empleados, :provision_vacaciones
    remove_column :formaciones, :fecha_inicio
    remove_column :formaciones, :fecha_fin
  end
end
