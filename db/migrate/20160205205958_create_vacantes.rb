class CreateVacantes < ActiveRecord::Migration
  def change
    create_table :vacantes do |t|
      t.string :division
      t.string :gerencia
      t.integer :vacantes
      t.string :periodo

      t.timestamps null: false
    end
  end
end
