class AddRutToAllModels < ActiveRecord::Migration
  def change
    add_column :posiciones, :rut, :string
    add_column :licencias, :rut, :string
    add_column :vacaciones, :rut, :string
    add_column :formaciones, :rut, :string
    add_column :movimientos, :rut, :string
    add_column :prezos, :rut, :string
    add_column :programa_talentos, :rut, :string
    add_column :retribuciones, :rut, :string
  end
end
