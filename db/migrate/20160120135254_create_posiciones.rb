class CreatePosiciones < ActiveRecord::Migration
  def change
    create_table :posiciones do |t|
      t.string :piramide
      t.string :tipo_contrato
      t.string :glosa_contrato
      t.string :division
      t.string :gerencia
      t.string :area
      t.string :subarea
      t.date :periodo
      t.integer :empleado_id

      t.timestamps null: false
    end

  end
end
