class NewUsersTable < ActiveRecord::Migration
  # def change
  #   drop_table "users"

  #   create_table "users", force: :cascade do |t|
  #     t.string   "email",                  default: "", null: false
  #     t.string   "encrypted_password",     default: "", null: false
  #     t.string   "reset_password_token"
  #     t.datetime "reset_password_sent_at"
  #     t.datetime "remember_created_at"
  #     t.integer  "sign_in_count",          default: 0,  null: false
  #     t.datetime "current_sign_in_at"
  #     t.datetime "last_sign_in_at"
  #     t.inet     "current_sign_in_ip"
  #     t.inet     "last_sign_in_ip"
  #     t.string   "confirmation_token"
  #     t.datetime "confirmed_at"
  #     t.datetime "confirmation_sent_at"
  #     t.string   "unconfirmed_email"
  #     t.integer  "failed_attempts",        default: 0,  null: false
  #     t.string   "unlock_token"
  #     t.datetime "locked_at"
  #     t.datetime "created_at",                          null: false
  #     t.datetime "updated_at",                          null: false
  #     t.string   "invitation_token"
  #     t.datetime "invitation_created_at"
  #     t.datetime "invitation_sent_at"
  #     t.datetime "invitation_accepted_at"
  #     t.integer  "invitation_limit"
  #     t.string   "invited_by_type"
  #     t.integer  "invited_by_id"
  #     t.integer  "invitations_count",      default: 0
  #     t.jsonb    "metadata",               default: {}
  #     t.integer  "rol",                    default: 0,  null: false
  #     t.string   "accesos",                default: [],              array: true
  #     t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
  #     t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
  #     t.index ["invitations_count"], name: "index_users_on_invitations_count", using: :btree
  #     t.index ["invited_by_id"], name: "index_users_on_invited_by_id", using: :btree
  #     t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  #   end
  # end
  def change
    # drop_table "users"
    #
    # create_table "users", force: :cascade do |t|
    #   t.string   "email",                  default: "", null: false
    #   t.string   "encrypted_password",     default: "", null: false
    #   t.string   "reset_password_token"
    #   t.datetime "reset_password_sent_at"
    #   t.datetime "remember_created_at"
    #   t.integer  "sign_in_count",          default: 0,  null: false
    #   t.datetime "current_sign_in_at"
    #   t.datetime "last_sign_in_at"
    #   t.inet     "current_sign_in_ip"
    #   t.inet     "last_sign_in_ip"
    #   t.string   "confirmation_token"
    #   t.datetime "confirmed_at"
    #   t.datetime "confirmation_sent_at"
    #   t.string   "unconfirmed_email"
    #   t.integer  "failed_attempts",        default: 0,  null: false
    #   t.string   "unlock_token"
    #   t.datetime "locked_at"
    #   t.datetime "created_at",                          null: false
    #   t.datetime "updated_at",                          null: false
    #   t.string   "invitation_token"
    #   t.datetime "invitation_created_at"
    #   t.datetime "invitation_sent_at"
    #   t.datetime "invitation_accepted_at"
    #   t.integer  "invitation_limit"
    #   t.string   "invited_by_type"
    #   t.integer  "invited_by_id"
    #   t.integer  "invitations_count",      default: 0
    #   t.jsonb    "metadata",               default: {}
    #   t.integer  "rol",                    default: 0,  null: false
    #   t.string   "accesos",                default: [],              array: true
    #   t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    #   t.index ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
    #   t.index ["invitations_count"], name: "index_users_on_invitations_count", using: :btree
    #   t.index ["invited_by_id"], name: "index_users_on_invited_by_id", using: :btree
    #   t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    # end
  end
end
