class AddNewModelsElements < ActiveRecord::Migration
  def change
    # create_table :empresas do |t|
    #   t.integer :empleado_id
    #   t.string :periodo
    #   t.string :nombre
    #   t.string :rut
    #   t.boolean :activo
    #   t.timestamps null: false
    # end
    add_column :licencias, :tipo_licencia, :string
    add_column :vacantes, :tipo_area, :string
    add_column :vacantes, :activo, :boolean
    add_column :empleados, :motivo_renuncia, :string
    add_column :posiciones, :nombre_empresa, :string
    add_column :posiciones, :es_plantilla, :boolean, default: true
    remove_column :vacantes, :periodo

  end
end
