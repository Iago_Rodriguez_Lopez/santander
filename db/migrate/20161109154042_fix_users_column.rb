class FixUsersColumn < ActiveRecord::Migration
  def change
    add_column :users, :metadata, :jsonb, default: {}, null: false
    add_column :users, :rol, :integer, default: 0, null: false
    add_column :users, :accesos, :string, array: true, default: []
  end
end
