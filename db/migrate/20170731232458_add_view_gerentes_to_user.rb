class AddViewGerentesToUser < ActiveRecord::Migration
  def change
    add_column :users, :view_gerentes, :boolean, default: false

  end
end
