class CreateVacaciones < ActiveRecord::Migration
  def change
    create_table :vacaciones do |t|
      t.date :fecha_inicio
      t.date :fecha_fin
      t.decimal :conseguidos, precision: 10, scale: 2
      t.decimal :consumidos, precision: 10, scale: 2
      t.decimal :saldo, precision: 10, scale: 2
      t.decimal :provision, precision: 10, scale: 2
      t.boolean :uso_10_dias_contiguos
      t.integer :empleado_id
      t.string :periodo

      t.timestamps null: false
    end
  end
end

