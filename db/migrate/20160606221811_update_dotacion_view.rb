class UpdateDotacionView < ActiveRecord::Migration
  def up
    if ActiveRecord::Base.connection.table_exists? 'dotacion_view'
      execute 'DROP VIEW dotacion_view'
    end
    execute <<-SQL
      CREATE OR REPLACE VIEW dotacion_view AS


        SELECT
                    A.id,
                    A.rut,
                    A.sexo,
                    A.fecha_ingreso_grupo,
                    A.fecha_nacimiento,
                    A.fecha_renuncia,
                    A.tipo_renuncia,
                    P.division,
                    P.gerencia,
                    P.periodo,
                    P.piramide,
                    P.tipo_contrato,
                    P.nombre_empresa,
                    V.tipo_area,
                    P.es_plantilla

         FROM posiciones P
    LEFT JOIN empleados        A  ON P.empleado_id  = A.id
    LEFT JOIN vacantes        V ON P.gerencia = V.gerencia
    ;
    SQL
  end

  def down
    execute 'DROP VIEW dotacion_view'
  end


end
