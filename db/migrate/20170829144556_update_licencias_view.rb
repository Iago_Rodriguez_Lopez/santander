


class UpdateLicenciasView < ActiveRecord::Migration
  def change

    if ActiveRecord::Base.connection.table_exists? 'licencia_view'
      execute 'DROP VIEW licencia_view'
    end    




    execute <<-SQL
      CREATE OR REPLACE VIEW licencia_view AS


        SELECT
                    L.id,
                    A.rut,
                    A.sexo,
                    A.fecha_ingreso_grupo,
                    A.fecha_nacimiento,
                    A.fecha_renuncia,
                    A.tipo_renuncia,
                    A.nombre,
                    A.apellidos,
                    P.division,
                    P.gerencia,
                    P.periodo,
                    P.piramide,
                    P.tipo_contrato,
                    P.nombre_empresa,
                    P.es_plantilla,
                    P.tipo_renta,
                    P.cargo,
                    P.estado_civil,
                    P.num_hijos,
                    L.periodo_anualizado AS periodo_licencia,
                    L.glosa_tipo_licencia,
                    L.dias_totales,
                    L.dias_corridos,
                    L.dias_habiles


         FROM licencias       L
    LEFT JOIN empleados         A   ON L.empleado_id = A.id
    LEFT JOIN posiciones        P   ON P.empleado_id  = A.id AND (L.periodo = P.periodo OR P.periodo = null)
    ;
    SQL
    

  end
end
