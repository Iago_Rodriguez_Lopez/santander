class AddOrderToEstructura < ActiveRecord::Migration
  def change
    add_column :vacantes, :division_order, :integer
    add_column :vacantes, :gerencia_order, :integer
  end
end
