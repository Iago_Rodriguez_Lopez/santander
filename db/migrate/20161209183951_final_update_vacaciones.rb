class FinalUpdateVacaciones < ActiveRecord::Migration

  def change
    if ActiveRecord::Base.connection.table_exists? 'vacacion_view'
      execute 'DROP VIEW vacacion_view'
    end
    remove_column :vacaciones, :conseguidos
    #add_column :vacaciones, :dias_10, :boolean
    #add_column :vacaciones, :periodos_pendientes, :integer

    execute <<-SQL
      CREATE OR REPLACE VIEW vacacion_view AS


        SELECT
                    V.id,
                    A.rut,
                    A.sexo,
                    A.fecha_ingreso_grupo,
                    A.fecha_nacimiento,
                    A.fecha_renuncia,
                    A.tipo_renuncia,
                    A.nombre,
                    A.apellidos,
                    P.division,
                    P.gerencia,
                    P.periodo,
                    P.piramide,
                    P.tipo_contrato,
                    P.nombre_empresa,
                    P.es_plantilla,
                    P.tipo_renta,
                    P.cargo,
                    P.estado_civil,
                    P.num_hijos,
                    V.periodo_anualizado AS periodo_vacacion,
                    V.saldo,
                    V.consumidos,
                    V.dias_10,
                    V.periodos_pendientes

         FROM vacaciones       V
    LEFT JOIN empleados         A   ON V.empleado_id = A.id
    LEFT JOIN posiciones        P   ON P.empleado_id  = A.id AND (V.periodo = P.periodo OR P.periodo = null)
    ;
    SQL

  end



end
