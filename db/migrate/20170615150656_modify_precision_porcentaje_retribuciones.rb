class ModifyPrecisionPorcentajeRetribuciones < ActiveRecord::Migration
  def change
    execute 'DROP VIEW retribucion_view'
    change_column :retribuciones, :porcentaje, :decimal, precision: 5,  scale: 2
    execute <<-SQL
      CREATE OR REPLACE VIEW retribucion_view AS


        SELECT
                    M.id,
                    A.rut,
                    A.sexo,
                    A.fecha_ingreso_grupo,
                    A.fecha_nacimiento,
                    A.fecha_renuncia,
                    A.tipo_renuncia,
                    A.nombre,
                    A.apellidos,
                    P.division,
                    P.gerencia,
                    P.periodo,
                    P.piramide,
                    P.tipo_contrato,
                    P.nombre_empresa,
                    P.es_plantilla,
                    P.tipo_renta,
                    P.cargo,
                    P.estado_civil,
                    P.num_hijos,
                    M.periodo_anualizado AS periodo_retribucion,
                    M.tipo_movimiento,
                    M.porcentaje,
                    M.anual AS retribucion_anual

         FROM retribuciones       M
    LEFT JOIN empleados         A   ON M.empleado_id = A.id
    LEFT JOIN posiciones        P   ON P.empleado_id  = A.id AND (M.periodo = P.periodo OR P.periodo = null)
    ;
    SQL
  end
end
