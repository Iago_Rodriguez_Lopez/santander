class CreateEmpleados < ActiveRecord::Migration
  def change
    create_table :empleados do |t|
      t.string :rut
      t.string :sexo
      t.date :fecha_ingreso_grupo
      t.date :fecha_nacimiento
      t.date :fecha_renuncia
      t.string :tipo_renuncia
      t.integer :dias_vacaciones
      t.integer :provision_vacaciones

      t.timestamps null: false
    end

  end
end


