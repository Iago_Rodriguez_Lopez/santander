class UpdateMovimientos < ActiveRecord::Migration
  def up
    add_column :movimientos, :posicion_origen_id, :integer
    
    if ActiveRecord::Base.connection.table_exists? 'movimiento_view'
      execute 'DROP VIEW movimiento_view'
    end

    execute <<-SQL
    CREATE OR REPLACE VIEW movimiento_view AS


      SELECT
                  M.id,
                  A.rut,
                  A.sexo,
                  A.fecha_ingreso_grupo,
                  A.fecha_nacimiento,
                  A.fecha_renuncia,
                  A.tipo_renuncia,
                  A.nombre,
                  A.apellidos,
                  P1.division,
                  P1.gerencia,
                  P1.piramide,
                  P1.tipo_contrato,
                  P1.nombre_empresa,
                  P1.es_plantilla,
                  P1.tipo_renta,
                  P1.cargo,
                  P1.estado_civil,
                  P1.num_hijos,
                  M.periodo,
                  M.periodo_anualizado AS periodo_movimiento,
                  M.tipo_movimiento
      FROM movimientos       M
      LEFT JOIN empleados         A   ON M.empleado_id = A.id 
      JOIN posiciones        P1  ON M.posicion_origen_id = P1.id
  
  ;
  SQL
  # LEFT JOIN posiciones        P   ON P.empleado_id  = A.id AND (P.periodo = M.periodo OR P.periodo = null)
  #LEFT JOIN posiciones        P   ON P.empleado_id  = A.id AND (P.periodo = (to_char(to_date(M.periodo, 'MM-YYYY') - interval '1 month', 'MM-YYYY')) OR P.periodo = null)
  

  end

  def down
    remove_column :movimientos, :posicion_origen_id, :string
    
  end
end
