class MaterializedTest < ActiveRecord::Migration
  def change
    if ActiveRecord::Base.connection.table_exists? 'dotacion_view'
      execute 'DROP MATERIALIZED VIEW dotacion_view'
    end

    execute <<-SQL


      CREATE MATERIALIZED VIEW dotacion_view AS


        SELECT
                    P.id,
                    A.rut,
                    A.nombre,
                    A.apellidos,
                    A.sexo,
                    A.fecha_ingreso_grupo,
                    A.fecha_nacimiento,
                    A.fecha_renuncia,
                    A.tipo_renuncia,
                    P.division,
                    P.gerencia,
                    P.periodo,
                    P.piramide,
                    P.tipo_contrato,
                    P.nombre_empresa,
                    P.tipo_renta,
                    P.cargo,
                    P.estado_civil,
                    P.num_hijos,
                    V.tipo_area,
                    P.es_plantilla

         FROM posiciones P
    LEFT JOIN empleados        A  ON P.empleado_id  = A.id
    LEFT JOIN vacantes        V ON P.gerencia = V.gerencia AND (P.periodo = V.periodo) AND (P.division = V.division)
    ;
    SQL

    add_index :dotacion_view, :periodo
    add_index :dotacion_view, :es_plantilla
    add_index :dotacion_view, :tipo_renta
    add_index(:dotacion_view, [:periodo, :es_plantilla, :tipo_renta],name: "view_dotacion_empleados_index")


  end
end
