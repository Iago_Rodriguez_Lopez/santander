class AddIndexes < ActiveRecord::Migration
  def change
        add_index :posiciones, :periodo
        add_index :posiciones, :es_plantilla
        add_index :posiciones, :tipo_renta
        add_index(:posiciones, [:id,:periodo, :es_plantilla, :tipo_renta],name: "posiciones_empleados_index")
        # add_index(:dotacion_view, [:periodo, :es_plantilla, :tipo_renta],name: "view_dotacion_empleados_index")
        add_index :retribuciones, :empleado_id
        add_index :posiciones, :empleado_id
        add_index :formaciones, :empleado_id
        add_index :egresos, :empleado_id
        add_index :movimientos, :empleado_id
        add_index :programa_talentos, :empleado_id







  end
end
