# == Schema Information
#
# Table name: prezos
#
#  id          :integer          not null, primary key
#  empleado_id :integer
#  periodo     :string
#  pre         :decimal(10, 2)
#  zona        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  rut         :string
#

class Prezo < ActiveRecord::Base
  belongs_to :empleado
  scope :in_period, ->(period) { where('periodo = ?', period) unless period.blank?}




  def self.csv_import(data, periodo)
    # csv_handler = File.open(data.tempfile)
    begin
    # csv_forced = csv_handler.read.force_encoding(Encoding::UTF_8)
    # csv_string_encoded = csv_handler.read.encode!("iso-8859-1", "UTF-8", invalid: :replace)
    # csv = CSV.parse(csv_forced, :headers => true, :col_sep => ";")
    # csv.each_with_index do |row, index| encoding: Encoding::ISO_8859_1
    #
    # validaciónes en cliente: checkear todos los ruts existen
      CSV.foreach(data.tempfile, encoding: Encoding::UTF_8,headers: true, :col_sep => "\t") do |row, index|
        empleado = Empleado.find_by_rut(row["Rut"])

        if empleado
          prezo = Prezo.find_by(empleado_id: empleado.id, periodo: periodo) || empleado.prezos.new
          prezo.rut = row["Rut"]
          prezo.pre = row["PRE_"]
          prezo.zona = row["Zona"]
          prezo.periodo = periodo
          prezo.save!
        end
      end
      return true
    rescue Exception => e
      puts e.message
      puts e.backtrace.inspect
      return false
    end
  end
end
