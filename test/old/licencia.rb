# == Schema Information
#
# Table name: licencias
#
#  id                  :integer          not null, primary key
#  fecha               :date
#  glosa_tipo_licencia :string
#  empleado_id         :integer
#  fecha_inicio        :date
#  fecha_termino       :date
#  dias_totales        :integer
#  dias_corridos       :integer
#  dias_habiles        :integer
#  periodo             :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  rut                 :string
#  periodo_anualizado  :string
#  tipo_licencia       :string
#

class Licencia < ActiveRecord::Base
  belongs_to :empleado
  scope :in_period, ->(period) { where('periodo = ?', period) unless period.blank?}


  def self.csv_import(data, periodo)
    # csv_handler = File.open(data.tempfile)
    begin
    # csv_forced = csv_handler.read.force_encoding(Encoding::UTF_8)
    # csv_string_encoded = csv_handler.read.encode!("iso-8859-1", "UTF-8", invalid: :replace)
    # csv = CSV.parse(csv_forced, :headers => true, :col_sep => ";")
    # csv.each_with_index do |row, index| encoding: Encoding::ISO_8859_1
    #rut_fncro
    # validaciónes en cliente: checkear todos los ruts existen
      CSV.foreach(data.tempfile, encoding: Encoding::UTF_8,headers: true, :col_sep => "\t") do |row, index|
        empleado = Empleado.find_by_rut(row["rut"])
        if empleado

            licencia = Licencia.find_by(empleado_id: empleado.id, periodo: periodo) || empleado.licencias.new
            licencia.rut = row["rut"]
            licencia.glosa_tipo_licencia = row["glosa_tipo_licencia"]
            licencia.dias_totales = row["dias_totales"].to_i
            licencia.dias_corridos = row["dias_corridos_periodo"].to_i
            licencia.dias_habiles = row["dias_habiles_periodo"].to_i
            licencia.periodo = periodo
            licencia.periodo_anualizado = Date.strptime(row["fecha_inicio"], "%d/%m/%y").strftime("%m-%Y")
            licencia.save!
        end
      end
      return true
    rescue Exception => e
      puts e.message
      # puts e.backtrace.inspect
      return false
    end
  end

end
