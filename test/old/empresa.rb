# == Schema Information
#
# Table name: empresas
#
#  id          :integer          not null, primary key
#  empleado_id :integer
#  periodo     :string
#  nombre      :string
#  rut         :string
#  activo      :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#

class Empresa < ActiveRecord::Base
  belongs_to :empleado

  scope :in_period, ->(period) { where('periodo = ?', period) unless period.blank?}

  def self.csv_import(data, periodo)
    # csv_handler = File.open(data.tempfile)
    begin
    # csv_forced = csv_handler.read.force_encoding(Encoding::UTF_8)
    # csv_string_encoded = csv_handler.read.encode!("iso-8859-1", "UTF-8", invalid: :replace)
    # csv = CSV.parse(csv_forced, :headers => true, :col_sep => ";")
    # csv.each_with_index do |row, index| encoding: Encoding::ISO_8859_1
    #rut_fncro
    # validaciónes en cliente: checkear todos los ruts existen
      CSV.foreach(data.tempfile, encoding: Encoding::UTF_8,headers: true, :col_sep => "\t") do |row, index|
        empleado = Empleado.find_by_rut(row["rut"])

        if empleado
            empresa = Empresa.find_by(empleado_id: empleado.id, periodo: periodo) || empleado.empresas.new
            empresa.rut = row["rut"]
            empresa.nombre = row["Empresa"]
            empresa.periodo = periodo
            empresa.activo = true
            empresa.save!
        end
      end
      date = Date.strptime(periodo,"%m-%Y")
      division = "global"
      view_dotacion = ViewDotacion.started_before(date).finished_after(date).in_period(periodo).to_json
      # $redis.set("view_dotacion"+periodo+division, view_dotacion)
      return true
    rescue Exception => e
      puts e.message
      puts e.backtrace.inspect
      return false
    end
  end

end
