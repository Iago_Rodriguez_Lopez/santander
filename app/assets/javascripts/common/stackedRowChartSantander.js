// Bar chart Module
/////////////////////////////////

// Declare namespace


// Declare component: (this outer function acts as the closure):
d3.cloudshapes.stackedRowChartSantander = function module() {
    var margin = { top: 0, right: 20, bottom: 40, left: 90 },
        width = 500,
        height = 500,
        gap = 0,
        ease = "bounce";

    var data = [];
    var labels = [];
    var dataTitle = "";
    var dataUnit = "";
    var onClickFunction = function() {};
    var svg;

    var updateData;

    var tip = d3.tip()
        .attr('class', 'd3-tip')
        .offset([-10, 0])
        .html(function(d, i) {
            return "<h4><strong>" + labels[i] + "</strong></h4><div class='content'><table><tr><td>" + dataTitle + ":</td><td style='font-size: 18px'>" + d + " " + dataUnit.toLocaleString("es-ES") + "</td></tr></table></div>";
        })



    function wrap(text, width, right) {
        text.each(function() {
            var text = d3.select(this),
                words = text.text().split(/\s+/).reverse(),
                word,
                line = [],
                lineNumber = 0,
                lineHeight = 1.1, // ems
                y = text.attr("y"),
                dy = parseFloat(text.attr("dy")),
                tspan = text.text(null).append("tspan").attr("x", -right).attr("y", y).attr("dy", dy + "em");
            while (word = words.pop()) {
                line.push(word);
                tspan.text(line.join(" "));
                if (tspan.node().getComputedTextLength() > width) {
                    line.pop();
                    tspan.text(line.join(" "));
                    line = [word];
                    tspan = text.append("tspan").attr("x", -right).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
                }
            }
        });
    }


    // Define the 'inner' function: which, through the surreal nature of JavaScript scoping, can access
    // the above variables.
    function exports(_selection) {
        _selection.each(function() {
            var chartW = width - margin.left - margin.right,
                chartH = height - margin.top - margin.bottom;

            var color = d3.scale.linear()
                .domain([0, data.length / 2, data.length])
                .range(["#F7D706", "#F78206", "#5F4027"]);

            var stackedData = new Array();

            data.forEach(function(d, i) {
                d.forEach(function(d, j) {

                    if (stackedData[i] === undefined) {
                        stackedData[i] = 0;
                    }
                    stackedData[i] = stackedData[i] + d;
                });
            });

            // Define x and y scale variables.
            var x1 = d3.scale.linear()
                .domain([0, d3.max(stackedData, function(d, i) { return d; })])
                .range([0, chartW]);

            var y1 = d3.scale.ordinal()
                .domain(labels.map(function(d) { return d; }))
                .rangeRoundBands([0, chartH], 0.1);

            var xAxis = d3.svg.axis()
                .scale(x1)
                .orient("bottom")
                .ticks(4)
                .innerTickSize(chartH)
                .outerTickSize(10)
                .tickPadding(5)
                .tickFormat(function(d) { return d.toLocaleString("es-ES"); });

            var yAxis = d3.svg.axis()
                .scale(y1)
                .orient("left")
                .outerTickSize(0)
                .tickPadding(20);
            // .tickFormat(function (d){ return ""});

            // If no SVG exists, create one - and add key groups:
            if (!svg) {
                svg = d3.select(this)
                    .append("svg")
                    .classed("santander-rowchart", true);
                var container = svg.append("g").classed("container-group", true);
                svg.select(".container-group")
                    .attr({ transform: "translate(" + margin.left + "," + margin.top + ")" });
            }

            // Transition the width and height of the main SVG and the key 'g' group:
            svg.transition().attr({ width: width, height: height });

            container.append("g")
                .attr("class", "y axis")
                .call(yAxis)
                .selectAll(".tick text")
                .call(wrap, (margin.left), 15);

            container.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(" + 0 + "," + 0 + ")")
                .call(xAxis);

            container.append("g")
                .attr("class", "x axis mark")
                .append('line')
                .attr("class", "group-name-line")
                .attr('x2', chartW)
                .attr('x1', -10)
                .attr('y2', chartH)
                .attr('y1', chartH)
                .style('stroke', "black");

            container.append("g")
                .attr("class", "y axis mark")
                .append('line')
                .attr("class", "group-name-line")
                .attr('x2', 0)
                .attr('x1', 0)
                .attr('y2', 0)
                .attr('y1', chartH)
                .style('stroke', "black");

            container.append("g").classed("row-chart-group", true);

            // Define gap between bars:
            var gapSize = y1.rangeBand() / 100 * gap;

            // Define width of each bar:
            var barH = y1.rangeBand() - gapSize;

            // Select all bars and bind data:
            var rows = svg.select(".row-chart-group")
                .selectAll(".row")

            data.forEach(function(d, i) {
                var aux = 1;
                var x = 0;
                rows.data(d)
                    .enter()
                    .append("rect")
                    .classed("row" + i, true)
                    .attr('x', function(d, i) {
                        x = x + aux;
                        aux = x1(d);
                        return x;
                    })
                    .attr('width', function(d, i) {
                        return x1(d);
                    })
                    .attr('y', y1(labels[i]) + gapSize / 2)
                    .attr('height', barH)
                    .on('mouseover', function(d, i) {
                        tip.show(d, i);
                        d3.select(this).style("stroke", "red");
                    })
                    .on('mouseout', function(d, i) {
                        tip.hide(d, i);
                        d3.select(this).style("stroke", "none");
                    })
                    .on("click", function(d) {
                        // svg.selectAll('.display-bar').classed('_selected_', false);
                        if (d3.select(this.parentNode).classed("_selected_")) {
                            d3.select(this.parentNode).classed("_selected_", false);
                        } else {
                            d3.select(this.parentNode).classed("_selected_", true);
                        }
                        onClickFunction(this, d);
                    })
                    .style("fill", function(d, i) { return color(i); });
            });

            svg.call(tip);;


            updateWidthStackedTowChartSantander = function() {
                if (width > 1000) { width = 1000; }

                chartW = width - margin.left - margin.right;
                x1 = d3.scale.linear()
                    .domain([0, d3.max(stackedData, function(d, i) { return d; })])
                    .range([0, chartW]);

                xAxis = d3.svg.axis()
                    .scale(x1)
                    .orient("bottom")
                    .ticks(4)
                    .innerTickSize(chartH)
                    .outerTickSize(10)
                    .tickPadding(5)
                    .tickFormat(function(d) { return d.toLocaleString("es-ES"); });

                svg.select('.x.axis')
                    .transition()
                    .attr("transform", "translate(" + 0 + "," + 0 + ")")
                    .call(xAxis);

                svg.select(".x.axis.mark")
                    .append('line')
                    .attr('x2', chartW)
                    .attr('x1', -10)
                    .attr('y2', chartH)
                    .attr('y1', chartH)
                    .style('stroke', "black");

                data.forEach(function(d, i) {
                    var aux = 1;
                    var x = 0;
                    svg.selectAll('rect.row' + i)
                        .transition()
                        .attr('x', function(d, i) {
                            x = x + aux;
                            aux = x1(d);
                            return x;
                        })
                        .attr('width', function(d, i) {
                            return x1(d);
                        });
                });

                svg.transition().attr({ width: width, height: height });
            }

            // D3 UPDATE code for bars
            // rows.transition()
            //     .ease(ease)
            //     .attr({
            //         x: 0,
            //         width: function(d, i) { return chartW - x1(d);},
            //         y: function(d, i) { return y1(labels[i]) + gapSize / 2; },
            //         height: barH
            //     });

            // D3 EXIT code for bars
            // rows.exit().transition().style({opacity: 0}).remove();


            updateData = function() {
                _data = data;
                // Define x and y scale variables.
                x1 = d3.scale.linear()
                    .domain([0, d3.max(_data, function(d, i) { return d; })])
                    .range([chartW, 0]);


                y1 = d3.scale.ordinal()
                    .domain(_data.map(function(d, i) { return i; }))
                    .rangeRoundBands([0, chartH], 0.1);
                // Define gap between bars:
                gapSize = y1.rangeBand() / 100 * gap;

                // Define width of each bar:
                barH = y1.rangeBand() - gapSize;

                // Select all bars and bind data:
                rows = svg.select(".row-chart-group")
                    .selectAll(".row")
                    .data(_data);


                rows.enter().append("rect")
                    .classed("row", true)
                    .attr({
                        x: 0,
                        width: function(d, i) { return chartW - x1(d); },
                        y: function(d, i) { return y1(i) + gapSize / 2; },
                        height: barH
                    })
                    .style("fill", color);




                // D3 UPDATE code for bars
                rows.transition()
                    .ease(ease).duration(1000)
                    .attr({
                        x: 0,
                        width: function(d, i) { return chartW - x1(d); },
                        y: function(d, i) { return y1(i) + gapSize / 2; },
                        height: barH
                    });

                // D3 EXIT code for bars
                rows.exit().transition().style({ opacity: 0 }).remove();

            }
        });
    }

    // GETTERS AND SETTERS:


    exports.data = function(_x) {
        if (!arguments.length) return data;
        data = _x;
        if (typeof updateData === 'function') updateData();
        return this;
    };


    exports.onClickFunction = function(value) {
        if (!arguments.length) return onClickFunction;
        onClickFunction = value;
        return this;
    };

    exports.labels = function(_x) {
        if (!arguments.length) return labels;
        labels = _x;
        return this;
    };

    exports.width = function(_x) {
        if (!arguments.length) return width;
        width = parseInt(_x);
        if (typeof updateWidthStackedTowChartSantander === 'function') updateWidthStackedTowChartSantander();
        return this;
    };

    exports.dataTitle = function(_x) {
        if (!arguments.length) return dataTitle;
        dataTitle = _x;
        return this;
    };

    exports.dataUnit = function(_x) {
        if (!arguments.length) return dataUnit;
        dataUnit = _x;
        return this;
    };

    exports.setColor = function(_x) {
        if (!arguments.length) return color;
        color = _x;
        return this;
    };

    exports.height = function(_x) {
        if (!arguments.length) return height;
        height = parseInt(_x);
        return this;
    };
    exports.gap = function(_x) {
        if (!arguments.length) return gap;
        gap = _x;
        return this;
    };
    exports.ease = function(_x) {
        if (!arguments.length) return ease;
        ease = _x;
        return this;
    };

    exports.margin = function(_x) {
        if (!arguments.length) return margin;
        margin = _x;
        return this;
    };
    return exports;
};