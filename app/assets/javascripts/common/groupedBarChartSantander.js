// Bar chart Module
/////////////////////////////////

// Declare namespace


// Declare component: (this outer function acts as the closure):
d3.cloudshapes.groupedBarChartSantander = function module() {
    var margin = { top: 20, right: 20, bottom: 40, left: 80 },
        width = 500,
        height = 500,
        gap = 0,
        color = "#001ff0"
    ease = "bounce";

    var colors = d3.scale.category20(),
        svg;

    var data = [];
    var labels = [];
    var dataTitle = "";
    var dataUnit = "";
    var onClickFunction = function() {};
    var svg;

    var updateData;

    var tip = d3.tip()
        .attr('class', 'd3-tip')
        .offset([-10, 0])
        .html(function(d, i) {
            return "<h4><strong>" + labels[i] + "</strong></h4><div class='content'><table><tr><td>" + dataTitle + ":</td><td style='font-size: 18px'>" + d + " " + dataUnit.toLocaleString("es-ES") + "</td></tr></table></div>";
        })


    // Define the 'inner' function: which, through the surreal nature of JavaScript scoping, can access
    // the above variables.
    function exports(_selection) {
        _selection.each(function() {
            _data = data;
            var chartW = width - margin.left - margin.right,
                chartH = height - margin.top - margin.bottom;

            // Define x and y scale variables.
            var x1 = d3.scale.ordinal()
                .domain(labels.map(function(d) { return d; }))
                .rangeRoundBands([0, chartW], 0.2);

            var x2 = d3.scale.ordinal().domain(_data.map(function(d, i) { return i; })).rangeRoundBands([0, chartW / _data[0]], 0.1);;

            var y1 = d3.scale.linear()
                .domain([0, d3.max(_.flatten(_data), function(d, i) { return d + d * .1; })]).nice(10)
                .range([chartH, 0]);


            var xAxis = d3.svg.axis()
                .scale(x1)
                .orient("bottom")
                .innerTickSize(0)
                .outerTickSize(10)
                .tickPadding(10)
                .tickFormat(function(d) { return d.toLocaleString("es-ES"); });

            var yAxis = d3.svg.axis()
                .scale(y1)
                .orient("left")
                .innerTickSize(-chartW)
                .ticks(3)
                .outerTickSize(20)
                .tickPadding(40)
                .tickFormat(function(d) { return "" });

            var yAxis1 = d3.svg.axis()
                .scale(y1)
                .orient('left')
                .ticks(3)
                .outerTickSize(0)
                .tickPadding(20)
                .tickSize(20)
                .tickFormat(function(d, i) {
                    if (i == 0) return "";
                    else return d.toLocaleString("es-ES");
                });



            var yAxis2 = d3.svg.axis()
                .scale(y1)
                .orient("left")
                .innerTickSize(0)
                .ticks(3)
                .tickSize(60)
                .tickPadding(60)
                .tickFormat(function(d) { return "" });



            // If no SVG exists, create one - and add key groups:
            if (!svg) {
                svg = d3.select(this)
                    .append("svg")
                    .classed("santander-groupedbarchart", true);
                var container = svg.append("g").classed("container-group", true);
            }

            container.append("g")
                .attr("class", "y axis")
                .attr("transform", "translate(" + 0 + "," + 0 + ")")
                .call(yAxis);

            container.append("g")
                .attr("class", "y axis1")
                .attr("transform", "translate(" + 0 + "," + -10 + ")")
                .call(yAxis1);

            container.append("g")
                .attr("class", "y axis2")
                .attr("transform", "translate(" + 0 + "," + 0 + ")")
                .call(yAxis2);

            container.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + chartH + ")")
                .call(xAxis);

            container.append("g")
                .attr("class", "x axis mark")
                .append('line')
                .attr("class", "group-name-line")
                .attr('x2', chartW)
                .attr('x1', -60)
                .attr('y2', chartH)
                .attr('y1', chartH)
                .style('stroke', "black");


            // Transition the width and height of the main SVG and the key 'g' group:
            svg.transition().attr({ width: width, height: height });
            svg.select(".container-group")
                .attr({ transform: "translate(" + margin.left + "," + margin.top + ")" });

            // Define gap between bars:
            var groupGapSize = x1.rangeBand() / 100 * gap;
            var barGapSize = 0;
            var barW = (x1.rangeBand() - barGapSize) / _data.length;

            // Select all bars and bind data:
            var groups = container.selectAll(".group-bars")
                .data(_data)
                .enter().append("g")
                .style("fill", function(d, i) { return colors[i] })
                .attr("transform", function(d, i) { return "translate(" + (barW * i) + ",0)"; });

            var bars = groups.selectAll(".bar")
                .data(function(d, i) { return d; });


            // ENTER, UPDATE and EXIT CODE:
            // D3 ENTER code for bars!
            bars.enter().append("rect")
                .classed("bar _selected_", true)
                .attr({
                    width: barW,
                    x: function(d, i) { return x1(labels[i]) + barGapSize / 2; },
                    y: function(d, i) { return y1(d); },
                    height: function(d, i) { return chartH - y1(d); }
                })
                .on('mouseover', function(d, i) { tip.show(d, i);
                    d3.select(this).style("stroke", "red"); })
                .on('mouseout', function(d, i) { tip.hide(d, i);
                    d3.select(this).style("stroke", "grey"); })
                .on("click", function(d) {
                    // svg.selectAll('.display-bar').classed('_selected_', false);
                    if (d3.select(this.parentNode).classed("_selected_")) {
                        d3.select(this.parentNode).classed("_selected_", false);
                    } else {
                        d3.select(this.parentNode).classed("_selected_", true);
                    }
                    onClickFunction(this, d);
                })
                .style("fill", color);

            svg.call(tip);

            // D3 EXIT code for bars
            bars.exit().transition().style({ opacity: 0 }).remove();


            updateData = function() {
                _data = data;

                y1 = d3.scale.linear()
                    .domain([0, d3.max(_.flatten(_data), function(d, i) { return d + d * .1; })])
                    .range([chartH, 0]);

                yAxis = d3.svg.axis()
                    .scale(y1)
                    .orient("left")
                    .innerTickSize(-chartW)
                    .ticks(3)
                    .outerTickSize(20)
                    .tickPadding(40)
                    .tickFormat(function(d) { return "" });

                yAxis1 = d3.svg.axis()
                    .scale(y1)
                    .orient('left')
                    .ticks(3)
                    .outerTickSize(0)
                    .tickPadding(20)
                    .tickSize(20)
                    .tickFormat(function(d, i) {
                        if (i == 0) return "";
                        else return d.toLocaleString("es-ES");
                    });



                yAxis2 = d3.svg.axis()
                    .scale(y1)
                    .orient("left")
                    .innerTickSize(0)
                    .ticks(3)
                    .tickSize(60)
                    .tickPadding(60)
                    .tickFormat(function(d) { return "" });

                svg.selectAll("g .y.axis").call(yAxis);
                svg.selectAll("g .y.axis1").call(yAxis1);
                svg.selectAll("g .y.axis2").call(yAxis2);

                groupGapSize = x1.rangeBand() / 100 * gap;
                barGapSize = 0;
                barW = (x1.rangeBand() - barGapSize) / _data.length;

                groups = container.selectAll(".group-bars").data(_data);
                //
                //ACTUALMENTE NO PERMITE AÑADIR NUEVOS GRUPOS DINAMICAMENTE
                bars = bars.data(function(d, i) { return _data[i] });


                bars.enter().append("rect")
                    .attr("class", "bar")
                    .attr("y", y1(0))
                    .attr("height", height - y1(0));


                // D3 UPDATE code for bars
                bars.transition().ease(ease).duration(1000)
                    .attr({
                        width: barW,
                        x: function(d, i) { return x1(labels[i]) + barGapSize / 2; },
                        y: function(d, i) { return y1(d); },
                        height: function(d, i) { return chartH - y1(d); }
                    });

                bars.exit()
                    .transition()
                    .duration(300)
                    .attr("y", y1(0))
                    .attr("height", chartH - y1(0))
                    .style('fill-opacity', 1e-6)
                    .remove();
                // bars.enter().append("rect")
                //     .classed("bar", true)
                //     .attr(
                //         {x: chartW,
                //         width: barW,
                //         y: function(d, i) { return y1(d); },
                //         height: function(d, i) { return chartH - y1(d);
                //         }
                //     });

                // D3 EXIT code for bars
                bars.exit().transition().style({ opacity: 0 }).remove();

            }

        });
    }

    exports.data = function(_x) {
        if (!arguments.length) return data;
        data = _x;
        if (typeof updateData === 'function') updateData();
        return this;
    };

    exports.onClickFunction = function(value) {
        if (!arguments.length) return onClickFunction;
        onClickFunction = value;
        return this;
    };

    exports.labels = function(_x) {
        if (!arguments.length) return labels;
        labels = _x;
        return this;
    };


    exports.dataTitle = function(_x) {
        if (!arguments.length) return dataTitle;
        dataTitle = _x;
        return this;
    };

    exports.dataUnit = function(_x) {
        if (!arguments.length) return dataUnit;
        dataUnit = _x;
        return this;
    };

    // GETTERS AND SETTERS:
    exports.width = function(_x) {
        if (!arguments.length) return width;
        width = parseInt(_x);
        return this;
    };

    exports.setColors = function(_x) {
        if (!arguments.length) return color;
        colors = _x;
        return this;
    };

    exports.height = function(_x) {
        if (!arguments.length) return height;
        height = parseInt(_x);
        return this;
    };
    exports.gap = function(_x) {
        if (!arguments.length) return gap;
        gap = _x;
        return this;
    };
    exports.ease = function(_x) {
        if (!arguments.length) return ease;
        ease = _x;
        return this;
    };

    exports.margin = function(_x) {
        if (!arguments.length) return margin;
        margin = _x;
        return this;
    };

    return exports;
};