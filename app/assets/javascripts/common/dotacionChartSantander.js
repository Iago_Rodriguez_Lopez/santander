// Bar chart Module
/////////////////////////////////

// Declare namespace


// Declare component: (this outer function acts as the closure):
d3.cloudshapes.dotacionChartSantander = function module() {
    var margin = { top: 60, right: 20, bottom: 40, left: 20 },
        width = 500,
        height = 500,
        gap = 0.1,
        color = "#001ff0"
    ease = "bounce";

    var data = [];
    var lineData = [];
    var labels = [];
    var dataTitle = [];
    var dataUnit = "";
    var onClickFunction = function() {};
    var currentMonth = null;
    var minimum = 0;
    var svg;

    var updateData;
    var updateWidth;

    // Define the 'inner' function: which, through the surreal nature of JavaScript scoping, can access
    // the above variables.
    function exports(_selection) {
        _selection.each(function() {
            _data = data;
            _lineData = lineData;
            var _aux = data.concat(lineData);

            var chartW = width - margin.left - margin.right,
                chartH = height - margin.top - margin.bottom;

            var tip = d3.tip()
                .attr('class', 'd3-tip')
                .offset([-10, 0])
                .html(function(d, i) {
                    var temp = (data[i] == undefined) ? "NA" : data[i];
                    var temp2 = (lineData[i] == undefined) ? "NA" : lineData[i];
                    if (temp > 0 & temp < 1) temp = temp.toFixed(4);
                    if (temp2 > 0 & temp2 < 1) temp2 = temp2.toFixed(4);
                    return "<h4><strong>" + labels[i] + "</strong></h4><div class='content'><table><tr><td>" + dataTitle[0] + ":</td><td style='font-size: 18px'>" + temp.toLocaleString("es-ES") + " " + dataUnit + "</td></tr><tr><td>" + dataTitle[1] + ":</td><td style='font-size: 18px'>" + temp2.toLocaleString("es-ES") + " " + dataUnit.toLocaleString("es-ES") + "</td></tr></table></div>";
                })


            // Define x and y scale variables.
            var x1 = d3.scale.ordinal()
                .domain(labels.map(function(d) { return d; }))
                .rangeRoundBands([0, chartW], 0.2);

            var y1 = d3.scale.linear()
                .domain([minimum, d3.max(_aux, function(d, i) { return d + d * .1; })])
                .range([chartH, 0]);

            var xAxis = d3.svg.axis()
                .scale(x1)
                .orient("bottom")
                .innerTickSize(0)
                .outerTickSize(10)
                .tickPadding(10)
                .tickFormat(function(d) { return d.toLocaleString("es-ES"); });

            var yAxis = d3.svg.axis()
                .scale(y1)
                .orient("left")
                .innerTickSize(-chartW)
                .ticks(3)
                .outerTickSize(20)
                .tickPadding(40)
                .tickFormat(function(d) { return "" });

            var yAxis1 = d3.svg.axis()
                .scale(y1)
                .orient('left')
                .ticks(3)
                .outerTickSize(0)
                .tickPadding(10)
                .tickSize(0)
                .tickFormat(function(d, i) {
                    if (i == 0) return "";
                    else return d.toLocaleString("es-ES") + " " + dataUnit;;
                });

            var yAxis2 = d3.svg.axis()
                .scale(y1)
                .orient("left")
                .innerTickSize(0)
                .ticks(3)
                .tickSize(60)
                .tickPadding(60)
                .tickFormat(function(d) { return "" });


            // If no SVG exists, create one - and add key groups:
            if (!svg) {
                svg = d3.select(this)
                    .append("svg")
                    .classed("santander-plantilla", true);
                var container = svg.append("g").classed("container-group", true);
            }


            container.append("g")
                .attr("class", "y axis")
                .attr("transform", "translate(" + 0 + "," + 0 + ")")
                .call(yAxis);

            container.append("g")
                .attr("class", "y axis1")
                .attr("transform", "translate(" + 0 + "," + -10 + ")")
                .call(yAxis1);

            container.append("g")
                .attr("class", "y axis2")
                .attr("transform", "translate(" + 0 + "," + 0 + ")")
                .call(yAxis2);

            container.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + chartH + ")")
                .call(xAxis);

            container.append("g")
                .attr("class", "x axis mark")
                .append('line')
                .attr("class", "group-name-line")
                .attr('x2', chartW)
                .attr('x1', -60)
                .attr('y2', chartH)
                .attr('y1', chartH)
                .style('stroke', "black");

            // Define gap between bars:
            var gapSize = x1.rangeBand() / 100 * gap;

            // Define width of each bar:
            var barW = x1.rangeBand() - gapSize;

            container.append("g").classed("plantilla-bar-chart-group", true);
            container.append("g").classed("plantilla-line-chart-group", true);

            var line = d3.svg.line()
                .x(function(d, i) { return x1(labels[i]) + (barW / 2); })
                .y(function(d) { return y1(d); })
                .interpolate("monotone");

            var path = svg.select(".plantilla-line-chart-group")
                .append("path") // Add the valueline path.
                .attr("class", "line")
                .attr("d", line(_lineData))
                .style("stroke", "#00BEFC");

            // Transition the width and height of the main SVG and the key 'g' group:
            svg.transition().attr({ width: width, height: height });
            svg.select(".container-group")
                .attr({ transform: "translate(" + margin.left + "," + margin.top + ")" });


            var circles = svg.select(".plantilla-line-chart-group")
                .selectAll(".circle")
                .data(_lineData);

            circles.enter().append('circle').classed("circle", true)
                .style("stroke", "#746e70")
                .style("fill", "white")
                .attr('cx', function(d, i) {
                    return x1(labels[i]) + (barW / 2);
                })
                .attr('cy', function(d) { return y1(d); })
                .attr('r', 3);

            var circlesText = svg.select(".plantilla-line-chart-group")
                .selectAll(".texts")
                .data(_lineData);

            circlesText.enter().append('text').classed("texts", true)
                .attr({
                    x: function(d, i) { return x1(labels[i]) + gapSize / 2 + barW / 2; },
                    y: function(d, i) { return y1(d) + 15; }
                })
                .attr('text-anchor', 'start')
                .text(function(d, i) {
                    return d.toLocaleString("es-ES");

                })
                .style("font-size", "10px");
            // .on('mouseover', function(d,i){tip.show(d,i);  d3.select(this).style("stroke", "red");})
            // .on('mouseout', function(d,i){tip.hide(d,i); d3.select(this).style("stroke", "grey");})

            // Select all bars and bind data:
            var bars = svg.select(".plantilla-bar-chart-group")
                .selectAll(".bar")
                .data(_data);


            var back_bars = svg.select(".plantilla-bar-chart-group")
                .selectAll(".back-bar")
                .data(labels);

            // D3 UPDATE code for bars
            back_bars.enter().append("rect")
                .classed("back-bar", true)
                .attr({
                    width: barW,
                    x: function(d, i) { return x1(labels[i]) + gapSize / 2; },
                    y: 0,
                    height: function(d, i) { return chartH; }
                })
                .style("fill", "grey")
                .style("fill-opacity", "0.2")
                .on('mouseover', function(d, i) { tip.show(d, i); })
                .on('mouseout', function(d, i) { tip.hide(d, i); });

            // ENTER, UPDATE and EXIT CODE:
            // D3 ENTER code for bars!
            bars.enter().append("g").append("rect")
                .classed("bar _selected_", true)
                .attr({
                    width: barW,
                    x: function(d, i) { return x1(labels[i]) + gapSize / 2; },
                    y: function(d, i) { return y1(d); },
                    height: function(d, i) { return chartH - y1(d); }
                })
                .style("fill", function(d, i) {
                    if (labels[i] == currentMonth) {
                        // if(i == (data.length-1)){
                        return "#ffcc03";
                    } else {
                        return "#83959f";
                    }
                })
                .on('mouseover', function(d, i) {
                    tip.show(d, i);
                    d3.select(this)
                        .style("stroke-width", "2")
                        .style("stroke-linejoin", "round")
                        .style("stroke", "#FA6006");
                })
                .on('mouseout', function(d, i) { tip.hide(d, i);
                    d3.select(this).style("stroke", "none"); })
                .on("click", function(d, i) {
                    tip.hide(d, i);
                    onClickFunction(i);
                });

            var barText = bars.append('text')
                .attr({
                    x: function(d, i) { return x1(labels[i]) + gapSize / 2 + barW / 2; },
                    y: function(d, i) { return y1(d) - 2; }
                })
                .attr('text-anchor', 'middle')
                .text(function(d, i) {
                    return d.toLocaleString("es-ES");

                })
                .style("font-size", "10px");


            svg.call(tip);


            // bars.exit().transition().style({opacity: 0}).remove();


            updateData = function() {
                _data = data;
                _labels = labels;

                bars = svg.select(".bar-chart-group")
                    .selectAll(".bar")
                    .data(_data);

                x1 = d3.scale.ordinal()
                    .domain(labels.map(function(d) { return d; }))
                    .rangeRoundBands([0, chartW], 0.2);

                y1 = d3.scale.linear()
                    .domain([minimum, d3.max(_data, function(d, i) { return d + d * .1; })])
                    .range([chartH, 0]);

                yAxis = d3.svg.axis()
                    .scale(y1)
                    .orient("left")
                    .innerTickSize(-chartW)
                    .ticks(3)
                    .outerTickSize(20)
                    .tickPadding(40)
                    .tickFormat(function(d) { return "" });

                yAxis1 = d3.svg.axis()
                    .scale(y1)
                    .orient('left')
                    .ticks(3)
                    .outerTickSize(0)
                    .tickPadding(0)
                    .tickSize(0)
                    .tickFormat(function(d, i) {
                        if (i == 0) return "";
                        else return d.toLocaleString("es-ES") + " " + dataUnit;
                    });

                yAxis2 = d3.svg.axis()
                    .scale(y1)
                    .orient("left")
                    .innerTickSize(0)
                    .ticks(3)
                    .tickSize(60)
                    .tickPadding(60)
                    .tickFormat(function(d) { return "" });


                svg.selectAll("g .y.axis").transition().call(yAxis);
                svg.selectAll("g .y.axis1").transition().call(yAxis1);
                svg.selectAll("g .y.axis2").transition().call(yAxis2);

                gapSize = x1.rangeBand() / 100 * gap;
                barW = x1.rangeBand() - gapSize;

                line = d3.svg.line()
                    .x(function(d, i) { return x1(labels[i]) + (barW / 2); })
                    .y(function(d) { return y1(d); })
                    .interpolate("monotone");

                svg.select(".line")
                    .transition()
                    .ease(ease)
                    .duration(1250)
                    .attr("d", line(_lineData));

                circles = svg.select(".plantilla-line-chart-group")
                    .selectAll(".circle")
                    .data(_lineData);

                circles.transition()
                    .ease(ease)
                    .duration(1250)
                    .attr('cx', function(d, i) {
                        return x1(labels[i]) + (barW / 2);
                    })
                    .attr('cy', function(d) { return y1(d); });

                // data that needs DOM = enter() (a set/selection, not an event!)
                bars = svg.select(".plantilla-bar-chart-group")
                    .selectAll(".bar")
                    .data(_data);

                // D3 UPDATE code for bars
                bars.transition().ease(ease).duration(1000)
                    .attr({
                        width: barW,
                        x: function(d, i) { return x1(labels[i]) + gapSize / 2; },
                        y: function(d, i) { return y1(d); },
                        height: function(d, i) { return chartH - y1(d); }
                    });

                barText.data(_data).transition().ease(ease).duration(1000)
                    .attr('y', function(d, i) { return y1(d) - 2; })
                    .text(function(d, i) {
                        if (d > 0 & d < 1) {
                            return d.toFixed(4).toLocaleString("es-ES");;
                        } else if (d >= 1) {
                            return d.toFixed(2).toLocaleString("es-ES");
                        } else {
                            return "";
                        }
                    });

                bars.exit()
                    .transition()
                    .duration(300)
                    .attr("y", y1(0))
                    .attr("height", chartH - y1(0))
                    .style('fill-opacity', 1e-6)
                    .remove();

                // bars.enter().append("rect")
                //     .classed("bar", true)
                //     .attr(
                //         {x: chartW,
                //         width: barW,
                //         y: function(d, i) { return y1(d); },
                //         height: function(d, i) { return chartH - y1(d);
                //         }
                //     });

                // D3 EXIT code for bars
                bars.exit().transition().style({ opacity: 0 }).remove();

            }

            updateWidth = function() {
                chartW = width - margin.left - margin.right;

                x1 = d3.scale.ordinal()
                    .domain(labels.map(function(d) { return d; }))
                    .rangeRoundBands([0, chartW], 0.2);

                xAxis.scale(x1);

                svg.select('.x.axis')
                    .transition()
                    .call(xAxis);

                svg.select('.y.axis')
                    .transition()
                    .call(yAxis);

                svg.select('.x.axis.mark line')
                    .transition()
                    .attr('x2', chartW);

                gapSize = x1.rangeBand() / 100 * gap;
                barW = x1.rangeBand() - gapSize;

                line.x(function(d, i) { return x1(labels[i]) + (barW / 2); })

                svg.selectAll('rect.bar')
                    .attr('width', barW)
                    .attr('x', function(d, i) { return x1(labels[i]) + gapSize / 2; });

                svg.selectAll('rect.back-bar')
                    .attr('width', barW)
                    .attr('x', function(d, i) { return x1(labels[i]) + gapSize / 2; });

                svg.select(".line")
                    .transition()
                    .ease(ease)
                    .duration(1250)
                    .attr("d", line(_lineData));

                svg.selectAll('circle')
                    .transition()
                    .attr('cx', function(d, i) { return x1(labels[i]) + (barW / 2); });

                barText.data(_data).transition().ease(ease).duration(1000)
                    .attr('x', function(d, i) { return x1(labels[i]) + gapSize / 2 + barW / 2; });

                circlesText.data(_lineData).transition().ease(ease).duration(1000)
                    .attr('x', function(d, i) { return x1(labels[i]) + gapSize / 2 + barW / 2; });




                svg.transition().attr({ width: width, height: height });
            }
        });
    }

    // GETTERS AND SETTERS:


    exports.data = function(_x) {
        if (!arguments.length) return data;
        data = _x;
        if (typeof updateData === 'function') updateData();
        return this;
    };

    exports.minimum = function(_x) {
        if (!arguments.length) return minimum;
        minimum = _x;
        if (typeof updateData === 'function') updateData();
        return this;
    };

    exports.lineData = function(_x) {
        if (!arguments.length) return lineData;
        lineData = _x;
        // if (typeof updateData === 'function') updateData();
        return this;
    };

    exports.onClickFunction = function(value) {
        if (!arguments.length) return onClickFunction;
        onClickFunction = value;
        return this;
    };

    exports.labels = function(_x) {
        if (!arguments.length) return labels;
        labels = _x;
        return this;
    };

    exports.width = function(_x) {
        if (!arguments.length) return width;
        width = parseInt(_x);
        if (typeof updateWidth === 'function') updateWidth();
        return this;
    };

    exports.dataTitle = function(_x) {
        if (!arguments.length) return dataTitle;
        dataTitle = _x;
        return this;
    };

    exports.dataUnit = function(_x) {
        if (!arguments.length) return dataUnit;
        dataUnit = _x;
        return this;
    };

    exports.currentMonth = function(_x) {
        if (!arguments.length) return currentMonth;
        currentMonth = _x;
        return this;
    };

    exports.setColor = function(_x) {
        if (!arguments.length) return color;
        color = _x;
        return this;
    };

    exports.height = function(_x) {
        if (!arguments.length) return height;
        height = parseInt(_x);
        return this;
    };
    exports.gap = function(_x) {
        if (!arguments.length) return gap;
        gap = _x;
        return this;
    };
    exports.ease = function(_x) {
        if (!arguments.length) return ease;
        ease = _x;
        return this;
    };

    exports.margin = function(_x) {
        if (!arguments.length) return margin;
        margin = _x;
        return this;
    };

    return exports;
};