
// Bar chart Module
/////////////////////////////////

// Declare namespace


// Declare component: (this outer function acts as the closure):
d3.cloudshapes.barChart = function module() {
    var margin = {top: 20, right: 20, bottom: 40, left: 40},
        width = 500,
        height = 500,
        gap = 0,
        color = "#001ff0"
        ease = "bounce";

    var data = [];
    var svg;

    var updateData;

    // Define the 'inner' function: which, through the surreal nature of JavaScript scoping, can access
    // the above variables.
    function exports(_selection) {
        _selection.each(function() {
            _data = data;
            var chartW = width - margin.left - margin.right,
                chartH = height - margin.top - margin.bottom;

      // Define x and y scale variables.
            var x1 = d3.scale.ordinal()
                    .domain(_data.map(function(d, i) { return i; }))
                    .rangeRoundBands([0, chartW], 0.1);

            var y1 = d3.scale.linear()
                    .domain([0, d3.max(_data, function(d, i) { return d; })])
                    .range([chartH, 0]);



      // If no SVG exists, create one - and add key groups:
            if (!svg) {
                svg = d3.select(this)
                    .append("svg")
                    .classed("barchart", true);
                var container = svg.append("g").classed("container-group", true);
                container.append("g").classed("bar-chart-group", true);
            }

            // Transition the width and height of the main SVG and the key 'g' group:
            svg.transition().attr({width: width, height: height});
            svg.select(".container-group")
                .attr({transform: "translate(" + margin.left + "," + margin.top + ")"});

            // Define gap between bars:
            var gapSize = x1.rangeBand() / 100 * gap;

            // Define width of each bar:
            var barW = x1.rangeBand() - gapSize;

            // Select all bars and bind data:
            var bars = svg.select(".bar-chart-group")
                    .selectAll(".bar")
                    .data(_data);


            // ENTER, UPDATE and EXIT CODE:
            // D3 ENTER code for bars!
            bars.enter().append("rect")
                .classed("bar", true)
                .attr(
                    {x: chartW,
                    width: barW,
                    y: function(d, i) { return y1(d); },
                    height: function(d, i) { return chartH - y1(d);
                    }
                }).style("fill", color);

      // D3 UPDATE code for bars
            bars.transition()
                .ease(ease)
                .attr({
                    width: barW,
                    x: function(d, i) { return x1(i) + gapSize / 2; },
                    y: function(d, i) { return y1(d); },
                    height: function(d, i) { return chartH - y1(d); }
                });

      // D3 EXIT code for bars
            bars.exit().transition().style({opacity: 0}).remove();


            updateData = function() {
                _data = data;
                bars = svg.select(".bar-chart-group")
                    .selectAll(".bar")
                    .data(_data);
                x1 = d3.scale.ordinal()
                    .domain(_data.map(function(d, i) { return i; }))
                    .rangeRoundBands([0, chartW], 0.1);
                 y1 = d3.scale.linear()
                    .domain([0, d3.max(_data, function(d, i) { return d; })])
                    .range([chartH, 0]);
                gapSize = x1.rangeBand() / 100 * gap;

                // Define width of each bar:
                barW = x1.rangeBand() - gapSize;
                // data that needs DOM = enter() (a set/selection, not an event!)
                bars.enter().append("rect")
                    .attr("class", "bar")
                    .attr("y", y1(0))
                    .attr("height", height - y1(0));


          // D3 UPDATE code for bars
                bars.transition().ease(ease).duration(1000)
                    .attr({
                        width: barW,
                        x: function(d, i) { return x1(i) + gapSize / 2; },
                        y: function(d, i) { return y1(d); },
                        height: function(d, i) { return chartH - y1(d); }
                });

                bars.exit()
                    .transition()
                    .duration(300)
                    .attr("y", y1(0))
                    .attr("height", chartH - y1(0))
                    .style('fill-opacity', 1e-6)
                    .remove();
                // bars.enter().append("rect")
                //     .classed("bar", true)
                //     .attr(
                //         {x: chartW,
                //         width: barW,
                //         y: function(d, i) { return y1(d); },
                //         height: function(d, i) { return chartH - y1(d);
                //         }
                //     });

          // D3 EXIT code for bars
                bars.exit().transition().style({opacity: 0}).remove();

            }

        });
    }

    exports.data = function(_x) {
        if (!arguments.length) return data;
        data = _x;
        if (typeof updateData === 'function') updateData();
        return this;
    };



    // GETTERS AND SETTERS:
    exports.width = function(_x) {
        if (!arguments.length) return width;
        width = parseInt(_x);
        return this;
    };

    exports.setColor = function(_x) {
        if (!arguments.length) return color;
        color = _x;
        return this;
    };

    exports.height = function(_x) {
        if (!arguments.length) return height;
        height = parseInt(_x);
        return this;
    };
    exports.gap = function(_x) {
        if (!arguments.length) return gap;
        gap = _x;
        return this;
    };
    exports.ease = function(_x) {
        if (!arguments.length) return ease;
        ease = _x;
        return this;
    };

    return exports;
};

