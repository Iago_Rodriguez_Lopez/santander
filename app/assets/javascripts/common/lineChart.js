
// Bar chart Module
/////////////////////////////////

// Declare namespace


// Declare component: (this outer function acts as the closure):
d3.cloudshapes.lineChart = function module() {
    var margin = {top: 20, right: 20, bottom: 40, left: 40},
        width = 500,
        height = 500,
        gap = 0,
        color = "#001ff0"
        ease = "bounce";


    var data = [];
    var svg;

    var updateData;

    // Define the 'inner' function: which, through the surreal nature of JavaScript scoping, can access
    // the above variables.
    function exports(_selection) {
        _selection.each(function() {
            _data = data;

            var chartW = width - margin.left - margin.right,
                chartH = height - margin.top - margin.bottom;

      // Define x and y scale variables.
            var x1 = d3.scale.ordinal()
                    .domain(_data.map(function(d, i) { return i; }))
                    .rangeRoundBands([0, chartW], 0.1);

            var y1 = d3.scale.linear()
                    .domain([0, d3.max(_data, function(d, i) { return d; })])
                    .range([chartH, 0]);



      // If no SVG exists, create one - and add key groups:
            if (!svg) {
                svg = d3.select(this)
                    .append("svg")
                    .classed("linechart", true);
                var container = svg.append("g").classed("container-group", true);
                container.append("g").classed("line-chart-group", true);
            }

      // Transition the width and height of the main SVG and the key 'g' group:
            svg.transition().attr({width: width, height: height});
            svg.select(".container-group")
                .attr({transform: "translate(" + margin.left + "," + margin.top + ")"});


            var line = d3.svg.line()
                .x(function(d,i) {
                    return x1(i); })
                .y(function(d) { return y1(d); });

            // Select all bars and bind data:
            var path = svg.select(".line-chart-group")
                .append("path")       // Add the valueline path.
                .attr("class", "line")
                .attr("d", line(_data))
                .style("stroke", color);

            updateData = function() {
                _data = data;
                chartW = width - margin.left - margin.right,
                chartH = height - margin.top - margin.bottom;
                y1 = d3.scale.linear()
                    .domain([0, d3.max(_data, function(d, i) { return d; })])
                    .range([chartH, 0]);

                path = path.transition().ease(ease).duration(1000).attr("d", line(_data));

            }


        });
    }

    exports.data = function(_x) {
        if (!arguments.length) return data;
        data = _x;
        if (typeof updateData === 'function') updateData();
        return this;
    };

    // GETTERS AND SETTERS:
    exports.width = function(_x) {
        if (!arguments.length) return width;
        width = parseInt(_x);
        return this;
    };

    exports.setColor = function(_x) {
        if (!arguments.length) return color;
        color = _x;
        return this;
    };

    exports.height = function(_x) {
        if (!arguments.length) return height;
        height = parseInt(_x);
        return this;
    };
    exports.gap = function(_x) {
        if (!arguments.length) return gap;
        gap = _x;
        return this;
    };
    exports.ease = function(_x) {
        if (!arguments.length) return ease;
        ease = _x;
        return this;
    };

    return exports;
};

