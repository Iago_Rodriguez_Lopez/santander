// Stacked Bar chart Module
/////////////////////////////////
// EJEMPLO CON CSV
// https://bl.ocks.org/mbostock/3886208

d3.cloudshapes.stackedBarChartSantander = function module() {
    var margin = { top: 20, right: 60, bottom: 40, left: 20 },
        width = 500,
        height = 500,
        gap = 0.1,
        ease = "bounce";

    var data = [];
    var stackedData = [];
    var labels = [];
    var dataTitle = "";
    var dataUnit = "";
    var onClickFunction = function() {};
    var labelRotation = 0;
    var svg;

    var updateData;
    var updateWidth2;

    var tip = d3.tip()
        .attr('class', 'd3-tip')
        .offset([-10, 0])
        .html(function(d, i) {
            return "<h4><strong>" + labels[i] +
                "</strong></h4><div class='content'><table><tr><td>" +
                dataTitle + " " + d.key + "s " + ":</td><td style='font-size: 18px'>" +
                d.value + " " + dataUnit.toLocaleString("es-ES") + "</td></tr></table></div>";
        });

    // Define the 'inner' function: which, through the surreal nature of JavaScript scoping, can access
    // the above variables.
    function exports(_selection) {
        _selection.each(function() {

            var color = d3.scale.linear()
                .domain([0, data[0].length])
                .range(["#f7b006", "#F75206"]);

            data.forEach(function(d, i) {
                d.forEach(function(d, j) {
                    if (stackedData[i] === undefined) {
                        stackedData[i] = 0;
                    }
                    stackedData[i] = stackedData[i] + d.value;
                });
            });

            var chartW = width - margin.left - margin.right,
                chartH = height - margin.top - margin.bottom;

            // Define x and y scale variables.
            var x1 = d3.scale.ordinal()
                .domain(labels.map(function(d) { return d; }))
                .rangeRoundBands([0, chartW], 0.2);

            var y1 = d3.scale.linear()
                .domain([0, d3.max(stackedData, function(d, i) { return d; })])
                .range([chartH, 0]);

            var xAxis = d3.svg.axis()
                .scale(x1)
                .innerTickSize(0)
                .outerTickSize(1)
                .tickPadding(10)
                .orient("bottom");

            var yAxis = d3.svg.axis()
                .scale(y1)
                .orient("right")
                .innerTickSize(chartW)
                .ticks(10)
                .outerTickSize(5)
                .tickPadding(5);

            // If no SVG exists, create one - and add key groups:
            if (!svg) {
                svg = d3.select(this)
                    .append("svg")
                    .classed("santander-stackedBarchart", true);
                var container = svg.append("g").classed("container-group", true);
            }

            container.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + chartH + ")")
                .call(xAxis);

            container.select(".x").selectAll("text")
                .style("text-anchor", function() {
                    if (labelRotation === 0) {
                        return "middle";
                    } else {
                        return "end";
                    }
                })
                .attr("transform", function(d) {
                    return "rotate(-" + labelRotation + ")";
                });

            // container.append("g")
            //   .attr("class", "y axis")
            //   .attr("transform", "translate(0,0)")
            //   .call(yAxis);

            // container.append("g")
            //     .attr("class", "x axis mark")
            //     .append('line')
            //     .attr("class", "stackedRowChart-x-axis")
            //     .attr('x2', chartW)
            //     .attr('x1', 0)
            //     .attr('y2', chartH)
            //     .attr('y1', chartH)
            //     .style('stroke', "black")
            //     .style('stroke-width', 1)
            //     ;

            // container.append("g")
            //     .attr("class", "y axis mark")
            //     .append('line')
            //     .attr("class", "group-name-line")
            //     .attr('x2', 0)
            //     .attr('x1', 0)
            //     .attr('y2', 0)
            //     .attr('y1', chartH)
            //     .style('stroke', "black")
            //     ;

            container.append("g").classed("stackedbar-chart-group", true);

            // Transition the width and height of the main SVG and the key 'g' group:
            svg.transition().attr({ width: width, height: height });
            svg.select(".container-group")
                .attr({ transform: "translate(" + margin.left + "," + margin.top + ")" });

            // Define gap between bars:
            var gapSize = x1.rangeBand() / 100 * gap;

            // Define width of each bar:
            var barW = x1.rangeBand() - gapSize;

            var bars = svg.select(".stackedbar-chart-group")
                .selectAll(".stackedBar");

            data.forEach(function(d, i) {
                var aux = 0;
                var index = i;

                bars
                    .data(d)
                    .enter()
                    .append("g")
                    .append("rect")
                    .attr('class', function(d, i) {
                        return 'bar' + index + '.' + d.key + '._selected_';
                    })
                    .attr('width', barW)
                    .attr('x', x1(labels[i]) + gapSize / 2)
                    .attr('y', function(d, i) {
                        aux = aux + d.value;
                        return y1(aux);
                    })
                    .attr('height', function(d, i) { return chartH - y1(d.value); })
                    .on('mouseover', function(d, i) {
                        tip.show(d, index);
                        d3.select(this).style("stroke", "red");
                    })
                    .on('mouseout', function(d, i) {
                        tip.hide(d, index);
                        d3.select(this).style("stroke", "none");
                    })
                    .on('click', function(d) {

                        if (d3.select(this.parentNode).classed("_selected_")) {
                            d3.select(this.parentNode).classed("_selected_", false);
                        } else {
                            d3.select(this.parentNode).classed("_selected_", true);
                        }
                        onClickFunction(this, d.value);
                    })
                    .style("fill", function(d, i) {
                        return color(i);
                    });
            });

            var barText = container.selectAll('.label')
                .data(stackedData)
                .enter()
                .append('text')
                .style('font-size', '1.2em')
                .attr('class', 'label')
                .attr({
                    x: function(d, i) { return x1(labels[i]) + gapSize / 2 + barW / 2; },
                    y: function(d, i) { return y1(d) - 2; }
                })
                .attr('text-anchor', 'middle')
                .text(function(d, i) {
                    if (d > 0 & d < 1)
                        return d.toFixed(4);
                    else
                        return d.toLocaleString("es-ES");
                });

            svg.call(tip);

            // updateWidth2 = function() {
            //     alert("ENTRA");
            //     chartW = width - margin.left - margin.right;

            //     x1 = d3.scale.ordinal()
            //         .domain(labels.map(function(d) { return d; }))
            //         .rangeRoundBands([0, chartW], 0.2);

            //     xAxis = d3.svg.axis()
            //         .scale(x1)
            //         .orient("bottom");

            //     gapSize = x1.rangeBand() / 100 * gap;
            //     barW = x1.rangeBand() - gapSize;

            //     svg.selectAll("g .x.axis")
            //         .transition()
            //         .attr("transform", "translate(0," + chartH + ")")
            //         .call(xAxis);

            //     container.select("stackedRowChart-x-axis")
            //         .transition()
            //         .attr('x2', chartW)
            //         ;

            //     svg.select(".stackedRowChart-x-axis")
            //         .transition()
            //         .attr('x2', chartW)
            //         .attr('x1', 0);

            //     svg.selectAll("g .y.axis").call(yAxis);

            //     data.forEach( (d,i) => {
            //         var index = i;
            //         svg.selectAll('rect.bar'+i)
            //             .transition()
            //             .attr('width', barW)
            //             .attr('x', (d, i) => {
            //                 return x1(labels[i]) + gapSize / 2;;
            //             })
            //             ;
            //     });

            //     container.select(".x").selectAll("text")
            //         .style("text-anchor", function(){
            //             if (labelRotation == 0){
            //                 return "middle";
            //             }else{
            //                 return "end";
            //             }
            //         })
            //         .attr("transform", function(d) {
            //             return "rotate(-"+labelRotation+")";
            //         });

            //     svg.transition().attr({width: width, height: height});
            // }


            updateData = function() {
                _data = data;
                _labels = labels;

                bars = svg.select(".stackedbar-chart-group")
                    .selectAll(".bar")
                    .data(_data);

                // x1 = d3.scale.ordinal()
                //     .domain(labels.map(function(d) { return d; }))
                //     .rangeRoundBands([0, chartW], 0.2);

                y1 = d3.scale.linear()
                    .domain([0, d3.max(_data, function(d, i) { return d; })])
                    .range([chartH, 0]);

                yAxis = d3.svg.axis()
                    .scale(y1)
                    .orient("left")
                    .innerTickSize(-chartW)
                    .ticks(3)
                    .outerTickSize(20)
                    .tickPadding(40)
                    .tickFormat(function(d) { return "" });


                svg.selectAll("g .y.axis").call(yAxis);

                gapSize = x1.rangeBand() / 100 * gap;

                // Define width of each bar:
                barW = x1.rangeBand() - gapSize;
                // data that needs DOM = enter() (a set/selection, not an event!)
                bars.enter().append("rect")
                    .attr("class", "bar _selected_")
                    .attr("y", y1(0))
                    .attr("height", height - y1(0));


                // D3 UPDATE code for bars
                bars.transition().ease(ease).duration(1000)
                    .attr({
                        width: barW,
                        x: function(d, i) { return x1(labels[i]) + gapSize / 2; },
                        y: function(d, i) { return y1(d); },
                        height: function(d, i) { return chartH - y1(d); }
                    });

                bars.exit()
                    .transition()
                    .duration(300)
                    .attr("y", y1(0))
                    .attr("height", chartH - y1(0))
                    .style('fill-opacity', 1e-6)
                    .remove();
                // bars.enter().append("rect")
                //     .classed("bar", true)
                //     .attr(
                //         {x: chartW,
                //         width: barW,
                //         y: function(d, i) { return y1(d); },
                //         height: function(d, i) { return chartH - y1(d);
                //         }
                //     });

                // D3 EXIT code for bars
                bars.exit().transition().style({ opacity: 0 }).remove();
            }

        });
    }

    // GETTERS AND SETTERS

    exports.data = function(_x) {
        if (!arguments.length) return data;
        data = _x;
        if (typeof updateData === 'function') updateData();
        return this;
    };

    exports.width = function(_x) {
        if (!arguments.length) return width;
        width = parseInt(_x);
        if (typeof updateWidth2 === 'function') updateWidth2;
        return this;
    };

    exports.onClickFunction = function(value) {
        if (!arguments.length) return onClickFunction;
        onClickFunction = value;
        return this;
    };

    exports.labels = function(_x) {
        if (!arguments.length) return labels;
        labels = _x;
        return this;
    };

    exports.dataTitle = function(_x) {
        if (!arguments.length) return dataTitle;
        dataTitle = _x;
        return this;
    };

    exports.dataUnit = function(_x) {
        if (!arguments.length) return dataUnit;
        dataUnit = _x;
        return this;
    };

    exports.setColor = function(_x) {
        if (!arguments.length) return color;
        color = _x;
        return this;
    };

    exports.height = function(_x) {
        if (!arguments.length) return height;
        height = parseInt(_x);
        return this;
    };

    exports.gap = function(_x) {
        if (!arguments.length) return gap;
        gap = _x;
        return this;
    };

    exports.margin = function(_x) {
        if (!arguments.length) return margin;
        margin = _x;
        return this;
    };

    exports.labelRotation = function(_x) {
        if (!arguments.length) return labelRotation
        labelRotation = parseInt(_x);
        return this;
    }

    exports.ease = function(_x) {
        if (!arguments.length) return ease;
        ease = _x;
        return this;
    };

    return exports;
};