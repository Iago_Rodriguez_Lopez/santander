
// Bar chart Module
/////////////////////////////////

// Declare namespace


// Declare component: (this outer function acts as the closure):
d3.cloudshapes.donutChart = function module() {
    var margin = {top: 0, right: 20, bottom: 40, left: 0},
        width = 500,
        height = 500,
        gap = 0,
        color = "#001ff0"
        ease = "bounce",
        radius = 0;

    var colors = d3.scale.category20(), svg;
    var data = [];
    var svg;

    var updateData;


    // Define the 'inner' function: which, through the surreal nature of JavaScript scoping, can access
    // the above variables.
    function exports(_selection) {
        _selection.each(function() {
            var _data = data;
            var chartW = width - margin.left - margin.right,
                chartH = height - margin.top - margin.bottom;
                radius = Math.min(chartW, chartH) / 2;
            // Define x and y scale variables.
            var x1 = d3.scale.ordinal()
                    .domain(_data.map(function(d, i) { return i; }))
                    .rangeRoundBands([0, chartW], 0.1);

            var y1 = d3.scale.linear()
                    .domain([0, d3.max(_data, function(d, i) { return d; })])
                    .range([chartH, 0]);



      // If no SVG exists, create one - and add key groups:
            if (!svg) {
                svg = d3.select(this)
                    .append("svg")
                    .classed("donutchart", true);
                var container = svg.append("g")
                    .classed("container-group", true);
                var pie_container= container.append("g")
                    .classed("donut-chart-group", true);
            }

      // Transition the width and height of the main SVG and the key 'g' group:
            svg.transition().attr({width: chartW, height: chartH});
            svg.select(".container-group")
                .attr({transform: "translate(" + (margin.left+chartW/2) + "," + (margin.top+chartH/2) + ")"});
            svg.select(".donut-chart-group");
                // .attr("transform", "translate(" + chartW/2 + "," + chartH/2  + ")")


            var pie = d3.layout.pie()
                .value(function(d) { return d; })
                .sort(null);

            var arc = d3.svg.arc()
                .innerRadius(radius)
                .outerRadius(10);

            var path = svg.select(".donut-chart-group").selectAll("path")
                .data(pie(_data));

            path.enter().append("path")
                .attr("fill", function(d, i) { return colors(i); })
                .attr("d", arc)
                .each(function(d) {this._current = d;} );

            path.transition()
                  .attrTween("d", arcTween);

           // path.exit().transition().duration(1000).remove()

            function arcTween(a) {
              var i = d3.interpolate(this._current, a);
              this._current = i(0);
              return function(t) {
                return arc(i(t));
              };
            }




            updateData = function() {
                _data = data;
                pie.value(function(d) { return d; }).sort(null);

                path = svg.select(".donut-chart-group").selectAll("path").data(pie(_data));

                path.exit().transition().duration(1000).remove();

                path.enter().append("path")
                    .attr("fill", function(d, i) { return colors(i); })
                    .attr("d", arc)
                    .each(function(d) {this._current = d;} );

                path.transition().duration(1000)
                    .attrTween("d", arcTween);

                path.enter().append("path")
                    .attr("fill", function(d, i) { return colors(i); })
                    .attr("d", arc)
                    .each(function(d) {this._current = d;} );

            }


        });
    }


    // GETTERS AND SETTERS:
    exports.width = function(_x) {
        if (!arguments.length) return width;
        width = parseInt(_x);
        return this;
    };

    exports.data = function(_x) {
        if (!arguments.length) return data;
        data = _x;
        if (typeof updateData === 'function') updateData();
        return this;
    };

    exports.setColor = function(_x) {
        if (!arguments.length) return color;
        color = _x;
        return this;
    };

    exports.height = function(_x) {
        if (!arguments.length) return height;
        height = parseInt(_x);
        return this;
    };
    exports.gap = function(_x) {
        if (!arguments.length) return gap;
        gap = _x;
        return this;
    };
    exports.ease = function(_x) {
        if (!arguments.length) return ease;
        ease = _x;
        return this;
    };

    return exports;
};

