
// Bar chart Module
/////////////////////////////////

// Declare namespace


// Declare component: (this outer function acts as the closure):
d3.cloudshapes.gridBarChartSantander = function module() {
    var margin = {top: 60, right: 20, bottom: 40, left: 20},
        innerPaddingLeft = 20,
        innerPaddingTop = 100,
        width = 500,
        height = 500,
        gap = 0.1,
        color = "#001ff0",
        ease = "bounce";

    var padding = {top: 0, right: 0, bottom: 0, left: 0};
    var legendX = 0;
    var legendY= 0;
    var selectable = true;
    var data = [];
    var labels = [];
    var titles = [];
    var titleSize = 20;
    var rows = 0;
    var columns = 3;
    var dataTitle = "";
    var dataUnit = "";
    var onClickFunction = function(){};
    var svg;
    var labelRotation = 0;
    var labelSize = 12;
    var updateData;
    var updateLinesData;
    var updateWidth;
    var clearSelection;

    var tip = d3.tip()
      .attr('class', 'd3-tip')
      .offset([-10, 0])
      .html(function(d,i) {
        if (d > 0 & d < 1) d = d.toFixed(2);
        return "<h4><td style='font-size: 18px'>"+d.toLocaleString()+" "+ dataUnit.toLocaleString() +"</td></div>";
      });


    // Define the 'inner' function: which, through the surreal nature of JavaScript scoping, can access
    // the above variables.
    function exports(_selection) {
        _selection.each(function() {
            _data = data;
            _flatData = _.flatten(_data);
            _labels = labels;
            _titles = titles;
            rows = Math.floor(data.length/columns);

            var chartW = width - margin.left - margin.right,
                chartH = height - margin.top - margin.bottom;

            miniChartW = (chartW-(innerPaddingLeft*2))/columns;
            //miniChartH = (chartH-(innerPadding*6))/(rows+2);

            miniChartH = (chartH-(innerPaddingTop*3))/(rows+1);
            // Define x and y scale variables.
            //
            var x0 = d3.scale.ordinal()
                    .domain(labels.map(function(d,i) { return i; }))
                    .range([0, chartW]);


            var y1 = d3.scale.linear()
                    .domain([0, d3.max(_flatData)])
                    .range([miniChartH, 0]);

            // If no SVG exists, create one - and add key groups:
            if (!svg) {
                svg = d3.select(this)
                    .append("svg")
                    .classed("santander-gridbarchart", true);
                if (selectable === false){
                    svg.classed("unselectable", true)
                }
            }
            svg.transition().attr({width: width, height: height});

            var container = svg.append("g").classed("container-group", true);
            svg.select(".container-group")
                .attr({transform: "translate(" + margin.left + "," + margin.top + ")"});


            _data.forEach(function(d,j){
                minicontainer = container.append("g").classed("mini-container mini-container-"+j, true);
                minichart =  minicontainer.append("g").classed("mini-bar-chart", true);
                minicontainer.attr({transform: "translate(" + ((miniChartW+innerPaddingLeft)*(j%columns)) + "," + ((miniChartH+innerPaddingTop)*Math.floor(j/columns)) + ")"});
                minichart.attr({transform: "translate(" + 0 + "," + 10 + ")"});

                var title = minicontainer.append("text")
                    .attr({
                        x: 0,
                        y: (-5)
                    })
                    .attr('text-anchor', 'left')
                    .style("font-size", (labelSize+2))
                    .style("font-weight", "bold")
                    .attr("stroke-width", 2)
                    .text(titles[j]);


                var x1 = d3.scale.ordinal()
                    .domain(labels[j].map(function(d) { return d; }))
                    .rangeRoundBands([0, miniChartW], 0.2);

                var y1 = d3.scale.linear()
                    .domain([0, d3.max(data[j])])
                    .range([miniChartH, 0]);

                // Define gap between bars:
                var gapSize = x1.rangeBand() / 100 * gap;
                // Define width of each bar:
                // var barW = d3.min([(x1.rangeBand() - gapSize), 20]);
                var barW = x1.rangeBand() - gapSize;

                // Select all bars and bind data:


                var back_bars = minichart.selectAll(".back-bar").data(_data[j]);


                 var bars = minichart.selectAll(".bar").data(_data[j]);


                // D3 UPDATE code for bars
                back_bars.enter().append("rect")
                    .classed("back-bar", true)
                    .attr("class", function(d,i){return "back-bar back-bar-"+i})
                    .attr({
                        width: barW,
                        x: function(d, i) { return x1(labels[j][i]) },
                        y: 0,
                        height: function(d, i) { return miniChartH; }
                    })
                    .style("fill", "grey")
                    .style("fill-opacity", "0.3")
                    .on('mouseover', function(d,i) { tip.show(d,i);})
                    .on('mouseout', function(d,i) {
                        tip.hide(d,i);
                        d3.select(this).style("stroke", "none");
                    });

                // ENTER, UPDATE and EXIT CODE:
                // D3 ENTER code for bars!
                var gBars = bars.enter().append("g").attr("data-group", j).attr("data-elem", function(d,i){ return i }).classed('selection-bar _selected_', true);

                gBars.append("rect")
                    .attr("class", function(d,i){return "bar mini-chart-"+j+" bar-"+i})
                    .attr({
                        width: barW,
                        x: function(d, i) { return x1(labels[j][i]) },
                        y: function(d, i) {
                            return y1(d); },
                        height: function(d, i) { return miniChartH - y1(d); }
                    })
                    .on('mouseover', function(d,i) {tip.show(d,i);  d3.select(this).style("stroke", "red");})
                    .on('mouseout', function(d,i) {
                        tip.hide(d,i);
                        d3.select(this).style("stroke", "none");
                    })
                    .on("click", function(d, i) {
                        if (selectable) {
                            var index = i;
                            container.selectAll(".selection-bar").classed('_selected_', function(d,i) {
                                if ($(this).data("elem") == index && $(this).data("group") == j){
                                    return true;
                                }else{
                                    return false;
                                }
                            });
                            container.selectAll('.x.axis')
                                .selectAll('text')
                                .style('opacity', function(d, i) {
                                    if (i === index && $(this).parent().parent().data("group") == j) {
                                        return 1;
                                    } else {
                                        return 0.3;
                                    }
                                });

                            onClickFunction(this, d,labels[j][i]);
                        }
                    })
                    .style("fill", color);


                var barText = gBars.append('text')
                    .attr({
                        x: function(d, i) { return x1(labels[j][i]) + barW /2; },
                        y: function(d, i) { return y1(d) - 2; }
                    })
                    .attr('text-anchor', 'middle')
                    .style("font-size", labelSize)
                    .attr("data-group", j)
                    .attr("data-elem", function(d,i){ return i })
                    .text(function(d, i) {
                        if (d > 0 & d < 1)
                            return d.toFixed(2);
                        else
                            return d.toLocaleString();
                    });



                    var xAxis = d3.svg.axis()
                        .scale(x1)
                        .orient("bottom")
                        .innerTickSize(2)
                        .outerTickSize(2)
                        .tickPadding(5)
                        .tickFormat(function(d){
                             if(d.length > 20)
                                 return d.substring(0,20)+'...';
                             else
                                 return d;
                        });



                    var yAxis = d3.svg.axis()
                        .scale(y1)
                        .orient("left")
                        .innerTickSize(-miniChartW)
                        .ticks(2)
                        .tickPadding(10)
                        .tickFormat(function (d){ return d.toLocaleString(); });



                    minichart.append("g")
                      .attr("class", "x axis")
                      .attr("data-group", j)
                      .attr("transform", "translate(0," + miniChartH + ")")
                      .call(xAxis)


                    minichart.append("g")
                        .attr("class", "x axis mark")
                        .append('line')
                        .attr("class", "group-name-line")
                        .attr('x2', miniChartW)
                        .attr('x1', 0)
                        .attr('y2', miniChartH)
                        .attr('y1', miniChartH)
                        .style('stroke', "black")
                        ;


                minichart.select('.x.axis')
                    .selectAll('.tick text')
                    .on("click", function(d, i) {
                        if (selectable) {
                            var index = i;
                            container.selectAll(".selection-bar").classed('_selected_', function(d,i) {
                                if ($(this).data("elem") == index && $(this).data("group") == j){
                                    return true;
                                }else{
                                    return false;
                                }
                            });
                            container.selectAll('.x.axis')
                                .selectAll('text')
                                .style('opacity', function(d, i) {
                                    if (i === index && $(this).parent().parent().data("group") == j) {
                                        return 1;
                                    } else {
                                        return 0.3;
                                    }
                                });

                            onClickFunction(this, d,labels[j][i]);
                        }
                    })

                    .style('cursor', 'pointer')
                    .append("svg:title")
                    .text(function(d, i) {
                        return labels[j][i] //Or some other custom decription
                    });;



            })



            container.selectAll(".x").selectAll("text")
                .style("text-anchor", function(){
                    if (labelRotation == 0){
                        return "middle"
                    }else{
                        return "end"
                    }
                })
                .style("font-size", labelSize)
                // .attr("dx", "-.8em")
                // .attr("dy", ".15em")
                .attr("transform", function(d) {
                return "rotate(-"+labelRotation+")"
                });







            svg.call(tip);







            updateData = function() {
                _data = data;
                _flatData = _.flatten(_data);



                y1 = d3.scale.linear()
                    .domain([0, d3.max(_flatData)])
                    .range([miniChartH, 0]);


                _data.forEach(function(d,j){
                    minicontainer = container.select(".mini-container-"+j);
                    minichart =  minicontainer.selectAll(".mini-bar-chart");


                     y1 = d3.scale.linear()
                        .domain([0, d3.max(d)])
                        .range([miniChartH, 0]);

                    var x1 = d3.scale.ordinal()
                        .domain(labels[j].map(function(d) { return d; }))
                        .rangeRoundBands([0, miniChartW], 0.2);

                    var gapSize = x1.rangeBand() / 100 * gap;
                    var barW = x1.rangeBand() - gapSize;

                    bars = minichart.selectAll(".bar").data(_data[j]);


                    bars.transition().ease(ease).duration(1000)
                        .attr({
                            width: barW,
                            x: function(d, i) { return x1(labels[j][i]) + gapSize / 2; },
                            y: function(d, i) { return y1(d); },
                            height: function(d, i) { return miniChartH - y1(d); }
                    });

                    barText = minichart.selectAll(".selection-bar text");
                    barText.data(_data[j]).transition().ease(ease).duration(1000)
                        .attr('y', function(d, i) { return y1(d) - 2; })
                        .text(function(d, i) {
                            if (d > 0 & d < 1)
                                return d.toFixed(2);
                            else
                                return d.toLocaleString();
                        });






                    // var yAxis = d3.svg.axis()
                    //     .scale(y1)
                    //     .orient("left")
                    //     .innerTickSize(-miniChartW)
                    //     .ticks(2)
                    //     .tickPadding(10)
                    //     .tickFormat(function (d){ return d.toLocaleString(); });

                });

            }

            updateWidth = function() {


            }

            clearSelection = function() {
                container.selectAll(".selection-bar").classed('_selected_', true);
                container.selectAll('.x.axis')
                        .selectAll('text')
                        .style('opacity', 1);
                onClickFunction(svg);
            }
        });
    }

    exports.cleanFilter = function() {
        if (typeof clearSelection === 'function') clearSelection();
    }

    // GETTERS AND SETTERS:


    exports.data = function(_x) {
        if (!arguments.length) return data;
        data = _x;
        if (typeof updateData === 'function') updateData();
        return this;
    };


    exports.linesData = function(_x) {
        if (!arguments.length) return linesData;
        linesData = _x;
        if (typeof updateLinesData === 'function') updateLinesData();
        return this;
    };

    exports.onClickFunction = function(value) {
        if (!arguments.length) return onClickFunction;
        onClickFunction = value;
        return this;
    };

    exports.labels = function(_x) {
        if (!arguments.length) return labels;
        labels = _x;
        return this;
    };

    exports.width = function(_x) {
        if (!arguments.length) return width;
        width = parseInt(_x);
        if (typeof updateWidth === 'function') updateWidth();
        return this;
    };

    exports.dataTitle = function(_x) {
        if (!arguments.length) return dataTitle;
        dataTitle = _x;
        return this;
    };

    exports.dataUnit = function(_x) {
        if (!arguments.length) return dataUnit;
        dataUnit = _x;
        return this;
    };

    exports.setColor = function(_x) {
        if (!arguments.length) return color;
        color = _x;
        return this;
    };

    exports.height = function(_x) {
        if (!arguments.length) return height;
        height = parseInt(_x);
        return this;
    };
    exports.gap = function(_x) {
        if (!arguments.length) return gap;
        gap = _x;
        return this;
    };
    exports.ease = function(_x) {
        if (!arguments.length) return ease;
        ease = _x;
        return this;
    };

    exports.labelRotation = function(_x) {
        if (!arguments.length) return labelRotation
        labelRotation = parseInt(_x);
        return this;
    }

    exports.labelSize = function(_x) {
        if (!arguments.length) return labelSize
        labelSize = parseInt(_x);
        return this;
    }

    exports.margin = function(_x) {
        if (!arguments.length) return margin;
        margin = _x;
        return this;
    };



    exports.titles = function(_x) {
        if (!arguments.length) return titles;
        titles = _x;
        return this;
    };




    exports.showYaxis = function(_x) {
        if (!arguments.length) return showYaxis;
        showYaxis = _x;
        return this;
    };


    exports.selectable = function(_x) {
        if (!arguments.length) return selectable;
        selectable = _x;
        return this;
    };

    return exports;
};



