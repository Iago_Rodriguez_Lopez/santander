// Bar chart Module
/////////////////////////////////

// Declare namespace


// Declare component: (this outer function acts as the closure):
d3.cloudshapes.lineChartSantander1 = function module() {
    var margin = { top: 10, right: 40, bottom: 40, left: 70 },
        width = 500,
        height = 500,
        gap = 0,
        color = "grey"
    ease = "bounce";


    var data = [];
    var labels = [];
    var svg;
    var container;
    var dataTitle = "";
    var dataUnit = "";
    var updateData;

    var tip = d3.tip()
        .attr('class', 'd3-tip')
        .offset([-10, 0])
        .html(function(d, i) {
            return "<h4><strong>" + labels[i] + "</strong></h4><div class='content'><table><tr><td>" + dataTitle + ":</td><td style='font-size: 18px'>" + d + " " + dataUnit.toLocaleString("es-ES") + "</td></tr></table></div>";
        })



    // Define the 'inner' function: which, through the surreal nature of JavaScript scoping, can access
    // the above variables.
    function exports(_selection) {
        _selection.each(function() {
            _data = data;
            _labels = labels;
            var chartW = width - margin.left - margin.right,
                chartH = height - margin.top - margin.bottom;

            var range_value = .2;

            // Define x and y scale variables.
            var x1 = d3.scale.ordinal().domain(labels)
                .rangePoints([0, chartW]);

            // var x1 = d3.scale.ordinal()
            //         .domain(_data.map(function(d, i) { return i; }))
            //         .range(["0","1","2","3","4","5","6"]);

            var y1 = d3.scale.linear()
                .domain([0, d3.max(_data, function(d, i) { return d + d * .1; })])
                .range([chartH, 0]).nice(1);

            var xAxis = d3.svg.axis()
                .scale(x1)
                .orient("bottom")
                .innerTickSize(-chartH)
                .outerTickSize(10)
                .tickPadding(10)
                .tickFormat(function(d) { return d.toLocaleString("es-ES"); });


            var xAxis1 = d3.svg.axis()
                .scale(x1)
                .orient("bottom")
                .tickPadding(6)
                .tickSize(8)
                .tickFormat(function(d) { return "" });

            var xAxis2 = d3.svg.axis()
                .scale(x1)
                .orient("bottom")
                .tickPadding(6)
                .tickSize(8)
                .tickFormat(function(d) { return "" });

            var yAxis = d3.svg.axis()
                .scale(y1)
                .orient("left")
                .innerTickSize(-chartW)
                .ticks(4)
                .outerTickSize(20)
                .tickPadding(25);

            var yAxis1 = d3.svg.axis()
                .scale(y1)
                .orient("left")
                .ticks(4)
                .tickSize(20)
                .tickPadding(1000)
                .tickFormat(function(d) { return "" });


            var yAxis2 = d3.svg.axis()
                .scale(y1)
                .orient("left")
                .ticks(4)
                .tickSize(20)
                .tickPadding(1000)
                .tickFormat(function(d) { return "" });


            // If no SVG exists, create one - and add key groups:
            if (!svg) {
                svg = d3.select(this)
                    .append("svg")
                    .classed("santander-linechart", true);
                container = svg.append("g").classed("container-group", true);
            }


            // Transition the width and height of the main SVG and the key 'g' group:
            svg.transition().attr({ width: width, height: height });
            container
                .attr({ transform: "translate(" + margin.left + "," + margin.top + ")" });

            var line = d3.svg.line()
                .x(function(d, i) { return x1(labels[i]); })
                .y(function(d) { return y1(d); });

            container.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + chartH + ")")
                .call(xAxis);

            container.append('g')
                .classed('axis', true)
                .classed('hours', true)
                .classed('labeled', true)
                .attr("transform", "translate(0," + chartH + ")")
                .call(xAxis1)

            container.append('g')
                .classed('axis', true)
                .classed('hours', true)
                .classed('labeled', true)
                .attr("transform", "translate(0," + -8 + ")")
                .call(xAxis1)

            container.append("g")
                .attr("class", "y axis")
                .attr("transform", "translate(" + 0 + "," + 0 + ")")
                .call(yAxis);

            container.append("g")
                .attr("class", "y axis1")
                .attr("transform", "translate(" + 0 + "," + 0 + ")")
                .call(yAxis1);

            container.append("g")
                .attr("class", "y axis2")
                .attr("transform", "translate(" + (chartW + 20) + "," + 0 + ")")
                .call(yAxis2);

            container.append("g").classed("line-chart-group", true).attr("transform", "translate(" + (x1.range()[0] * (1 - range_value)) + ",0)");

            // Select all bars and bind data:
            var path = svg.select(".line-chart-group")
                .append("path") // Add the valueline path.
                .attr("class", "line")
                .attr("d", line(_data))
                .style("stroke", color);

            var circles = svg.select(".line-chart-group")
                .selectAll(".circle")
                .data(_data);

            circles.enter().append('circle').classed("circle", true)
                .style("stroke", "grey")
                .style("fill", "white")
                .attr('cx', function(d, i) {
                    return x1(labels[i]);
                })
                .attr('cy', function(d) { return y1(d); })
                .attr('r', 3)
                .on('mouseover', function(d, i) {
                    tip.show(d, i);
                    d3.select(this).style("stroke", "red");
                })
                .on('mouseout', function(d, i) {
                    tip.hide(d, i);
                    d3.select(this).style("stroke", "grey");
                });


            svg.call(tip);

            updateData = function() {
                _data = data;
                y1 = d3.scale.linear()
                    .domain([0, d3.max(_data, function(d, i) { return d + d * .1; })])
                    .range([chartH, 0]).nice(1);

                yAxis = d3.svg.axis()
                    .scale(y1)
                    .orient("left")
                    .innerTickSize(-chartW + 40)
                    .ticks(4)
                    .outerTickSize(20)
                    .tickPadding(10);

                svg.selectAll("g .y.axis").call(yAxis);

                svg.selectAll("g .x.axis").call(xAxis);

                path.transition().ease(ease).duration(1000).attr("d", line(_data));

                circles = svg.selectAll("circle").data(_data);
                circles.transition().ease(ease).duration(1000).attr('cy', function(d) { return y1(d); });

            }


        });
    }

    exports.data = function(_x) {
        if (!arguments.length) return data;
        data = _x;
        if (typeof updateData === 'function') updateData();
        return this;
    };

    exports.labels = function(_x) {
        if (!arguments.length) return labels;
        labels = _x;
        return this;
    };

    // GETTERS AND SETTERS:
    exports.width = function(_x) {
        if (!arguments.length) return width;
        width = parseInt(_x);
        return this;
    };

    exports.setColor = function(_x) {
        if (!arguments.length) return color;
        color = _x;
        return this;
    };

    exports.height = function(_x) {
        if (!arguments.length) return height;
        height = parseInt(_x);
        return this;
    };

    exports.dataTitle = function(_x) {
        if (!arguments.length) return dataTitle;
        dataTitle = _x;
        return this;
    };

    exports.dataUnit = function(_x) {
        if (!arguments.length) return dataUnit;
        dataUnit = _x;
        return this;
    };

    exports.gap = function(_x) {
        if (!arguments.length) return gap;
        gap = _x;
        return this;
    };
    exports.ease = function(_x) {
        if (!arguments.length) return ease;
        ease = _x;
        return this;
    };

    exports.margin = function(_x) {
        if (!arguments.length) return margin;
        margin = _x;
        return this;
    };
    return exports;
};