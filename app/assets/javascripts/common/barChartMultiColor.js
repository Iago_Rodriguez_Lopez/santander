// Bar chart Module
/////////////////////////////////

// Declare namespace


// Declare component: (this outer function acts as the closure):
d3.cloudshapes.barChartMultiColor = function module() {
    var margin = { top: 60, right: 20, bottom: 40, left: 20 },
        width = 500,
        height = 500,
        gap = 0.1,
        colors = ["#001ff0"],
        ease = "bounce";

    var selectable = true;
    var data = [];
    var labels = [];
    var dataTitle = "";
    var dataUnit = "";
    var onClickFunction = function() {};
    var svg;
    var labelRotation = 0;
    var labelSize = 12;
    var updateData;
    var updateWidth;
    var clearSelection;

    var tip = d3.tip()
        .attr('class', 'd3-tip')
        .offset([-10, 0])
        .html(function(d, i) {
            d = d.toLocaleString("es-ES", { maximumFractionDigits: 2 })
            return "<h4><strong>" + labels[i] + "</strong></h4><div class='content'><table><tr><td>" + dataTitle + ":</td><td style='font-size: 18px'>" + d + " " + dataUnit.toLocaleString("es-ES") + "</td></tr></table></div>";
        });

    function wrap(text, width, right) {
        text.each(function() {
            var text = d3.select(this),
                words = text.text().split(/\s+/).reverse(),
                word,
                line = [],
                lineNumber = 0,
                lineHeight = 1.1, // ems
                y = text.attr("y"),
                dy = parseFloat(text.attr("dy")),
                tspan = text.text(null).append("tspan").attr("x", -right).attr("y", y).attr("dy", dy + "em");

            while (word = words.pop()) {
                line.push(word);
                tspan.text(line.join(" "));
                if (tspan.node().getComputedTextLength() > width) {
                    line.pop();
                    tspan.text(line.join(" "));
                    line = [word];
                    tspan = text.append("tspan").attr("x", -right).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
                }
            }
        });
    }

    // Define the 'inner' function: which, through the surreal nature of JavaScript scoping, can access
    // the above variables.
    function exports(_selection) {
        _selection.each(function() {
            _data = data;
            var chartW = width - margin.left - margin.right,
                chartH = height - margin.top - margin.bottom;

            // Define x and y scale variables.
            var x1 = d3.scale.ordinal()
                .domain(labels.map(function(d) { return d; }))
                .rangeRoundBands([0, chartW], 0.2);

            var y1 = d3.scale.linear()
                .domain([0, d3.max(_data, function(d, i) { return d + d * 0.1; })])
                .range([chartH, 0]);

            var xAxis = d3.svg.axis()
                .scale(x1)
                .orient("bottom")
                .innerTickSize(0)
                .outerTickSize(10)
                .tickPadding(10)
                .tickFormat(function(d) { return d.toLocaleString("es-ES"); });

            var yAxis = d3.svg.axis()
                .scale(y1)
                .orient("left")
                .innerTickSize(-chartW)
                .ticks(2)
                .outerTickSize(20)
                .tickPadding(40)
                .tickFormat(function(d) { return ""; });

            var yAxis1 = d3.svg.axis()
                .scale(y1)
                .orient('left')
                .ticks(2)
                .outerTickSize(0)
                .tickPadding(20)
                .tickSize(0)
                .tickFormat(function(d, i) {
                    if (i === 0) return "";
                    else return d.toLocaleString("es-ES") + " " + dataUnit;
                });

            var yAxis2 = d3.svg.axis()
                .scale(y1)
                .orient("left")
                .innerTickSize(0)
                .ticks(2)
                .tickSize(60)
                .tickPadding(60)
                .tickFormat(function(d) { return ""; });


            // If no SVG exists, create one - and add key groups:
            if (!svg) {
                svg = d3.select(this)
                    .append("svg")
                    .classed("santander-barchart", true);
                if (selectable === false) {
                    svg.classed("unselectable", true)
                }
                var container = svg.append("g").classed("container-group", true);
            }


            container.append("g")
                .attr("class", "y axis")
                .attr("transform", "translate(" + 0 + "," + 0 + ")")
                .call(yAxis);

            container.append("g")
                .attr("class", "y axis1")
                .attr("transform", "translate(" + 0 + "," + -10 + ")")
                .call(yAxis1);

            container.append("g")
                .attr("class", "y axis2")
                .attr("transform", "translate(" + 0 + "," + 0 + ")")
                .call(yAxis2);

            container.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate(0," + chartH + ")")
                .call(xAxis)

            container.select(".x").selectAll("text")
                .style("text-anchor", function() {
                    if (labelRotation == 0) {
                        return "middle"
                    } else {
                        return "end"
                    }
                })
                .style("font-size", labelSize)
                // .attr("dx", "-.8em")
                // .attr("dy", ".15em")
                .attr("transform", function(d) {
                    return "rotate(-" + labelRotation + ")"
                });;


            container.append("g")
                .attr("class", "x axis mark")
                .append('line')
                .attr("class", "group-name-line")
                .attr('x2', chartW)
                .attr('x1', -60)
                .attr('y2', chartH)
                .attr('y1', chartH)
                .style('stroke', "black");

            container.append("g").classed("bar-chart-group", true);

            // Transition the width and height of the main SVG and the key 'g' group:
            svg.transition().attr({ width: width, height: height });
            svg.select(".container-group")
                .attr({ transform: "translate(" + margin.left + "," + margin.top + ")" });

            // Define gap between bars:
            var gapSize = x1.rangeBand() / 100 * gap;

            // Define width of each bar:
            var barW = x1.rangeBand() - gapSize;

            // Select all bars and bind data:
            var bars = svg.select(".bar-chart-group")
                .selectAll(".bar")
                .data(_data);


            var back_bars = svg.select(".bar-chart-group")
                .selectAll(".back-bar")
                .data(_data);

            // D3 UPDATE code for bars
            back_bars.enter().append("rect")
                .classed("back-bar", true)
                .transition()
                .ease(ease)
                .attr({
                    width: barW,
                    x: function(d, i) { return x1(labels[i]) + gapSize / 2; },
                    y: 0,
                    height: function(d, i) { return chartH; }
                })
                .style("fill", "grey")
                .style("fill-opacity", "0.3");

            // ENTER, UPDATE and EXIT CODE:
            // D3 ENTER code for bars!
            var gBars = bars.enter().append("g").classed('_selected_', true);

            gBars.append("rect")
                .classed("bar", true)
                .attr({
                    width: barW,
                    x: function(d, i) { return x1(labels[i]) + gapSize / 2; },
                    y: function(d, i) {
                        return y1(d);
                    },
                    height: function(d, i) { return chartH - y1(d); }
                })
                .on('mouseover', function(d, i) {
                    tip.show(d, i);
                    d3.select(this).style("stroke", "red");
                })
                .on('mouseout', function(d, i) {
                    tip.hide(d, i);
                    d3.select(this).style("stroke", "none");
                })
                .on("click", function(d, i) {
                    if (selectable) {
                        var index = i;
                        gBars.classed('_selected_', function(d, i) {
                            return i === index;
                        });

                        svg.select('.x.axis')
                            .selectAll('text')
                            .style('opacity', function(d, i) {
                                if (i === index) {
                                    return 1;
                                } else {
                                    return 0.3;
                                }
                            });

                        onClickFunction(this, d, labels[i]);
                    }
                })
                .style("fill", function(d, i) { return colors[i % colors.length] });

            var barText = gBars.append('text')
                .attr({
                    x: function(d, i) { return x1(labels[i]) + gapSize / 2 + barW / 2; },
                    y: function(d, i) { return y1(d) - 2; }
                })
                .attr('text-anchor', 'middle')
                .style("font-size", labelSize)
                .text(function(d, i) {
                    return d.toLocaleString("es-ES", { maximumFractionDigits: 2 })
                });

            svg.select('.x.axis')
                .selectAll('.tick text')
                .on("click", function(d, i) {
                    if (selectable) {
                        var index = i;
                        gBars.classed('_selected_', function(d, i) {
                            return i === index;
                        });

                        svg.select('.x.axis')
                            .selectAll('text')
                            .style('opacity', function(d, i) {
                                if (i === index) {
                                    return 1;
                                } else {
                                    return 0.3;
                                }
                            });

                        onClickFunction(svg.selectAll('.bar')[0][i], d, labels[i]);
                    }
                })
                .style('cursor', 'pointer');

            svg.call(tip);


            // bars.exit().transition().style({opacity: 0}).remove();


            updateData = function() {
                _data = data;
                _labels = labels;

                bars = svg.select(".bar-chart-group")
                    .selectAll(".bar")
                    .data(_data);

                // x1 = d3.scale.ordinal()
                //     .domain(labels.map(function(d) { return d; }))
                //     .rangeRoundBands([0, chartW], 0.2);

                y1 = d3.scale.linear()
                    .domain([0, d3.max(_data, function(d, i) { return d + d * .1; })])
                    .range([chartH, 0]);

                yAxis = d3.svg.axis()
                    .scale(y1)
                    .orient("left")
                    .innerTickSize(-chartW)
                    .ticks(2)
                    .outerTickSize(20)
                    .tickPadding(40)
                    .tickFormat(function(d) { return "" });

                yAxis1 = d3.svg.axis()
                    .scale(y1)
                    .orient('left')
                    .ticks(2)
                    .outerTickSize(0)
                    .tickPadding(20)
                    .tickSize(0)
                    .tickFormat(function(d, i) {
                        if (i == 0) return "";
                        else return d.toLocaleString("es-ES") + " " + dataUnit;
                    });

                yAxis2 = d3.svg.axis()
                    .scale(y1)
                    .orient("left")
                    .innerTickSize(0)
                    .ticks(2)
                    .tickSize(60)
                    .tickPadding(60)
                    .tickFormat(function(d) { return "" });


                svg.selectAll("g .y.axis").transition().call(yAxis);
                svg.selectAll("g .y.axis1").transition().call(yAxis1);
                svg.selectAll("g .y.axis2").transition().call(yAxis2);

                gapSize = x1.rangeBand() / 100 * gap;

                // Define width of each bar:
                barW = x1.rangeBand() - gapSize;
                // data that needs DOM = enter() (a set/selection, not an event!)
                bars.enter().append("rect")
                    .attr("class", "bar _selected_")
                    .attr("y", y1(0))
                    .attr("height", height - y1(0));


                // D3 UPDATE code for bars
                bars.transition().ease(ease).duration(1000)
                    .attr({
                        width: barW,
                        x: function(d, i) { return x1(labels[i]) + gapSize / 2; },
                        y: function(d, i) { return y1(d); },
                        height: function(d, i) { return chartH - y1(d); }
                    });

                barText.data(_data).transition().ease(ease).duration(1000)
                    .attr('y', function(d, i) { return y1(d) - 2; })
                    .text(function(d, i) {
                        return d.toLocaleString("es-ES", { maximumFractionDigits: 2 })
                    });

                bars.exit()
                    .transition()
                    .duration(300)
                    .attr("y", y1(0))
                    .attr("height", chartH - y1(0))
                    .style('fill-opacity', 1e-6)
                    .remove();

                // D3 EXIT code for bars
                bars.exit().transition().style({ opacity: 0 }).remove();

            }

            updateWidth = function() {
                chartW = width - margin.left - margin.right;

                x1 = d3.scale.ordinal()
                    .domain(labels.map(function(d) { return d; }))
                    .rangeRoundBands([0, chartW], 0.2);

                xAxis = d3.svg.axis()
                    .scale(x1)
                    .orient("bottom")
                    .innerTickSize(0)
                    .outerTickSize(10)
                    .tickPadding(10)
                    .tickFormat(function(d) { return d.toLocaleString("es-ES"); });

                yAxis = d3.svg.axis()
                    .scale(y1)
                    .orient("left")
                    .innerTickSize(-chartW)
                    .ticks(2)
                    .outerTickSize(20)
                    .tickPadding(40)
                    .tickFormat(function(d) { return "" });

                yAxis2 = d3.svg.axis()
                    .scale(y1)
                    .orient("left")
                    .innerTickSize(0)
                    .ticks(2)
                    .tickSize(60)
                    .tickPadding(60)
                    .tickFormat(function(d) { return "" });

                yAxis2 = d3.svg.axis()
                    .scale(y1)
                    .orient("left")
                    .innerTickSize(0)
                    .ticks(2)
                    .tickSize(60)
                    .tickPadding(60)
                    .tickFormat(function(d) { return "" });

                svg.select('.x.axis')
                    // .attr("transform", "translate(0," + chartH + ")")
                    .call(xAxis);

                svg.selectAll("g .y.axis").call(yAxis);
                svg.selectAll("g .y.axis1").call(yAxis1);
                svg.selectAll("g .y.axis2").call(yAxis2);

                svg.select(".x.axis.mark line")
                    .transition()
                    .attr('x2', chartW);

                gapSize = x1.rangeBand() / 100 * gap;
                barW = x1.rangeBand() - gapSize;

                svg.selectAll("rect.back-bar")
                    .transition()
                    .attr('width', barW)
                    .attr('x', function(d, i) { return x1(labels[i]) + gapSize / 2; });

                svg.selectAll("rect.bar")
                    .transition()
                    .attr('width', barW)
                    .attr('x', function(d, i) { return x1(labels[i]) + gapSize / 2; });

                barText.data(_data).transition().ease(ease).duration(1000)
                    .attr('x', function(d, i) { return x1(labels[i]) + gapSize / 2 + barW / 2; });

                container.select(".x").selectAll("text")
                    .style("text-anchor", function() {
                        if (labelRotation == 0) {
                            return "middle";
                        } else {
                            return "end";
                        }
                    })
                    .style("font-size", labelSize)
                    .attr("transform", function(d) {
                        return "rotate(-" + labelRotation + ")";
                    });

                svg.transition().attr({ width: width, height: height });
            }

            clearSelection = function() {
                gBars.classed('_selected_', true);
                svg.select('.x.axis')
                    .selectAll('text')
                    .style('opacity', 1);
                onClickFunction(svg);
            }
        });
    }

    exports.cleanFilter = function() {
        if (typeof clearSelection === 'function') clearSelection();
    }

    // GETTERS AND SETTERS:


    exports.data = function(_x) {
        if (!arguments.length) return data;
        data = _x;
        if (typeof updateData === 'function') updateData();
        return this;
    };


    exports.onClickFunction = function(value) {
        if (!arguments.length) return onClickFunction;
        onClickFunction = value;
        return this;
    };

    exports.labels = function(_x) {
        if (!arguments.length) return labels;
        labels = _x;
        return this;
    };

    exports.width = function(_x) {
        if (!arguments.length) return width;
        width = parseInt(_x);
        if (typeof updateWidth === 'function') updateWidth();
        return this;
    };

    exports.dataTitle = function(_x) {
        if (!arguments.length) return dataTitle;
        dataTitle = _x;
        return this;
    };

    exports.dataUnit = function(_x) {
        if (!arguments.length) return dataUnit;
        dataUnit = _x;
        return this;
    };

    exports.setColors = function(_x) {
        if (!arguments.length) return color;
        colors = _x;
        return this;
    };

    exports.height = function(_x) {
        if (!arguments.length) return height;
        height = parseInt(_x);
        return this;
    };
    exports.gap = function(_x) {
        if (!arguments.length) return gap;
        gap = _x;
        return this;
    };
    exports.ease = function(_x) {
        if (!arguments.length) return ease;
        ease = _x;
        return this;
    };

    exports.labelRotation = function(_x) {
        if (!arguments.length) return labelRotation
        labelRotation = parseInt(_x);
        return this;
    }

    exports.labelSize = function(_x) {
        if (!arguments.length) return labelSize
        labelSize = parseInt(_x);
        return this;
    }

    exports.margin = function(_x) {
        if (!arguments.length) return margin;
        margin = _x;
        return this;
    };

    exports.selectable = function(_x) {
        if (!arguments.length) return selectable;
        selectable = _x;
        return this;
    };

    return exports;
};