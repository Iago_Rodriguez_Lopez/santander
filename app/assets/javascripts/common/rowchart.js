
// Bar chart Module
/////////////////////////////////

// Declare namespace


// Declare component: (this outer function acts as the closure):
d3.cloudshapes.rowChart = function module() {
    var margin = {top: 0, right: 20, bottom: 40, left: 0},
        width = 500,
        height = 500,
        gap = 0,
        color = "#001ff0"
        ease = "bounce";

    var data = [];
    var svg;

    var updateData;

    // Define the 'inner' function: which, through the surreal nature of JavaScript scoping, can access
    // the above variables.
    function exports(_selection) {
        _selection.each(function() {
            _data = data;
            var chartW = width - margin.left - margin.right,
                chartH = height - margin.top - margin.bottom;

      // Define x and y scale variables.
            var x1 = d3.scale.linear()
                    .domain([0, d3.max(_data, function(d, i) { return d; })])
                    .range([chartW, 0]);


            var y1 = d3.scale.ordinal()
                    .domain(_data.map(function(d, i) { return i; }))
                    .rangeRoundBands([0, chartH], 0.1);


      // If no SVG exists, create one - and add key groups:
            if (!svg) {
                svg = d3.select(this)
                    .append("svg")
                    .classed("rowchart", true);
                var container = svg.append("g").classed("container-group", true);
                container.append("g").classed("row-chart-group", true);
            }

      // Transition the width and height of the main SVG and the key 'g' group:
            svg.transition().attr({width: width, height: height});
            svg.select(".container-group")
                .attr({transform: "translate(" + margin.left + "," + margin.top + ")"});

      // Define gap between bars:
            var gapSize = y1.rangeBand() / 100 * gap;

      // Define width of each bar:
            var barH = y1.rangeBand() - gapSize;

            // Select all bars and bind data:
            var rows = svg.select(".row-chart-group")
                    .selectAll(".row")
                    .data(_data);


            rows.enter().append("rect")
                .classed("row", true)
                .attr(
                    {x: 0,
                    width: function(d, i) { return x1(d);},
                    y: function(d, i) { return y1(i) + gapSize / 2; },
                    height: barH
                }).style("fill", color);
      // D3 UPDATE code for bars
            rows.transition()
                .ease(ease)
                .attr({
                    x: 0,
                    width: function(d, i) { return chartW - x1(d);},
                    y: function(d, i) { return y1(i) + gapSize / 2; },
                    height: barH
                });

      // D3 EXIT code for bars
            rows.exit().transition().style({opacity: 0}).remove();


            updateData = function() {
                _data = data;
                // Define x and y scale variables.
                x1 = d3.scale.linear()
                    .domain([0, d3.max(_data, function(d, i) { return d; })])
                    .range([chartW, 0]);


                y1 = d3.scale.ordinal()
                    .domain(_data.map(function(d, i) { return i; }))
                    .rangeRoundBands([0, chartH], 0.1);
                // Define gap between bars:
                gapSize = y1.rangeBand() / 100 * gap;

                // Define width of each bar:
                barH = y1.rangeBand() - gapSize;

                // Select all bars and bind data:
                rows = svg.select(".row-chart-group")
                        .selectAll(".row")
                        .data(_data);


                rows.enter().append("rect")
                    .classed("row", true)
                    .attr(
                        {x: 0,
                        width: function(d, i) { return x1(d);},
                        y: function(d, i) { return y1(i) + gapSize / 2; },
                        height: barH
                    }).style("fill", color);
          // D3 UPDATE code for bars
                rows.transition()
                    .ease(ease).duration(1000)
                    .attr({
                        x: 0,
                        width: function(d, i) { return chartW - x1(d);},
                        y: function(d, i) { return y1(i) + gapSize / 2; },
                        height: barH
                    });

          // D3 EXIT code for bars
                rows.exit().transition().style({opacity: 0}).remove();

            }
        });
    }

    exports.data = function(_x) {
        if (!arguments.length) return data;
        data = _x;
        if (typeof updateData === 'function') updateData();
        return this;
    };


    // GETTERS AND SETTERS:
    exports.width = function(_x) {
        if (!arguments.length) return width;
        width = parseInt(_x);
        return this;
    };

    exports.setColor = function(_x) {
        if (!arguments.length) return color;
        color = _x;
        return this;
    };

    exports.height = function(_x) {
        if (!arguments.length) return height;
        height = parseInt(_x);
        return this;
    };
    exports.gap = function(_x) {
        if (!arguments.length) return gap;
        gap = _x;
        return this;
    };
    exports.ease = function(_x) {
        if (!arguments.length) return ease;
        ease = _x;
        return this;
    };

    return exports;
};

