// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require bootstrap-sprockets
//= require_tree ./manager
//= require_tree ./common

var goToUrl = function(url){ window.location.href = url  };

var saveUserLog = function(_params){
  const url = '/api/v1/user_logs';
  $.ajax({
      url: url,
      cache: false,
      async: false,
      type: 'POST',
      dataType: "JSON",
      data: _params,
      error: function(){
        console.log('Error al guardar log de usuarios')
      }
  })


}

var downloadData = function(dataFiltered, title) {
  let a = document.createElement("a");
  document.body.appendChild(a);
  a.style = "display: none";
  const csvText = ConvertToCSV(dataFiltered)
  let blob = new Blob( [csvText], {type: 'text/csv'} );
  url = window.URL.createObjectURL(blob);
  a.href = url;
  a.download = title + '.csv';
  a.click();
  window.URL.revokeObjectURL(url);
  document.body.removeChild(a);
}

var ConvertToCSV = function(objArray) {
    var array = typeof objArray != 'object' ? JSON.parse(objArray) : objArray;
    // Permute id by count
    let keys = Object.keys(array[0]);
    keys = keys.filter(d => d !== 'count')
    var str = keys.join(',') + '\r\n'; 
    for (var i = 0; i < array.length; i++) {
        var line = '';
        for (let index in array[i]) {
            if (line != '') line += ',';
            if (index === 'id')
              line += array[i]['count'];
            else if (index !== 'count')
              line += array[i][index];
        }
        str += line + '\r\n';
    }
    return str;
}


$(document).ready(function($){


  	$('select').selectBoxIt({
          theme: "bootstrap"
      });

    $("#tabla").css("top", $(window).scrollTop()+$( window ).height()-25 + "px");

    $(window).scroll(function() {
        $("#tabla").css("top", $(window).scrollTop()+$( window ).height()-25 + "px");
    });

    if (Cookies.get('sidebar-status') == "closed"){
      $(".page-container").addClass('sidebar-collapsed');
    }else{
      $(".page-container").removeClass('sidebar-collapsed');

    }

    $("#download-data").click(function(){
      const dataFiltered = allDimension.top(Infinity);
      dataFiltered.length > 0 ? downloadData(dataFiltered, 'actual') : console.log('No Data')
    })

    $("#download-data-old").click(function(){
      const dataFiltered = allDimensionOld.top(Infinity);
      dataFiltered.length > 0 ? downloadData(dataFiltered, 'Histórico') : console.log('No Data')
    })


});




