//= require jquery
//= require jquery_ujs
//= require bootstrap-sprockets
//= require select2-full


var counter = 0;

var divInputOptions = {
  placeholder: "División",
  ajax: {
    url: "/dashboard/api/v1/vacantes",
    dataType: 'json',
    delay: 250,
    data: function (params) {
      return {
        d: params.term, // search term
        p: params.page
      };
    },
    processResults: function (data, params) {
      // parse the results into the format expected by Select2
      // since we are using custom formatting functions we do not need to
      // alter the remote JSON data, except to indicate that infinite
      // scrolling can be used
      params.page = params.page || 1;

      return {
        results: data.map(function(d){
          return {
            text: d.division,
            id: d.division
          };
        })
      };
    },
    cache: true
  },
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 3,
  maximumSelectionLength: 1
};

var gerInputOptions = {
  placeholder: "Gerencia",
  ajax: {
    url: "/api/v1/vacantes",
    dataType: 'json',
    delay: 250,
    data: function (params) {
      let s = this.attr("id");
      let id = s[s.length-1];

      console.log(id);

      return {
        d: $("#usuario_accesos_division_"+id).val(),
        g: params.term, // search term
        p: params.page
      };
    },
    processResults: function (data, params) {
      // parse the results into the format expected by Select2
      // since we are using custom formatting functions we do not need to
      // alter the remote JSON data, except to indicate that infinite
      // scrolling can be used
      params.page = params.page || 1;

      return {
        results: data.map(function(d){
          return {
            text: d.gerencia,
            id: d.gerencia
          };
        })
      };
    },
    cache: true
  },
  escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
  minimumInputLength: 3
};

var init = function(){
  $('[data-toggle="tooltip"]').tooltip();
  counter = 0;

  // Accesos toggle
  $('#usuario_rol').change(function(){
    if(this.value == 0)
      $('#usuario-accesos-group').slideDown();
    else
      $('#usuario-accesos-group').slideUp();
  });

  // Responder a filtro de fecha
  $("#periodo_fecha_2i, #periodo_fecha_1i").change(function(){
    console.log(this);

    let month = $("#periodo_fecha_2i").val();

    if (parseInt(month) < 10)
      month = "0"+month;

    let year = $("#periodo_fecha_1i").val();

    location.href = "/dashboard/admin/"+$(this).data("resource")+"?month="+month+"&year="+year;
  });

  $('#upload_file').bind('change', function(){
    let file = this.files[0]
    let valid = validateUploadFile(file);

    if(valid)
      $('#upload_file_invalid').slideUp();
    else
      $('#upload_file_invalid').slideDown();

    $('label[for="upload_file"]').html(
      "<strong>Archivo seleccionado:</strong> "+file.name+"<br/><br/><em>Click para cambiarlo.</em>"
    );

    $('#upload_submit').prop('disabled', !valid);
  });

  $("[data-toggle='tooltip']").tooltip();

  // clear search
  $("#clear_search").click(function(){
    $("#search_query").val("");
    $("#search_form").submit();
  });
};

var validateUploadFile = function(file) {
  return true;
}

var createAcceso = function(){
  counter++;

  var cont = $($.parseHTML($('#usuario-accesos-base').wrap("<p/>").parent().html()));

  $('#usuario-accesos-base').unwrap();
  cont.attr('id', 'usuario-accesos-'+counter);
  cont.show();

  var div_input = cont.find('[data-select="division"]');
  var ger_input = cont.find('[data-select="gerencia"]');

  div_input.attr("id", "usuario_accesos_division_"+counter);
  //div_input.attr("name", "usuario[accesos]["+counter+"][division]");

  ger_input.attr("id", "usuario_accesos_gerencia_"+counter);
  //ger_input.attr("name", "usuario[accesos]["+counter+"][gerencia]");

  var btn = cont.find('[data-action="delete-acceso"]');
  btn.attr('id', 'usuario-accesos-delete-'+counter);
  btn.data('id', counter);

  cont.find('input[type="hidden"]').remove();

  if(counter == 1)
    btn.attr('disabled', true);

  $('#usuario-accesos').append(cont);

  // Reinicializamos
  accesos();

  div_input.select2(divInputOptions);
  ger_input.select2(gerInputOptions);
};

var accesos = function(){
  $('.gerencia').off();

  $('[data-action="delete-acceso"]').off();
  $('[data-action="create-acceso"]').off();

  $('[data-action="delete-acceso"]').click(function(){
    $('#usuario-accesos-'+$(this).data("id")).remove();
    accesos();
  });

  $('[data-action="create-acceso"]').click(function(){
    createAcceso();
    return false;
  });

  $('.select2-search__field:last').keydown(function(e){
    var key = e.keyCode || e.which;

    if (key == '9') {
      createAcceso();
      $('.division:last').focus();
      return false;
    }
  });
};




$(document).ready(init);
$(document).ready(createAcceso);
