module ApplicationHelper

	def nav_link(text, link)

    recognized = Rails.application.routes.recognize_path(link)
    if recognized[:controller] == params[:controller]
      content_tag(:a, :class => "active") do
        link_to( text, link)
      end
    else
      content_tag(:li) do
        link_to( text, link)
      end
    end
	end

	def flash_demux(type)
	    case type
	    when "success"
	      :success
	    when "alert"
	      :danger
	    else
	      :info
	    end
	  end

end
