# == Schema Information
#
# Table name: licencia_view
#
#  id                  :integer
#  rut                 :string
#  sexo                :string
#  fecha_ingreso_grupo :date
#  fecha_nacimiento    :date
#  fecha_renuncia      :date
#  tipo_renuncia       :string
#  nombre              :string
#  apellidos           :string
#  division            :string
#  gerencia            :string
#  periodo             :string
#  piramide            :string
#  tipo_contrato       :string
#  nombre_empresa      :string
#  es_plantilla        :boolean
#  tipo_renta          :string
#  cargo               :string
#  estado_civil        :string
#  num_hijos           :integer
#  periodo_licencia    :string
#  glosa_tipo_licencia :string
#  dias_totales        :integer
#  dias_corridos       :integer
#  dias_habiles        :integer
#



class ViewLicencia < ActiveRecord::Base
  self.table_name = 'licencia_view'
  after_initialize :readonly!


  scope :in_period, ->(period) { where('periodo = ?', period) unless period.blank?}
  scope :in_periods, ->(period, period_old) { where('periodo = ? or periodo = ?', period, period_old) unless period.blank? and period_old.blank?}
  scope :active, -> {where("es_plantilla = ?", true)}
  scope :renta_fija, -> {where("tipo_renta = ?", "Renta Fija")}
  scope :indefinido, -> {where("tipo_contrato = ?", "Indefinido")}
  scope :no_maternales, -> {where("(glosa_tipo_licencia != ? and glosa_tipo_licencia != ?)", "Licencia Maternal", "Patología del embarazo")}
  scope :in_division, ->(division) { where('division = ?', division) unless (division.blank? or division=="Todas" or division=="Global" or division=="global")}

end
