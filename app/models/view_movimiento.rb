# == Schema Information
#
# Table name: movimiento_view
#
#  id                  :integer
#  rut                 :string
#  sexo                :string
#  fecha_ingreso_grupo :date
#  fecha_nacimiento    :date
#  fecha_renuncia      :date
#  tipo_renuncia       :string
#  nombre              :string
#  apellidos           :string
#  division            :string
#  gerencia            :string
#  piramide            :string
#  tipo_contrato       :string
#  nombre_empresa      :string
#  es_plantilla        :boolean
#  tipo_renta          :string
#  cargo               :string
#  estado_civil        :string
#  num_hijos           :integer
#  periodo             :string
#  periodo_movimiento  :string
#  tipo_movimiento     :string
#

class ViewMovimiento < ActiveRecord::Base
  self.table_name = 'movimiento_view'
  after_initialize :readonly!

  scope :indefinido, -> {where("tipo_contrato = ?", "Indefinido")}
  scope :active, -> {where("es_plantilla = ?", true)}
  scope :main_types, -> { where('tipo_movimiento = ? or tipo_movimiento = ?', "Promoción", "Desarrollo Horizontal")}
  scope :in_period, ->(period) { where('periodo = ?', period) unless period.blank?}
  scope :in_periods, ->(period, period_old) { where('periodo = ? or periodo = ?', period, period_old) unless period.blank? and period_old.blank?}
  scope :in_division, ->(division) { where('division = ?', division) unless (division.blank? or division=="Todas" or division=="Global" or division=="global")}

end
