# == Schema Information
#
# Table name: vacantes
#
#  id             :integer          not null, primary key
#  division       :string
#  gerencia       :string
#  vacantes       :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  empleados      :integer
#  tipo_area      :string
#  activo         :boolean
#  periodo        :string
#  division_order :integer
#  gerencia_order :integer
#

class Vacante < ActiveRecord::Base
    # scope :in_period, ->(period) { where('periodo = ?', period) unless period.blank?}
  scope :active, -> {where("activo = ?", true)}
  scope :in_period, ->(period) { where('periodo = ?', period) unless period.blank?}
  scope :in_user, -> (user) { where("division in (?)", user.divisiones)}

  PARAMETROS = [:division, :gerencia, :tipo_area]

  scope :with_text, -> (text) {
    queries = PARAMETROS.map do |p|
      "#{p.to_s} ILIKE '%#{text}%'"
    end

    where(queries.join(" OR "))
  }

  def self.fill_vacantes(periodo)
    elementos = Posicion.in_period(periodo).pluck(:division, :gerencia).uniq
    elementos.each do |elemento|
      Vacante.find_or_create_by(division: elemento[0], gerencia: elemento[1], activo: true, periodo: periodo, tipo_area: "Apoyo y Estructura")
    end
    Vacante.where(division: "División Comercial").where(periodo: periodo).update_all(tipo_area: "Negocio")
    Vacante.where(division: "División Global Corporate Banking").where(periodo: periodo).update_all(tipo_area: "Negocio")
    Vacante.where(division: "División Banca Empresas e Instituciones").where(periodo: periodo).update_all(tipo_area: "Negocio")
  end


  def self.recreate_vacantes
    periodos = ["01-2016", "02-2016", "03-2016", "04-2016", "05-2016", "06-2016", "07-2016", "08-2016", "09-2016"]
    periodos.each do |periodo|
      Vacante.where(periodo: periodo).delete_all
      elementos = Posicion.in_period(periodo).pluck(:division, :gerencia).uniq
      elementos.each do |elemento|
        Vacante.find_or_create_by(division: elemento[0], gerencia: elemento[1], activo: true, periodo: periodo, tipo_area: "Apoyo y Estructura")
      end
      Vacante.where(division: "División Banca Comercial").where(periodo: periodo).update_all(tipo_area: "Negocio")
      Vacante.where(division: "División Global Corporate Banking").where(periodo: periodo).update_all(tipo_area: "Negocio")
      Vacante.where(division: "División Banca Empresas e Instituciones").where(periodo: periodo).update_all(tipo_area: "Negocio")
    end
  end

end
