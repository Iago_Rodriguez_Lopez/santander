# == Schema Information
#
# Table name: formaciones
#
#  id                 :integer          not null, primary key
#  materia            :string
#  nombre_del_curso   :string
#  fecha_inicio       :date
#  fecha_fin          :date
#  horas              :integer
#  periodo            :string
#  modalidad          :string
#  empleado_id        :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  rut                :string
#  periodo_anualizado :string
#  asistencia         :string
#

class Formacion < ActiveRecord::Base
  belongs_to :empleado

  scope :in_period, ->(period) { where('periodo = ?', period) unless period.blank?}

  PARAMETROS = [:materia, :nombre_del_curso, :modalidad, :rut, :asistencia]

  scope :with_text, -> (text) {
    queries = PARAMETROS.map do |p|
      "#{p.to_s} ILIKE '%#{text}%'"
    end

    where(queries.join(" OR "))
  }

  # def self.csv_import(data, periodo)
  #   row_index = 0
  #   ruts = []
  #   begin
  #     CSV.foreach(data.tempfile, encoding: Encoding::UTF_8,headers: true, :col_sep => "\t") do |row, index|
  #       row_index = row_index+1
  #       empleado = Empleado.find_by_rut(row["rut_fncro"])
  #       if empleado
  #           formacion = empleado.formaciones.new
  #           formacion.rut = row["rut_fncro"]
  #           formacion.materia = row["materia"]
  #           formacion.nombre_del_curso = row["nom_curso"]
  #           formacion.modalidad = row["Modalidad"]
  #           formacion.periodo = periodo
  #           formacion.periodo_anualizado = Date.strptime(row["periodo"], "%Y%m").strftime("%m-%Y")
  #           formacion.asistencia = row["asistencia"]
  #           formacion.horas = row["HORAS"]
  #           formacion.save!
  #       else
  #         ruts << row["rut_fncro"]
  #       end
  #     end
  #     # date = Date.strptime(periodo,"%m-%Y")
  #     # period_old = (date-1.year).end_of_year.strftime("%m-%Y")
  #     # division = "global"
  #     # view_formacion = ViewFormacion.in_periods(periodo, period_old).to_json
  #     # $redis.set("view_formacion"+periodo+division, view_formacion)
  #     return ruts
  #   rescue Exception => e
  #     puts e.message
  #     puts e.backtrace.inspect
  #     return row_index
  #   end
  # end


  def self.csv_import(data, periodo)
    row_index = 0
    ruts = []
    formaciones = []
    begin
      CSV.foreach(data.tempfile, encoding: Encoding::UTF_8,headers: true, :col_sep => "\t") do |row, index|
        row_index = row_index+1
        empleado = Empleado.find_by_rut(row["rut_fncro"])
        if empleado
            formacion = empleado.formaciones.new
            formacion.rut = row["rut_fncro"]
            formacion.materia = row["materia"]
            formacion.nombre_del_curso = row["nom_curso"]
            formacion.modalidad = row["Modalidad"]
            formacion.periodo = periodo
            formacion.periodo_anualizado = Date.strptime(row["periodo"], "%Y%m").strftime("%m-%Y")
            formacion.asistencia = row["asistencia"]
            formacion.horas = row["HORAS"]
            formaciones << formacion
        else
          ruts << row["rut_fncro"]
        end
      end
      Formacion.import formaciones, :validate => false
      # date = Date.strptime(periodo,"%m-%Y")
      # period_old = (date-1.year).end_of_year.strftime("%m-%Y")
      # division = "global"
      # view_formacion = ViewFormacion.in_periods(periodo, period_old).to_json
      # $redis.set("view_formacion"+periodo+division, view_formacion)
      return ruts.first(10)
    rescue Exception => e
      puts e.message
      puts e.backtrace.inspect
      return row_index
    end
  end

end
