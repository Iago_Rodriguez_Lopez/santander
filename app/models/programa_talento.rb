# == Schema Information
#
# Table name: programa_talentos
#
#  id          :integer          not null, primary key
#  empleado_id :integer
#  periodo     :string
#  nombre      :string
#  estado      :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  rut         :string
#

class ProgramaTalento < ActiveRecord::Base
  belongs_to :empleado
  scope :in_period, ->(period) { where('periodo = ?', period) unless period.blank?}

  def self.csv_import(data, periodo)
    row_index = 0
    begin
    # csv_forced = csv_handler.read.force_encoding(Encoding::UTF_8)
    # csv_string_encoded = csv_handler.read.encode!("iso-8859-1", "UTF-8", invalid: :replace)
    # csv = CSV.parse(csv_forced, :headers => true, :col_sep => ";")
    # csv.each_with_index do |row, index| encoding: Encoding::ISO_8859_1
    #
    # validaciónes en cliente: checkear todos los ruts existen
      CSV.foreach(data.tempfile, encoding: Encoding::UTF_8,headers: true, :col_sep => "\t") do |row, index|
        empleado = Empleado.find_by_rut(row["Rut"])
        row_index = row_index+1
        if empleado
          prta = ProgramaTalento.find_by(empleado_id: empleado.id, periodo: periodo) || empleado.programa_talentos.new
          prta.rut = row["Rut"]
          prta.nombre = row["Programa"]
          prta.estado = row["Estado"]
          prta.periodo = periodo
          prta.save!
        end
      end
      date = Date.strptime(periodo,"%m-%Y")
      division = "global"
      view_dotacion = ViewDotacion.started_before(date).finished_after(date).in_period(periodo).to_json
      # $redis.set("view_dotacion"+periodo+division, view_dotacion)
      return true
    rescue Exception => e
      puts e.message
      puts e.backtrace.inspect
      return row_index
    end
  end
end
