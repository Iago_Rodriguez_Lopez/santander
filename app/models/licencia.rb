# == Schema Information
#
# Table name: licencias
#
#  id                  :integer          not null, primary key
#  fecha               :date
#  glosa_tipo_licencia :string
#  empleado_id         :integer
#  fecha_inicio        :date
#  fecha_termino       :date
#  dias_totales        :integer
#  dias_corridos       :integer
#  dias_habiles        :integer
#  periodo             :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  rut                 :string
#  periodo_anualizado  :string
#  tipo_licencia       :string
#



class Licencia < ActiveRecord::Base
  belongs_to :empleado

  scope :in_period, ->(period) { where('periodo = ?', period) unless period.blank?}

  PARAMETROS = [:dias_totales, :dias_corridos_periodo, :dias_habiles_periodo, :glosa_tipo_licencia, :rut, :periodo]

  scope :with_text, -> (text) {
    queries = PARAMETROS.map do |p|
      "#{p.to_s} ILIKE '%#{text}%'"
    end

    where(queries.join(" OR "))
  }

  def self.csv_import(data, periodo)
    row_index = 0
    ruts = []
    licencias = []
    begin
      CSV.foreach(data.tempfile, encoding: Encoding::UTF_8,headers: true, :col_sep => "\t") do |row, index|
        row_index = row_index+1
        empleado = Empleado.find_by_rut(row["rut"])
        if empleado
            licencia = empleado.licencias.new
            licencia.rut = row["rut"]
            licencia.dias_totales = row["dias_totales"]
            licencia.dias_corridos = row["dias_corridos_periodo"]
            licencia.dias_habiles = row["dias_habiles_periodo"]
            licencia.glosa_tipo_licencia = row["glosa_tipo_licencia"]
            # licencia.fecha_inicio = Date.strptime(row["fecha_inicio"], "%d/%m/%Y")
            # licencia.fecha_inicio = Date.strptime(row["fecha_termino"], "%d/%m/%Y")
            licencia.periodo = periodo
            licencia.periodo_anualizado = Date.strptime(row["periodo"], "%Y%m").strftime("%m-%Y")

            licencias << licencia
        else
          ruts << row["rut"].first(10)
        end
      end
      Licencia.import licencias
      return ruts
    rescue Exception => e
      puts e.message
      puts e.backtrace.inspect
      return row_index
    end
  end

end
