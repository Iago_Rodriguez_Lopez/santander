# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :inet
#  last_sign_in_ip        :inet
#  failed_attempts        :integer          default(0), not null
#  unlock_token           :string
#  locked_at              :datetime
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  invitation_token       :string
#  invitation_created_at  :datetime
#  invitation_sent_at     :datetime
#  invitation_accepted_at :datetime
#  invitation_limit       :integer
#  invited_by_type        :string
#  invited_by_id          :integer
#  invitations_count      :integer          default(0)
#  metadata               :jsonb
#  rol                    :integer          default(0), not null
#  accesos                :string           default([]), is an Array
#  name                   :string
#

class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable, :lockable

  after_initialize :default_metadata

  validates :rut, presence: true
  validates :rut, uniqueness: true

  validates :email, presence: true

  #has many
  has_many :user_logs

  def role
    case rol
    when 0
      "Divisional"
    when 1
      "Manager"
    when 2
      "Administrador"
    end
  end

  def nombre_completo
    "#{metadata["nombres"]} #{metadata["apellidos"]}"
  end

  def rol_to_s
    case rol
    when 2
      "Administrador"
    when 1
      "Manager"
    when 0
      "Divisional"
    end
  end


  def divisiones
    accesos.map{|acceso| acceso[0..(acceso.size-3)]}
  end

  private

  def default_metadata
    self.metadata ||= {}
  end
end
