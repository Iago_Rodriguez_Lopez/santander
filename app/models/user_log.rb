class UserLog < ActiveRecord::Base
   belongs_to :user 

   validates :division, presence: true
   validates :gerencia, presence: true
   validates :period, presence: true
   validates :source, presence: true
end
