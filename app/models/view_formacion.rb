# == Schema Information
#
# Table name: formacion_view
#
#  id                  :integer
#  rut                 :string
#  sexo                :string
#  fecha_ingreso_grupo :date
#  fecha_nacimiento    :date
#  fecha_renuncia      :date
#  tipo_renuncia       :string
#  nombre              :string
#  apellidos           :string
#  division            :string
#  gerencia            :string
#  periodo             :string
#  piramide            :string
#  tipo_contrato       :string
#  nombre_empresa      :string
#  tipo_renta          :string
#  cargo               :string
#  estado_civil        :string
#  num_hijos           :integer
#  periodo_formacion   :string
#  materia             :string
#  horas               :integer
#  nombre_del_curso    :string
#  modalidad           :string
#  asistencia          :string
#  es_plantilla        :boolean
#

class ViewFormacion < ActiveRecord::Base
  self.table_name = 'formacion_view'
  after_initialize :readonly!


  scope :in_period, ->(period) { where('periodo = ?', period) unless period.blank?}
  scope :in_periods, ->(period, period_old) { where('periodo = ? or periodo = ?', period, period_old) unless period.blank? and period_old.blank?}
  scope :active, -> {where("es_plantilla = ?", true)}
  scope :renta_fija, -> {where("tipo_renta = ?", "Renta Fija")}
  scope :indefinido, -> {where("tipo_contrato = ?", "Indefinido")}
  scope :asistentes, -> {where("asistencia = ? or asistencia = ?", "Presente", "Pendiente")}

  scope :started_before, ->(date) { where('fecha_ingreso_grupo < ?', date)}
  scope :started_after, ->(date) { where('fecha_ingreso_grupo >= ?', date)}

  scope :finished_before, ->(date) { where('fecha_renuncia < ?', date)}
  scope :finished_after, ->(date) { where('fecha_renuncia >= ?', date)}
  scope :in_division, ->(division) { where('division = ?', division) unless (division.blank? or division=="Todas" or division=="Global" or division=="global")}

end
