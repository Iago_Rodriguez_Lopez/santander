# == Schema Information
#
# Table name: dotacion_view
#
#  id                  :integer
#  rut                 :string
#  nombre              :string
#  apellidos           :string
#  sexo                :string
#  fecha_ingreso_grupo :date
#  fecha_nacimiento    :date
#  fecha_renuncia      :date
#  tipo_renuncia       :string
#  division            :string
#  gerencia            :string
#  periodo             :string
#  piramide            :string
#  tipo_contrato       :string
#  nombre_empresa      :string
#  tipo_renta          :string
#  cargo               :string
#  estado_civil        :string
#  num_hijos           :integer
#  tipo_area           :string
#  es_plantilla        :boolean
#

class ViewSaldo < ActiveRecord::Base
  self.table_name = 'saldo_view'
  after_initialize :readonly!

  scope :renta_fija, -> {where("tipo_renta = ?", "Renta Fija")}
  scope :indefinido, -> {where("tipo_contrato = ?", "Indefinido")}
  scope :active, -> {where("es_plantilla = ?", true)}
  scope :in_period, ->(period) { where('periodo = ?', period) unless period.blank?}
  scope :started_before, ->(date) { where('fecha_ingreso_grupo < ?', date)}
  scope :started_after, ->(date) { where('fecha_ingreso_grupo >= ?', date)}

  scope :finished_before, ->(date) { where('fecha_renuncia < ?', date)}
  scope :finished_after, ->(date) { where('fecha_renuncia >= ?', date)}
  scope :in_division, ->(division) { where('division = ?', division) unless (division.blank? or division=="Todas" or division=="Global" or division=="global")}
  scope :altas, ->(start_date, end_date) { where('fecha_ingreso_grupo between ? and ?', start_date, end_date)}
  scope :bajas, ->(start_date, end_date) { where('fecha_renuncia between ? and ?', start_date, end_date)}

end
