# == Schema Information
#
# Table name: posiciones
#
#  id             :integer          not null, primary key
#  piramide       :string
#  tipo_contrato  :string
#  glosa_contrato :string
#  division       :string
#  gerencia       :string
#  area           :string
#  subarea        :string
#  periodo        :string
#  empleado_id    :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  rut            :string
#  nombre_empresa :string
#  es_plantilla   :boolean          default(TRUE)
#  tipo_renta     :string
#  cargo          :string
#  estado_civil   :string
#  num_hijos      :integer
#

class Posicion < ActiveRecord::Base
  belongs_to :empleado

  scope :in_date, ->(start_date, end_date) { where('periodo >= ? AND periodo <= ?', start_date, end_date) unless start_date.blank? and end_date.blank?}
  scope :in_period, ->(period) { where('periodo = ?', period) unless period.blank?}
  scope :in_division, ->(division) { where('division = ?', division) unless division.blank?}

  PARAMETROS = [:piramide, :tipo_contrato, :glosa_contrato, :division, :gerencia, :area, :subarea, :rut, :nombre_empresa, :tipo_renta, :cargo, :estado_civil]

  scope :with_text, -> (text) {
    queries = PARAMETROS.map do |p|
      "#{p.to_s} ILIKE '%#{text}%'"
    end

    where(queries.join(" OR "))
  }

    # validates :empleado_id, :periodo, presence: :true
  validates :periodo, uniqueness: {scope: :empleado_id}


  # def rut
  #   if self.empleado
  #     return self.empleado.rut
  #   else
  #     return ''
  #   end
  # end

  # def self.csv_import(data, periodo)
  #   row_index = 0
  #   ruts = []
  #   posiciones = []
  #   begin
  #     CSV.foreach(data.tempfile, encoding: Encoding::UTF_8,headers: true, :col_sep => "\t") do |row, index|
  #       empleado = Empleado.find_by_rut(row["rut"])
  #       row_index = row_index+1
  #       if empleado
  #         posicion = Posicion.find_by(empleado_id: empleado.id, periodo: periodo) || empleado.posiciones.new
  #         posicion.rut = row["rut"]
  #         posicion.piramide = row["Piramide"]
  #         posicion.tipo_contrato = row["Contrato"]
  #         posicion.division = row["División_UR"]
  #         posicion.gerencia = row["Gerencia_UR"]
  #         posicion.nombre_empresa = row["Empresa"]
  #         posicion.es_plantilla = row["considero España"] == "n" ?  false : true
  #         posicion.tipo_renta = row["Tipo Renta"]
  #         posicion.cargo = row["Cargo"]
  #         posicion.estado_civil = row["Estado Civil"]
  #         posicion.num_hijos = row["Num Hijos"]
  #         posicion.periodo = periodo
  #         posicion.save!
  #       else
  #         ruts << row["rut"]
  #       end
  #     end
  #     # $redis.flushdb()
  #     return ruts
  #   rescue Exception => e
  #     puts e.message
  #     puts e.backtrace.inspect
  #     return row_index
  #   end
  # end


  def self.csv_import(data, periodo)
    row_index = 0
    ruts = []
    posiciones = []
    begin
      CSV.foreach(data.tempfile, encoding: Encoding::UTF_8,headers: true, :col_sep => "\t") do |row, index|
        empleado = Empleado.find_by_rut(row["rut"])
        row_index = row_index+1
        if empleado
          posicion = empleado.posiciones.new
          posicion.rut = row["rut"]
          posicion.piramide = row["Piramide"]
          posicion.tipo_contrato = row["Contrato"]
          posicion.division = row["División_UR"]
          posicion.gerencia = row["Gerencia_UR"]
          posicion.nombre_empresa = row["Empresa"]
          posicion.es_plantilla = row["considero España"] == "n" ?  false : true
          posicion.tipo_renta = row["Tipo Renta"]
          posicion.cargo = row["Cargo"]
          posicion.estado_civil = row["Estado Civil"]
          posicion.num_hijos = row["Num Hijos"]
          posicion.saldo_vacaciones = row["saldo vacaciones"]
          posicion.periodos_pendientes_vacaciones = row["periodos pendientes vacaciones"]
          posicion.periodo = periodo
          posiciones << posicion
        else
          ruts << row["rut"]
        end
      end
      Posicion.import posiciones

      # $redis.flushdb()
      return ruts
    rescue Exception => e
      puts e.message
      puts e.backtrace.inspect
      return row_index
    end
  end

end
