# == Schema Information
#
# Table name: movimientos
#
#  id                 :integer          not null, primary key
#  empleado_id        :integer
#  periodo            :string
#  tipo_movimiento    :string
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  rut                :string
#  periodo_anualizado :string
#

class Movimiento < ActiveRecord::Base
  belongs_to :empleado
  belongs_to :posicion, class_name: 'Posicion', foreign_key: 'posicion_origen_id'

  
  scope :in_period, ->(period) { where('periodo = ?', period) unless period.blank?}

  PARAMETROS = [:periodo, :tipo_movimiento, :rut]

  scope :with_text, -> (text) {
    queries = PARAMETROS.map do |p|
      "#{p.to_s} ILIKE '%#{text}%'"
    end

    where(queries.join(" OR "))
  }

  def self.csv_import(data, periodo)
    # csv_handler = File.open(data.tempfile)
    row_index = 0
    ruts = []
    movimientos = []
    begin
      CSV.foreach(data.tempfile, encoding: Encoding::UTF_8,headers: true, :col_sep => "\t") do |row, index|
        empleado = Empleado.find_by_rut(row["pos_rut"])
        row_index = row_index+1
        if empleado
            movimiento = empleado.movimientos.new
            movimiento.rut = row["pos_rut"]
            movimiento.tipo_movimiento = row["tipo_cambio_posicion"]
            movimiento.periodo = periodo
            # posicion = Posicion.where(rut: movimiento.rut, periodo: (Date.strptime(movimiento.periodo_anualizado, "%m-%Y")-1.month).strftime("%m-%Y")).first
            movimiento.periodo_anualizado = Date.strptime(row["pos_periodo"], "%Y%m").strftime("%m-%Y")
            posicion = Posicion.where(rut: movimiento.rut, periodo: (Date.strptime(movimiento.periodo_anualizado, "%m-%Y")-1.month).strftime("%m-%Y")).first
            movimiento.posicion = posicion
            movimientos << movimiento
        else
          ruts << row["pos_rut"]
        end
      end
      Movimiento.import movimientos
      return ruts
    rescue Exception => e
      puts e.message
      puts e.backtrace.inspect
      return row_index
    end
  end

  def self.add_origen
    Movimiento.where("periodo in (?)",["09-2017", "10-2017", "11-2017"]).each do |movimiento|
      # puts "ANTIGUO: "+movimiento.posicion.id.to_s if movimiento.posicion
      posicion = Posicion.where(rut: movimiento.rut, periodo: (Date.strptime(movimiento.periodo_anualizado, "%m-%Y")-1.month).strftime("%m-%Y")).first
      # if posicion 
      #   puts "NUEVO: "+posicion.id.to_s
      # else 
      #   puts "MOVIMIENTO SIN POSICION: "
      # end
      movimiento.posicion = posicion
      movimiento.save
    end
  end

end
