# == Schema Information
#
# Table name: retribuciones
#
#  id                 :integer          not null, primary key
#  empleado_id        :integer
#  periodo            :string
#  periodo_anualizado :string
#  tipo_movimiento    :string
#  resto_año          :decimal(10, 2)
#  anual              :decimal(10, 2)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  rut                :string
#  porcentaje         :decimal(2, 2)
#

class Retribucion < ActiveRecord::Base
  belongs_to :empleado
  scope :in_period, ->(period) { where('periodo = ?', period) unless period.blank?}

  PARAMETROS = [:tipo_movimiento, :rut]

  scope :with_text, -> (text) {
    queries = PARAMETROS.map do |p|
      "#{p.to_s} ILIKE '%#{text}%'"
    end

    where(queries.join(" OR "))
  }

  def self.csv_import(data, periodo)
    row_index = 0
    ruts = []
    retribuciones = []
    # csv_handler = File.open(data.tempfile)
    begin
      CSV.foreach(data.tempfile, encoding: Encoding::UTF_8,headers: true, :col_sep => "\t") do |row, index|
        empleado = Empleado.find_by_rut(row["Sim Rutfun"])
        row_index = row_index+1
        if empleado
            retribucion = empleado.retribuciones.new
            retribucion.rut = row["Sim Rutfun"]
            retribucion.tipo_movimiento = row["Tipo Mov"]
            retribucion.resto_año = row["Resto Año"]
            retribucion.anual = row["12 Meses"]
            retribucion.porcentaje = row["porcentaje"]
            retribucion.periodo_anualizado = Date.strptime(row["período"], "%Y%m").strftime("%m-%Y")
            retribucion.periodo = periodo
            retribuciones << retribucion
        else
          ruts << row["Sim Rutfun"]
        end
      end
      Retribucion.import retribuciones
      return ruts
    rescue Exception => e
      puts e.message
      puts e.backtrace.inspect
      return row_index
    end
  end

end
