# == Schema Information
#
# Table name: vacaciones
#
#  id          :integer          not null, primary key
#  conseguidos :decimal(10, 2)
#  consumidos  :decimal(10, 2)
#  saldo       :decimal(10, 2)
#  empleado_id :integer
#  periodo     :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  rut         :string
#

class Vacacion < ActiveRecord::Base
  belongs_to :empleado
  scope :in_period, ->(period) { where('periodo = ?', period) unless period.blank?}

  PARAMETROS = [:rut]

  scope :with_text, -> (text) {
    queries = PARAMETROS.map do |p|
      "#{p.to_s} ILIKE '%#{text}%'"
    end

    where(queries.join(" OR "))
  }


  def self.csv_import(data, periodo)
    row_index = 0
    ruts = []
    vacaciones = []
    # csv_handler = File.open(data.tempfile)
    begin
      CSV.foreach(data.tempfile, encoding: Encoding::UTF_8,headers: true, :col_sep => "\t") do |row, index|
        empleado = Empleado.find_by_rut(row["rut"])
        row_index = row_index+1
        if empleado
            vacacion = empleado.vacaciones.new
            vacacion.rut = row["rut"]
            vacacion.saldo = row["saldo"]
            vacacion.consumidos = row["consumo"]
            vacacion.dias_10 = row["10_dias"] == "SI" ? true : false
            vacacion.periodos_pendientes = row["periodos pendientes"] || 0
            vacacion.periodo_anualizado = Date.strptime(row["periodo"], "%Y%m").strftime("%m-%Y")
            vacacion.periodo = periodo
            vacaciones << vacacion
        else
          ruts << row["sol_rut"]
        end
      end
      Vacacion.import vacaciones
      return ruts
    rescue Exception => e
      puts e.message
      puts e.backtrace.inspect
      return row_index
    end
  end

end
