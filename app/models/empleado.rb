# == Schema Information
#
# Table name: empleados
#
#  id                  :integer          not null, primary key
#  rut                 :string
#  sexo                :string
#  fecha_ingreso_grupo :date
#  fecha_nacimiento    :date
#  fecha_renuncia      :date
#  tipo_renuncia       :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  motivo_renuncia     :string
#  nombre              :string
#  apellidos           :string
#

require 'csv'

class Empleado < ActiveRecord::Base
  has_many :formaciones
  has_many :licencias
  has_many :posiciones
  has_many :vacaciones
  has_many :prezos
  has_many :empresas
  has_many :movimientos
  has_many :programa_talentos
  has_many :retribuciones
  has_many :egresos

  validates :rut, presence: :true
  validates :rut, uniqueness: :true


  PARAMETROS = [:rut, :motivo_renuncia, :nombre, :apellidos]

  scope :with_text, -> (text) {
    queries = PARAMETROS.map do |p|
      "#{p.to_s} ILIKE '%#{text}%'"
    end

    where(queries.join(" OR "))
  }

  def edad
    now = Time.now.utc.to_date
    now.year - self.fecha_nacimiento.year - ((now.month > self.fecha_nacimiento.month || (now.month == self.fecha_nacimiento.month && now.day >= self.fecha_nacimiento.day)) ? 0 : 1)
  end

  def rut_beautified
    result = rut.dup # We need a new copy!
    result[0] = '' if result[0] == '0'
    result[0] = '' if result[0] == '0'

    principal, dv = result.split("-")
    principal = principal.reverse.scan(/.{1,3}/).join(".").reverse

    "#{principal}-#{dv}"
  end

  def rut_raw
    result = rut.dup

    result[0] = '' if result[0] == '0'
    result[0] = '' if result[0] == '0'
    result.delete("-")
  end

  def self.csv_import(data, date)
    # csv_handler = File.open(data.tempfile)
    row_index = 0;

    begin
      
      CSV.foreach(data.tempfile, encoding: Encoding::UTF_8,headers: true, :col_sep => "\t") do |row, index|
        row_index = row_index + 1
        # row = row.to_hash
        #if !row["rut"].nil?
          empleado = find_by_rut(row[row.headers[0]]) || Empleado.new(rut: row[row.headers[0]])
          empleado.sexo = row["Sexo"]
          empleado.fecha_nacimiento = Date.strptime(row["Nacimiento"], "%d/%m/%y")
          if empleado.fecha_nacimiento > Date.today
            empleado.fecha_nacimiento = Date.new(empleado.fecha_nacimiento.year - 100, empleado.fecha_nacimiento.month, empleado.fecha_nacimiento.mday)
          end
          empleado.fecha_ingreso_grupo = Date.strptime(row["Inicio"], "%d/%m/%y")
          if empleado.fecha_ingreso_grupo > Date.today
            empleado.fecha_ingreso_grupo = Date.new(empleado.fecha_ingreso_grupo.year - 100, empleado.fecha_ingreso_grupo.month, empleado.fecha_ingreso_grupo.mday)
          end
          empleado.nombre = row["Nombres"]
          empleado.apellidos = row["Apellidos"]

          empleado.fecha_renuncia = Date.strptime(row["Termino"], "%d/%m/%y")

          if empleado.fecha_renuncia == Date.new(1999, 12, 31)
            empleado.fecha_renuncia = Date.new(202999, 12, 31)
          end
          # empleado.fecha_renuncia = Date.new(("20"+row["F Término Grupo"].split("-")[2]).to_i,row["F Término Grupo"].split("-")[1].to_i, row["F Término Grupo"].split("-")[0].to_i)

          empleado.save!
        #end
      end
      # $redis.flushdb()
      return true
    rescue Exception => e
      puts row_index
      puts e.message
      puts e.backtrace.inspect
      return false
    end
  end

  def self.csv_import_bajas(data, date)
    # csv_handler = File.open(data.tempfile)
    row_index = 0;
    begin
    month_names = [nil, "ene", "feb", "mar", "abr", "may", "jun", "jul", "ago", "sep", "oct", "nov", "dic"]
      CSV.foreach(data.tempfile, encoding: Encoding::UTF_8,headers: true, :col_sep => "\t") do |row, index|
        row = row.to_hash
        row_index = row_index+1
        empleado = Empleado.find_by_rut(row["rut"])
        if empleado
            empleado.fecha_renuncia = Date.strptime(row["PERIODO"], "%Y%m") if row["PERIODO"]
            empleado.tipo_renuncia = row["Tipo"]
            empleado.motivo_renuncia = row["CAUSA GRAVE?"]
            empleado.save!
        end
      end
      # $redis.flushdb()
      return true
    rescue Exception => e
      puts row_index
      puts e.message
      puts e.backtrace.inspect
      return row_index
    end
  end
end
