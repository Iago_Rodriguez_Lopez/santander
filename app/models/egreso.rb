# == Schema Information
#
# Table name: egresos
#
#  id                 :integer          not null, primary key
#  empleado_id        :integer
#  periodo            :string
#  periodo_anualizado :string
#  tipo_egreso        :string
#  rut                :string
#  division           :string
#  gerencia           :string
#  piramide           :string
#  tipo_contrato      :string
#  es_plantilla       :boolean          default(TRUE)
#  tipo_renta         :string
#  cargo              :string
#  estado_civil       :string
#  num_hijos          :integer
#


class Egreso < ActiveRecord::Base
  belongs_to :empleado
  scope :in_period, ->(period) { where('periodo = ?', period) unless period.blank?}

  PARAMETROS = [:division, :gerencia, :piramide, :tipo_contrato, :tipo_renta, :cargo, :estado_civil]

  scope :with_text, -> (text) {
    queries = PARAMETROS.map do |p|
      "#{p.to_s} ILIKE '%#{text}%'"
    end

    where(queries.join(" OR "))
  }

  def self.csv_import(data, periodo)
    # csv_handler = File.open(data.tempfile)
    row_index = 0
    ruts = []
    egresos = []
    begin
      CSV.foreach(data.tempfile, encoding: Encoding::UTF_8,headers: true, :col_sep => "\t") do |row, index|
        empleado = Empleado.find_by_rut(row["Rut"])
        row_index = row_index+1
        if empleado
            egreso = empleado.egresos.new
            egreso.rut = row["Rut"]
            egreso.periodo_anualizado = (Date.strptime(row["periodo"], "%Y%m")).strftime("%m-%Y") if row["periodo"]
            egreso.tipo_egreso = row["Tipo"]
            egreso.piramide = row["Piramide"]
            egreso.tipo_contrato = row["Contrato"]
            egreso.division = row["División_UR"]
            egreso.gerencia = row["Gerencia_UR"]
            egreso.es_plantilla = row["considero España"] == "n" ?  false : true
            egreso.tipo_renta = row["Tipo Renta"]
            egreso.cargo = row["Cargo"]
            egreso.estado_civil = row["Estado Civil"]
            egreso.num_hijos = row["Num Hijos"]
            egreso.periodo = periodo
            egresos << egreso
        else
          ruts << row["Rut"]
        end
      end
      Egreso.import egresos
      return ruts
    rescue Exception => e
      puts e.message
      puts e.backtrace.inspect
      return row_index
    end
  end

end
