class ManagersController < ApplicationController
	layout "manager_simplified", :only => [:entrada]
	layout "manager"
	before_filter :authenticate_user!, :except => [:entrada]
	before_filter :assign_divisional, :except => [:entrada]
  	# before_filter :authorize_admin!

	def index
		@periodo_list = (Date.today - 1.month).strftime("%m-%Y")
		if current_user.role == "Divisional"
			@divisiones_list = Vacante.active.in_user(current_user).pluck(:division).uniq.compact
		else
			@divisiones_list = Vacante.active.pluck(:division).uniq.compact
		end

	end


	def entrada
		# puts params.to_s
		if params[:cod_usr]
			user = User.find_by_rut(params[:cod_usr])
			if user
			  sign_in(:user, user)
			  redirect_to root_path
			else
			  redirect_to root_path, :flash => { :error => "No tienes acceso!" }
			end
		end
	end
	
	def dotaciones
		@current_period = @periodo || (Date.today - 1.month).strftime("%m-%Y")
		@current_date = Date.strptime(@current_period, "%m-%Y")
		@periodos = (((@current_date - 1.year).beginning_of_year)..(@current_date - 1.month)).map{|d| d.strftime("%m-%Y")}.uniq
		@temp_data = ViewDotacion.all.group('periodo').order('count_id asc').count('id')
		@months = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]
		@months2 = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
		@current_month = @months[(@current_date).month-1]
		@current_month2 = @months2[(@current_date).month-1]
		@year = @current_date.year
		@prev_year = (@current_date - 1.year).year
		@line_data = Array.new(12, 0)
		@bar_data = Array.new(12, 0)

		# @months.each_with_index do |month, index|
		# 	date = Date.new(@prev_year,(index+1), 1)
		# 	@line_data[index] = ViewDotacion.active.in_period(date.strftime("%m-%Y")).in_division(@current_division).pluck(:rut).uniq.count || 0
		# 	date = Date.new(@year,(index+1), 1)
		# 	@bar_data[index] = ViewDotacion.active.in_period(date.strftime("%m-%Y")).in_division(@current_division).pluck(:rut).uniq.count || 0
		# end
		@line_data = ViewDotacion.active.in_division(@current_division).where("periodo in (?)", (((@current_date-1.year).beginning_of_year)..(@current_date-1.year).end_of_year).map{|d| d.strftime("%m-%Y")}.uniq).group(:periodo).count.map{|key, value| value}
		aux2 = ViewDotacion.active.in_division(@current_division).where("periodo in (?)", ((@current_date.beginning_of_year)..@current_date.end_of_year).map{|d| d.strftime("%m-%Y")}.uniq).group(:periodo).count.map{|key, value| value}
		@bar_data = Array.new(12,0).map.with_index{|elem, index| aux2[index] ||  0 }
		
		# @months.each_with_index do |month, index|
		# 	date = Date.new(@year,(index+1), 1)
		# 	if (date <= @current_date)
		# 		#@bar_data[index] = ViewDotacion.active.started_before(date).finished_after(date).in_period(@current_period).in_division(@current_division).count || 0
		# 		@bar_data[index] = ViewDotacion.active.in_period(date.strftime("%m-%Y")).in_division(@current_division).pluck(:rut).uniq.count || 0

		# 	end
		# end
	end




	def rotaciones
		@current_period = @periodo || (Date.today - 1.month).strftime("%m-%Y")
		@current_date = Date.strptime(@current_period, "%m-%Y")

		@periodos = (((@current_date - 1.year).beginning_of_year)..(@current_date - 1.month)).map{|d| d.strftime("%m-%Y")}.uniq
		@prev_period = (@current_date - 1.year).strftime("%m-%Y")
		@periods = [@prev_period, @current_period]
		@temp_data = ViewDotacion.all.group('periodo').order('count_id asc').count('id')
		@months = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]
		@months2 = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
		@current_month = @months[(@current_date).month-1]
		@current_month2 = @months2[(@current_date).month-1]
		@year = @current_date.year
		@prev_year = (@current_date - 1.year).year
		@years = [@prev_year, @year]

		if (@current_division == "global" or @current_division == "")
			@current_by_division = ViewDotacion.active.in_division(@current_division).indefinido.where("periodo in (?)", @current_period).group(:division).count
			@old_by_division = ViewDotacion.active.in_division(@current_division).indefinido.where("periodo in (?)", @prev_period).group(:division).count
			@last_by_division = ViewDotacion.active.in_division(@current_division).indefinido.where("periodo in (?)", "12-"+@prev_year.to_s).group(:division).count
		else
			@current_by_division = ViewDotacion.active.in_division(@current_division).indefinido.where("periodo in (?)", @current_period).group(:gerencia).count
			@old_by_division = ViewDotacion.active.in_division(@current_division).indefinido.where("periodo in (?)", @prev_period).group(:gerencia).count
			@last_by_division = ViewDotacion.active.in_division(@current_division).indefinido.where("periodo in (?)", "12-"+@prev_year.to_s).group(:gerencia).count
		end
		
		@dotaciones_year_1 = ViewDotacion.active.in_division(@current_division).indefinido.where("periodo in (?)", (((@current_date-1.year).beginning_of_year)..(@current_date-1.year).end_of_year).map{|d| d.strftime("%m-%Y")}.uniq).group(:periodo).count.map{|key, value| value}
		aux2 = ViewDotacion.active.in_division(@current_division).indefinido.where("periodo in (?)", ((@current_date.beginning_of_year)..@current_date.end_of_year).map{|d| d.strftime("%m-%Y")}.uniq).group(:periodo).count.map{|key, value| value}
		@dotaciones_year_2 = Array.new(12,0).map.with_index{|elem, index| aux2[index] ||  0 }


	end




	def movimientos
		@current_period = @periodo || (Date.today - 1.month).strftime("%m-%Y")
		@current_date = Date.strptime(@current_period, "%m-%Y")

		@periodos = (((@current_date - 1.year).beginning_of_year)..(@current_date - 1.month)).map{|d| d.strftime("%m-%Y")}.uniq
		@prev_period = (@current_date - 1.year).strftime("%m-%Y")
		@periods = [@prev_period, @current_period]
		@temp_data = ViewDotacion.all.group('periodo').order('count_id asc').count('id')
		@months = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]
		@months2 = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
		@current_month = @months[(@current_date).month-1]
		@current_month2 = @months2[(@current_date).month-1]
		@year = @current_date.year
		@prev_year = (@current_date - 1.year).year
		@years = [@prev_year, @year]

		if (@current_division == "global" or @current_division == "")
			@current_by_division = ViewDotacion.active.in_division(@current_division).indefinido.where("periodo in (?)", @current_period).group(:division).count
			@old_by_division = ViewDotacion.active.in_division(@current_division).indefinido.where("periodo in (?)", @prev_period).group(:division).count
			@last_by_division = ViewDotacion.active.in_division(@current_division).indefinido.where("periodo in (?)", "12-"+@prev_year.to_s).group(:division).count
		else
			@current_by_division = ViewDotacion.active.in_division(@current_division).indefinido.where("periodo in (?)", @current_period).group(:gerencia).count
			@old_by_division = ViewDotacion.active.in_division(@current_division).indefinido.where("periodo in (?)", @prev_period).group(:gerencia).count
			@last_by_division = ViewDotacion.active.in_division(@current_division).indefinido.where("periodo in (?)", "12-"+@prev_year.to_s).group(:gerencia).count
		end
		@dotaciones_year_1 = ViewDotacion.active.in_division(@current_division).indefinido.where("periodo in (?)", (((@current_date-1.year).beginning_of_year)..(@current_date-1.year).end_of_year).map{|d| d.strftime("%m-%Y")}.uniq).group(:periodo).count.map{|key, value| value}
		aux2 = ViewDotacion.active.in_division(@current_division).indefinido.where("periodo in (?)", ((@current_date.beginning_of_year)..@current_date.end_of_year).map{|d| d.strftime("%m-%Y")}.uniq).group(:periodo).count.map{|key, value| value}
		@dotaciones_year_2 = Array.new(12,0).map.with_index{|elem, index| aux2[index] ||  0 }

	end

	def remuneraciones
		@current_period = @periodo || (Date.today - 1.month).strftime("%m-%Y")
		@current_date = Date.strptime(@current_period, "%m-%Y")
		@periodos = (((@current_date - 1.year).beginning_of_year)..(@current_date - 1.month)).map{|d| d.strftime("%m-%Y")}.uniq
		@prev_period = (@current_date - 1.year).strftime("%m-%Y")
		@periods = [@prev_period, @current_period]
		@temp_data = ViewDotacion.all.group('periodo').order('count_id asc').count('id')
		@months = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]
		@months2 = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
		@current_month = @months[(@current_date).month-1]
		@current_month2 = @months2[(@current_date).month-1]
		@year = @current_date.year
		@prev_year = (@current_date - 1.year).year
		@years = [@prev_year, @year]

		if (@current_division == "global" or @current_division == "")
			@current_by_division = ViewDotacion.active.in_division(@current_division).indefinido.renta_fija.where("periodo in (?)", @current_period).group(:division).count
			@old_by_division = ViewDotacion.active.in_division(@current_division).indefinido.renta_fija.where("periodo in (?)", @prev_period).group(:division).count
			@last_by_division = ViewDotacion.active.in_division(@current_division).indefinido.renta_fija.where("periodo in (?)", "12-"+@prev_year.to_s).group(:division).count
		else
			@current_by_division = ViewDotacion.active.in_division(@current_division).indefinido.renta_fija.where("periodo in (?)", @current_period).group(:gerencia).count
			@old_by_division = ViewDotacion.active.in_division(@current_division).indefinido.renta_fija.where("periodo in (?)", @prev_period).group(:gerencia).count
			@last_by_division = ViewDotacion.active.in_division(@current_division).indefinido.renta_fija.where("periodo in (?)", "12-"+@prev_year.to_s).group(:gerencia).count

		end

		@dotaciones_year_1 = ViewDotacion.active.in_division(@current_division).indefinido.renta_fija.where("periodo in (?)", (((@current_date-1.year).beginning_of_year)..(@current_date-1.year).end_of_year).map{|d| d.strftime("%m-%Y")}.uniq).group(:periodo).count.map{|key, value| value}
		aux2 = ViewDotacion.active.in_division(@current_division).indefinido.renta_fija.where("periodo in (?)", ((@current_date.beginning_of_year)..@current_date.end_of_year).map{|d| d.strftime("%m-%Y")}.uniq).group(:periodo).count.map{|key, value| value}
		@dotaciones_year_2 = Array.new(12,0).map.with_index{|elem, index| aux2[index] ||  0 }
	end


	def licencias
		@current_period = @periodo || (Date.today - 1.month).strftime("%m-%Y")
		@current_date = Date.strptime(@current_period, "%m-%Y")
		@periodos = (((@current_date - 1.year).beginning_of_year)..(@current_date - 1.month)).map{|d| d.strftime("%m-%Y")}.uniq
		@prev_period = (@current_date - 1.year).strftime("%m-%Y")
		@periods = [@prev_period, @current_period]
		@temp_data = ViewDotacion.all.group('periodo').order('count_id asc').count('id')
		@months = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]
		@months2 = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
		@current_month = @months[(@current_date).month-1]
		@current_month2 = @months2[(@current_date).month-1]
		@year = @current_date.year
		@prev_year = (@current_date - 1.year).year
		@years = [@prev_year, @year]



		@old_by_division_aux = ViewDotacion.active.in_division(@current_division).indefinido.where("periodo in (?)", (((@current_date-1.year).beginning_of_year)..(@current_date-1.year).end_of_year).map{|d| d.strftime("%m-%Y")}.uniq).group(:division, :periodo).count#.map{|key,value| {key[1] => {key[0]=> value}}}
		@current_by_division_aux = ViewDotacion.active.in_division(@current_division).indefinido.where("periodo in (?)", (((@current_date.beginning_of_year)..@current_date.end_of_year)).map{|d| d.strftime("%m-%Y")}.uniq).group(:division, :periodo).count#.map{|key,value| {key[1] => {key[0]=> value}}}
		
		@old_by_division = {}
		keys = @old_by_division_aux.keys 
		keys.each do |key|
			division = key[0]
			period = key[1]
			employees = @old_by_division_aux[key]
			@old_by_division = @old_by_division.merge({period => {division => employees}}) if @old_by_division[period] == nil
			@old_by_division[period] = (@old_by_division[period].merge({division => employees}))  unless @old_by_division[period] == nil
		end

		@current_by_division = {}
		keys = @current_by_division_aux.keys 
		keys.each do |key|
			division = key[0]
			period = key[1]
			employees = @current_by_division_aux[key]
			#@current_by_division[period] = {} if @current_by_division[period] == nil
			#{@current_by_division[period] => {division => employees}}  if @current_by_division[period] == nil
			@current_by_division = @current_by_division.merge({period => {division => employees}}) if @current_by_division[period] == nil
			@current_by_division[period] = (@current_by_division[period].merge({division => employees}))  unless @current_by_division[period] == nil
		end


		@dotaciones_year_1 = ViewDotacion.active.in_division(@current_division).indefinido.where("periodo in (?)", (((@current_date-1.year).beginning_of_year)..(@current_date-1.year).end_of_year).map{|d| d.strftime("%m-%Y")}.uniq).group(:periodo).count.map{|key, value| value}
		aux2 = ViewDotacion.active.in_division(@current_division).indefinido.where("periodo in (?)", ((@current_date.beginning_of_year)..@current_date.end_of_year).map{|d| d.strftime("%m-%Y")}.uniq).group(:periodo).count.map{|key, value| value}
		@dotaciones_year_2 = Array.new(12,0).map.with_index{|elem, index| aux2[index] ||  0 }
	end








	def formaciones
		@current_period = @periodo || (Date.today - 1.month).strftime("%m-%Y")
		@current_date = Date.strptime(@current_period, "%m-%Y")

		# @current_division = params[:division] || "global"
		# @subgroup = @current_division != "global" ?  Vacante.active.where(division: @current_division).in_period(@current_period).pluck(:gerencia).uniq  :  Vacante.active.in_period(@current_period).pluck(:division).uniq

		@periodos = (((@current_date - 1.year).beginning_of_year)..(@current_date - 1.month)).map{|d| d.strftime("%m-%Y")}.uniq
		@prev_period = (@current_date - 1.year).strftime("%m-%Y")
		@periods = [@prev_period, @current_period]
		@temp_data = ViewDotacion.all.group('periodo').order('count_id asc').count('id')
		@months = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]
		@months2 = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
		@current_month = @months[(@current_date).month-1]
		@current_month2 = @months2[(@current_date).month-1]
		@year = @current_date.year
		@prev_year = (@current_date - 1.year).year
		@years = [@prev_year, @year]

		if (@current_division == "global" or @current_division == "")
			@current_by_division = ViewDotacion.active.in_division(@current_division).renta_fija.where("periodo in (?)", @current_period).group(:division).count
			@old_by_division = ViewDotacion.active.in_division(@current_division).renta_fija.where("periodo in (?)", @prev_period).group(:division).count
			@last_by_division = ViewDotacion.active.in_division(@current_division).renta_fija.where("periodo in (?)", "12-"+@prev_year.to_s).group(:division).count
		else
			@current_by_division = ViewDotacion.active.in_division(@current_division).renta_fija.where("periodo in (?)", @current_period).group(:gerencia).count
			@old_by_division = ViewDotacion.active.in_division(@current_division).renta_fija.where("periodo in (?)", @prev_period).group(:gerencia).count
			@last_by_division = ViewDotacion.active.in_division(@current_division).renta_fija.where("periodo in (?)", "12-"+@prev_year.to_s).group(:gerencia).count

		end


		@dotaciones_year_1 = ViewDotacion.active.in_division(@current_division).renta_fija.where("periodo in (?)", (((@current_date-1.year).beginning_of_year)..(@current_date-1.year).end_of_year).map{|d| d.strftime("%m-%Y")}.uniq).group(:periodo).count.map{|key, value| value}
		aux2 = ViewDotacion.active.in_division(@current_division).renta_fija.where("periodo in (?)", ((@current_date.beginning_of_year)..@current_date.end_of_year).map{|d| d.strftime("%m-%Y")}.uniq).group(:periodo).count.map{|key, value| value}
		@dotaciones_year_2 = Array.new(12,0).map.with_index{|elem, index| aux2[index] ||  0 }

		# @dotaciones_year_1 = Array.new(12, 0)
		# @dotaciones_year_2 = Array.new(12, 0)
		# @months.each_with_index do |month, index|
		# 	date = Date.new(@prev_year,(index+1), 1)
		# 	@dotaciones_year_1[index] = ViewDotacion.active.in_period(date.strftime("%m-%Y")).in_division(@current_division).pluck(:rut).uniq.count || 0
		# 	date = Date.new(@year,(index+1), 1)
		# 	@dotaciones_year_2[index] = ViewDotacion.active.in_period(date.strftime("%m-%Y")).in_division(@current_division).pluck(:rut).uniq.count || 0

		# end

	end



	def vacaciones
		@current_period = @periodo || (Date.today - 1.month).strftime("%m-%Y")
		@current_date = Date.strptime(@current_period, "%m-%Y")

		@periodos = (((@current_date - 1.year).beginning_of_year)..(@current_date - 1.month)).map{|d| d.strftime("%m-%Y")}.uniq
		@prev_period = (@current_date - 1.year).strftime("%m-%Y")
		@periods = [@prev_period, @current_period]
		@temp_data = ViewDotacion.all.group('periodo').order('count_id asc').count('id')
		@months = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]
		@months2 = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
		@current_month = @months[(@current_date).month-1]
		@current_month2 = @months2[(@current_date).month-1]
		@year = @current_date.year
		@prev_year = (@current_date - 1.year).year
		@years = [@prev_year, @year]

		# @current_by_division = ViewDotacion.active.in_division(@current_division).indefinido.renta_fija.where("periodo in (?)", @current_period).group(:division).count
		# @old_by_division = ViewDotacion.active.in_division(@current_division).indefinido.renta_fija.where("periodo in (?)", @prev_period).group(:division).count
		# @last_by_division = ViewDotacion.active.in_division(@current_division).indefinido.renta_fija.where("periodo in (?)", "12-"+@prev_year.to_s).group(:division).count


		@dotaciones_year_1 = ViewDotacion.active.in_division(@current_division).renta_fija.where("periodo in (?)", (((@current_date-1.year).beginning_of_year)..(@current_date-1.year).end_of_year).map{|d| d.strftime("%m-%Y")}.uniq).group(:periodo).count.map{|key, value| value}
		aux2 = ViewDotacion.active.in_division(@current_division).renta_fija.where("periodo in (?)", ((@current_date.beginning_of_year)..@current_date.end_of_year).map{|d| d.strftime("%m-%Y")}.uniq).group(:periodo).count.map{|key, value| value}
		@dotaciones_year_2 = Array.new(12,0).map.with_index{|elem, index| aux2[index] ||  0 }


	end

	def saldo
		@current_period = @periodo || (Date.today - 1.month).strftime("%m-%Y")
		@current_date = Date.strptime(@current_period, "%m-%Y")
		@periodos = (((@current_date - 1.year).beginning_of_year)..(@current_date - 1.month)).map{|d| d.strftime("%m-%Y")}.uniq
		@prev_period = (@current_date - 1.year).strftime("%m-%Y")
		@periods = [@prev_period, @current_period]
		@temp_data = ViewDotacion.all.group('periodo').order('count_id asc').count('id')
		@months = ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"]
		@months2 = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"]
		@current_month = @months[(@current_date).month-1]
		@current_month2 = @months2[(@current_date).month-1]
		@year = @current_date.year
		@prev_year = (@current_date - 1.year).year
		@years = [@prev_year, @year]
		@dotaciones_year_1 = ViewDotacion.active.in_division(@current_division).renta_fija.where("periodo in (?)", (((@current_date-1.year).beginning_of_year)..(@current_date-1.year).end_of_year).map{|d| d.strftime("%m-%Y")}.uniq).group(:periodo).count.map{|key, value| value}
		aux2 = ViewDotacion.active.in_division(@current_division).renta_fija.where("periodo in (?)", ((@current_date.beginning_of_year)..@current_date.end_of_year).map{|d| d.strftime("%m-%Y")}.uniq).group(:periodo).count.map{|key, value| value}
		@dotaciones_year_2 = Array.new(12,0).map.with_index{|elem, index| aux2[index] ||  0 }

	end


	private
	def authorize_admin!
		if !current_user.has_role?("admin")
			redirect_to root_path, alert: "No autorizado"
			return
		end
	end

	def assign_divisional
		if (current_user.rol == 1 or  current_user.rol == 2)
			@current_division = params[:division] || "global"
		else
			if current_user.divisiones.include?(params[:division])
				@current_division = params[:division]
			else
				@current_division = current_user.divisiones.first
			end
		end
	end
end
