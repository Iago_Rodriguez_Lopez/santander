class RolesController < ApplicationController

  	before_filter :authenticate_user!
  	before_filter :authorize_admin!


	def update
		user = User.find(params[:user_id])
		if user.update_attributes(:role => params[:role])
			redirect_to users_path, notice: "Rol de usuario modificado"
		else
			redirect_to users_path, alert: "Error al modificar el rol del usuario"
		end
	end

	def authorize_admin!
		if !current_user.has_role?("admin") 
			redirect_to root_path, alert: "No autorizado"
			return
		end
	end

end
