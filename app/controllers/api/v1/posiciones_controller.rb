class Api::V1::PosicionesController < Api::V1::BaseController
        respond_to :json


  def index
    if ( !((params[:month] and !params[:month].empty?)  and (params[:year] and !(params[:year]).empty? )))
      period = Date.today.strftime("%m-%Y")
      @posiciones = Posicion.in_period(period)
    else
      period = params[:month]+"-"+params[:year]
      @posiciones = Posicion.in_period(period)
    end
    render json: serialize_models(@posiciones)
  end

  def show
    @posicion = Posicion.find(params[:id])
    render json: serialize_model(@posicion)
  end


  def create
    empleado = Empleado.find_by_rut(params[:data][:attributes][:rut])
    posicion = empleado.posiciones.new(posicion_params)
    if posicion.save
      render json: serialize_model(posicion), status: 201
    else
      render json: serialized_errors(posicion), status: 422
    end
  end

  def update
    posicion = Posicion.find(params[:id])
    attrs = params[:data][:attributes]
    if posicion.update_attributes(posicion_params)
      render json: serialize_model(posicion), status: 201
    else
      render json: serialized_errors(posicion), status: 422
    end
  end

  def destroy
    posicion = Posicion.find(params[:id])
    posicion.destroy!
    render json: "", status: 204

  end

  private


    def posicion_params
      params.require(:data).require(:attributes).permit(:piramide, :tipo_contrato, :glosa_contrato, :rut, :division, :gerencia, :area, :subarea, :periodo, :empleado_id, :es_plantilla, :nombre_empresa,  :tipo_renta, :cargo, :estado_civil, :num_hijos)
    end




end
