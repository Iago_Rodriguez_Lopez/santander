class Api::V1::ProgramaTalentosController < Api::V1::BaseController
        respond_to :json


  def index
    if (params[:month] and !(params[:month]).empty? and params[:year] and !(params[:year]).empty? )
      period = "0"+params[:month]+"-"+params[:year] if params[:month].length == 1
      period = params[:month]+"-"+params[:year] if params[:month].length == 2
      @programa_talentos = ProgramaTalento.in_period(period)
    else
      period = Date.today.strftime("%m-%Y")
      @programa_talentos = ProgramaTalento.in_period(period)
    end
    render json: serialize_models(@programa_talentos, include: ['empleado'])
  end

  def show
    @programa_talento = ProgramaTalento.find(params[:id])
    render json: serialize_model(@programa_talento, include: ['empleado'])
  end


  def create
    empleado = Empleado.find_by_rut(params[:data][:attributes][:rut])
    programa_talento = empleado.programa_talentos.new(programa_talento_params)
    if programa_talento.save
      render json: serialize_model(programa_talento, include: ['empleado']), status: 201
    else
      render json: serialized_errors(programa_talento), status: 422
    end
  end

  def update
    programa_talento = ProgramaTalento.find(params[:id])
    attrs = params[:data][:attributes]
    if programa_talento.update_attributes(programa_talento_params)
      render json: serialize_model(programa_talento, include: ['empleado']), status: 201
    else
      render json: serialized_errors(programa_talento), status: 422
    end
  end

  def destroy
    programa_talento = ProgramaTalento.find(params[:id])
    programa_talento.destroy!
    render json: "", status: 204

  end

  private


    def programa_talento_params
      params.require(:data).require(:attributes).permit(:nombre, :estado, :periodo, :empleado_id, :rut)
    end



end
