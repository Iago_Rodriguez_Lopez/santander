class Api::V1::BaseController < ActionController::Base
  # Convenience methods for serializing models:
  #
  before_filter :to_snake_case_params
  before_filter :check_token

  private
  def check_token
      #head :unauthorized unless request.headers["token"] == "0c29c432157a4ce49509f3d4bed5089c"
  end
  def serialize_model(model, options = {})
    options[:is_collection] = false
    JSONAPI::Serializer.serialize(model, options)
  end

  def serialize_models(models, options = {})
    options[:is_collection] = true
    JSONAPI::Serializer.serialize(models, options)
  end

  def serialized_errors(model)
    {errors: model.errors.messages.map{|k,v| {"detail": v[0], "source": {"pointer": "data/attributes/"+k.to_s}}}}
  end

  def to_snake_case_params
    params[:data].deep_transform_keys!(&:underscore) if (!params.nil? and !params[:data].nil?)
  end
end
