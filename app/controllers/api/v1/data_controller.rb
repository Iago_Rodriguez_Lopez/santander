class Api::V1::DataController < Api::V1::BaseController
  respond_to :json


    def dotaciones
      # if stale?("dotaciones"+params[:period]+params[:division], :last_modified => Time.current.beginning_of_day.utc)
      if params[:period].nil? or params[:period].blank?
        period = (Date.current-1.month).strftime("%m-%Y")
      else
        period = params[:period]
      end
      if params[:division].nil? or params[:division].blank? or params[:division] == "global"
        division = "global"
        # view_dotacion =  $redis.get("view_dotacion"+period+division)
        # if view_dotacion.nil?
        date = Date.strptime(period,"%m-%Y")
          #view_dotacion = ViewDotacion.active.started_before(date).finished_after(date).in_period(period).to_json
        view_dotacion = ViewDotacion.active.in_period(period).uniq.all_json
          # $redis.set("view_dotacion"+period+division, view_dotacion)
        # end
      else
        date = Date.strptime(period,"%m-%Y")
        division = params[:division]
        #view_dotacion = ViewDotacion.active.started_before(date).finished_after(date).in_period(period).in_division(division).to_json
        view_dotacion = ViewDotacion.active.in_period(period).in_division(division).uniq.all_json
      end
      render json: view_dotacion
    end



    def rotaciones
      # if stale?("dotaciones"+params[:period]+params[:division], :last_modified => Time.current.beginning_of_day.utc)
      if params[:period].nil? or params[:period].blank?
        period = (Date.current-1.month).strftime("%m-%Y")
        period_old = (Date.current-1.year).end_of_year.strftime("%m-%Y")
      else
        period = params[:period]
        period_old = (Date.strptime(period,"%m-%Y")-1.year).end_of_year.strftime("%m-%Y")
      end
      if params[:division].nil? or params[:division].blank? or params[:division] == "global"
        division = "global"
        date = Date.strptime(period,"%m-%Y")
        view_rotacion = ViewRotacion.active.in_periods(period, period_old).indefinido.to_json
      else
        date = Date.strptime(period,"%m-%Y")
        division = params[:division]
        view_rotacion = ViewRotacion.active.in_periods(period, period_old).in_division(division).indefinido.to_json
        # view_dotacion = ViewDotacion.finished_before((date-1.year).beginning_of_year).finished_after(date).in_period(period).in_division(division).to_json
      end
      render json: view_rotacion

    end

    def movimientos
      # if stale?("dotaciones"+params[:period]+params[:division], :last_modified => Time.current.beginning_of_day.utc)
      if params[:period].nil? or params[:period].blank?
        period = (Date.current-1.month).strftime("%m-%Y")
        period_old = (Date.current-1.year).end_of_year.strftime("%m-%Y")
      else
        period = params[:period]
        period_old = (Date.strptime(period,"%m-%Y")-1.year).end_of_year.strftime("%m-%Y")
      end
      if params[:division].nil? or params[:division].blank? or params[:division] == "global"
        division = "global"
        date = Date.strptime(period,"%m-%Y")
        view_movimiento = ViewMovimiento.main_types.active.in_periods(period, period_old).indefinido.to_json
      else
        date = Date.strptime(period,"%m-%Y")
        division = params[:division]
        view_movimiento = ViewMovimiento.main_types.active.in_periods(period, period_old).in_division(division).indefinido.to_json
        # view_dotacion = ViewDotacion.finished_before((date-1.year).beginning_of_year).finished_after(date).in_period(period).in_division(division).to_json
      end
      render json: view_movimiento

    end

    def formaciones
      # if stale?("dotaciones"+params[:period]+params[:division], :last_modified => Time.current.beginning_of_day.utc)
      if params[:period].nil? or params[:period].blank?
        period = (Date.current-1.month).strftime("%m-%Y")
        period_old = (Date.current-1.year).end_of_year.strftime("%m-%Y")
      else
        period = params[:period]
        period_old = (Date.strptime(period,"%m-%Y")-1.year).end_of_year.strftime("%m-%Y")
      end
      if params[:division].nil? or params[:division].blank? or params[:division] == "global"
        division = "global"
        # view_formacion =  $redis.get("view_formacion"+period+division)
        # if view_formacion.nil?
          date = Date.strptime(period,"%m-%Y")
          view_formacion = ViewFormacion.active.in_periods(period, period_old).asistentes.all_json
          # $redis.set("view_formacion"+period+division, view_formacion)
        # end
      else
        date = Date.strptime(period,"%m-%Y")
        division = params[:division]
        view_formacion = ViewFormacion.active.in_periods(period, period_old).in_division(division).asistentes.all_json
        # view_dotacion = ViewDotacion.finished_before((date-1.year).beginning_of_year).finished_after(date).in_period(period).in_division(division).to_json
      end
      render json: view_formacion

    end




    def remuneraciones
      # if stale?("dotaciones"+params[:period]+params[:division], :last_modified => Time.current.beginning_of_day.utc)
      if params[:period].nil? or params[:period].blank?
        period = (Date.current-1.month).strftime("%m-%Y")
        period_old = (Date.current-1.year).end_of_year.strftime("%m-%Y")
      else
        period = params[:period]
        period_old = (Date.strptime(period,"%m-%Y")-1.year).end_of_year.strftime("%m-%Y")

      end
      if params[:division].nil? or params[:division].blank? or params[:division] == "global"
        division = "global"
        date = Date.strptime(period,"%m-%Y")
        view_retribuciones = ViewRetribucion.in_periods(period, period_old).indefinido.renta_fija.to_json
      else
        date = Date.strptime(period,"%m-%Y")
        division = params[:division]
        view_retribuciones = ViewRetribucion.in_periods(period, period_old).in_division(division).indefinido.renta_fija.to_json
        # view_dotacion = ViewDotacion.finished_before((date-1.year).beginning_of_year).finished_after(date).in_period(period).in_division(division).to_json
      end
      render json: view_retribuciones
    end


    def licencias
      # if stale?("dotaciones"+params[:period]+params[:division], :last_modified => Time.current.beginning_of_day.utc)
      if params[:period].nil? or params[:period].blank?
        period = (Date.current-1.month).strftime("%m-%Y")
        period_old = (Date.current-1.year).end_of_year.strftime("%m-%Y")
      else
        period = params[:period]
        period_old = (Date.strptime(period,"%m-%Y")-1.year).end_of_year.strftime("%m-%Y")

      end
      if params[:division].nil? or params[:division].blank? or params[:division] == "global"
        division = "global"
        date = Date.strptime(period,"%m-%Y")
        view_licencias = ViewLicencia.in_periods(period, period_old).no_maternales.indefinido.all_json
      else
        date = Date.strptime(period,"%m-%Y")
        division = params[:division]
        view_licencias = ViewLicencia.in_periods(period, period_old).no_maternales.in_division(division).indefinido.all_json
        # view_dotacion = ViewDotacion.finished_before((date-1.year).beginning_of_year).finished_after(date).in_period(period).in_division(division).to_json
      end
      render json: view_licencias
    end


    def vacaciones
      # if stale?("dotaciones"+params[:period]+params[:division], :last_modified => Time.current.beginning_of_day.utc)
      if params[:period].nil? or params[:period].blank?
        period = (Date.current-1.month).strftime("%m-%Y")
        period_old = (Date.current-1.year).end_of_year.strftime("%m-%Y")
      else
        period = params[:period]
        period_old = (Date.strptime(period,"%m-%Y")-1.year).end_of_year.strftime("%m-%Y")

      end
      if params[:division].nil? or params[:division].blank? or params[:division] == "global"
        division = "global"
        date = Date.strptime(period,"%m-%Y")
        view_vacaciones = ViewVacacion.in_periods(period, period_old).indefinido.all_json
      else
        date = Date.strptime(period,"%m-%Y")
        division = params[:division]
        view_vacaciones = ViewVacacion.in_periods(period, period_old).in_division(division).indefinido.all_json
        # view_dotacion = ViewDotacion.finished_before((date-1.year).beginning_of_year).finished_after(date).in_period(period).in_division(division).to_json
      end
      render json: view_vacaciones
    end


    def saldo
      # if stale?("dotaciones"+params[:period]+params[:division], :last_modified => Time.current.beginning_of_day.utc)
      if params[:period].nil? or params[:period].blank?
        period = (Date.current-1.month).strftime("%m-%Y")
      else
        period = params[:period]
      end
      if params[:division].nil? or params[:division].blank? or params[:division] == "global"
        division = "global"
        # view_dotacion =  $redis.get("view_dotacion"+period+division)
        # if view_dotacion.nil?
        date = Date.strptime(period,"%m-%Y")
          #view_dotacion = ViewDotacion.active.started_before(date).finished_after(date).in_period(period).to_json
        view_saldo = ViewSaldo.active.in_period(period).uniq.all_json
          # $redis.set("view_dotacion"+period+division, view_dotacion)
        # end
      else
        date = Date.strptime(period,"%m-%Y")
        division = params[:division]
        #view_dotacion = ViewDotacion.active.started_before(date).finished_after(date).in_period(period).in_division(division).to_json
        view_saldo = ViewSaldo.active.in_period(period).in_division(division).uniq.all_json
      end
      render json: view_saldo
    end



    def estructura
      if params[:period].nil? or params[:period].blank?
        period = (Date.current-1.month).strftime("%m-%Y")
      else
        period = params[:period]
      end
      if params[:division].nil? or params[:division].blank? or params[:division] == "global"
        division = "global"
        view_estructura = Vacante.order(:division_order).active.in_period(period).pluck(:division).uniq.compact
      else
        division = params[:division]
        view_estructura = Vacante.order(:division_order).active.where(division: division).in_period(period).pluck(:gerencia).uniq.compact
      end
      render json: view_estructura
    end


    def estructura_licencias
      if params[:period].nil? or params[:period].blank?
        period = (Date.current-1.month).strftime("%m-%Y")
      else
        period = params[:period]
      end
      if params[:division].nil? or params[:division].blank? or params[:division] == "global"
        division = "global"
        date = Date.strptime(period,"%m-%Y")
        view_dotacion = ViewDotacion.select(:rut,:division, :gerencia, :piramide, :sexo, :fecha_nacimiento).active.in_period(period).uniq.all_json
      else
        date = Date.strptime(period,"%m-%Y")
        division = params[:division]
      view_dotacion = ViewDotacion.select(:rut,:division, :gerencia, :piramide, :sexo, :fecha_nacimiento).in_division(division).active.in_period(period).uniq.all_json

        # view_dotacion = ViewDotacion.finished_before((date-1.year).beginning_of_year).finished_after(date).in_period(period).in_division(division).to_json
      end
      
      render json: view_dotacion
    end

    def user_logs
      user_log = UserLog.new(user_log_params)
      user_log.user = current_user
      if user_log.save
        render json: true
      else
        render json: false
      end
    end

    private

    def user_log_params
      params.permit(:division, :gerencia, :period, :source)
    end

end

