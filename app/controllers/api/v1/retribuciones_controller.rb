class Api::V1::RetribucionesController < Api::V1::BaseController
        respond_to :json


  def index
    if ( params[:month] and !(params[:month]).empty? and params[:year] and !(params[:year]).empty? )
      period = params[:month]+"-"+params[:year]
      @retribuciones = Retribucion.in_period(period)
    else
      period = Date.today.strftime("%m-%Y")
      @retribuciones = Retribucion.in_period(period)
    end
    render json: serialize_models(@retribuciones)
  end

  def show
    @retribucion = Retribucion.find(params[:id])
    render json: serialize_model(@retribucion)
  end


  def create
    empleado = Empleado.find_by_rut(params[:data][:attributes][:rut])
    retribucion = empleado.retribuciones.new(retribucion_params)
    if retribucion.save
      render json: serialize_model(retribucion), status: 201
    else
      render json: serialized_errors(retribucion), status: 422
    end
  end

  def update
    retribucion = Retribucion.find(params[:id])
    attrs = params[:data][:attributes]
    if retribucion.update_attributes(retribucion_params)
      render json: serialize_model(retribucion), status: 201
    else
      render json: serialized_errors(retribucion), status: 422
    end
  end

  def destroy
    retribucion = Retribucion.find(params[:id])
    retribucion.destroy!
    render json: "", status: 204

  end

  private


    def retribucion_params
      params.require(:data).require(:attributes).permit(:tipo_retribucion, :tipo_movimiento, :resto_año, :anual, :periodo, :empleado_id, :rut, :periodo_anualizado)
    end



end
