class Api::V1::VacantesController < Api::V1::BaseController
  respond_to :json

  def index
    if params[:d].present? and params[:g].blank?
      respond_with Vacante.select(:division).where("division ILIKE ?", "%#{params[:d]}%").uniq.to_json
      return
    elsif params[:d].present? and params[:g].present?
      respond_with Vacante.select(:division, :gerencia).where("division = ? AND gerencia ILIKE ?", params[:d], "%#{params[:g]}%").uniq.to_json
    end
  end
end
