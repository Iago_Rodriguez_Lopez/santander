class Api::V1::EmpleadosController < Api::V1::BaseController
        respond_to :json


  def index
    # if stale?("empleados", :last_modified => Empleado.maximum(:updated_at))
      @empleados = Empleado.all
      render json: serialize_models(@empleados)
    # end
  end

  def show
    if params[:posicion_id]
      @empleado = Posicion.find(params[:posicion_id]).empleado
    elsif params[:licencia_id]
      @empleado = Licencia.find(params[:licencia_id]).empleado
    elsif params[:movimiento_id]
      @empleado = Movimiento.find(params[:movimiento_id]).empleado
    elsif params[:vacacion_id]
      @empleado = Vacacion.find(params[:vacacion_id]).empleado
    elsif params[:prezo_id]
      @empleado = Prezo.find(params[:prezo_id]).empleado
    elsif params[:formacion_id]
      @empleado = Formacion.find(params[:formacion_id]).empleado
    elsif params[:programa_talento_id]
      @empleado = ProgramaTalento.find(params[:programa_talento_id]).empleado
    elsif params[:retribucion_id]
      @empleado = Retribucion.find(params[:retribucion_id]).empleado
    elsif params[:egreso_id]
      @empleado = Egreso.find(params[:egreso_id]).empleado
    elsif params[:empresa_id]
      @empleado = Empresa.find(params[:empresa_id]).empleado
    else
      @empleado = Empleado.find(params[:id])
    end
    render json: serialize_model(@empleado)
  end


  def create
    empleado = Empleado.new(empleado_params)
    if empleado.save
      render json: serialize_model(empleado), status: 201
    else
      render json: serialized_errors(empleado), status: 422
    end
  end

  def update
    empleado = Empleado.find(params[:id])
    if empleado.update_attributes(empleado_params)

      render json: serialize_model(empleado), status: 200
      #render json: :ok
    else
      render json: serialized_errors(empleado), status: 422
    end
  end

  def destroy
    empleado = Empleado.find(params[:id])
    empleado.destroy!
    render json: "", status: 204
  end


  private

    def empleado_params
      params.require(:data).require(:attributes).permit(:rut, :sexo, :fecha_ingreso_grupo, :fecha_renuncia, :tipo_renuncia, :fecha_nacimiento, :motivo_renuncia, :nombre, :apellidos)
    end

end
