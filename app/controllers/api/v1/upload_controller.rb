class Api::V1::UploadController < Api::V1::BaseController
        respond_to :json

    def create
      file = params[:files][0]
      date = params[:month]+"-"+params[:year]
      if params[:model_type] == "bajas"
        result = Empleado.csv_import_bajas(file, date)
      else
        model = params[:model_type].classify.safe_constantize
        result = model.csv_import(file, date)
      end
      if result.is_a? Enumerable and result.empty?
        render :json => {:message => "Recibido y procesado correctamente."}, status: 200
      elsif result.is_a? Enumerable and !result.empty?
        render :json => {:message => "Recibido correctamente. No se han procesado los siguientes RUTs: "+ result.compact.to_s}, status: 200
      elsif result == 0
        render json: {:message => "Error al cargar archivo. Posible error de codificación o cabeceras."}, status: 200
      else
        render json: {:message => "Error al cargar archivo. El error se encuentra alrededor de la línea "+result.to_s+". Revisa el formato de los datos."}, status: 200
      end
    end


    def remove
      date = params[:month]+"-"+params[:year]
      model = params[:model_type].classify.safe_constantize
      if model.where("periodo = ?", date).delete_all
        render :json => {:message => "Eliminados correctamente."}, status: 200
      else
        render json: {:message => "Error al eliminar"}, status: 422
      end
    end

    def generate
      # $redis.flushdb()
      date = params[:month]+"-"+params[:year]
      Vacante.fill_vacantes(date)
      render json: {:message => "ok"}, status: 200
    end
end
