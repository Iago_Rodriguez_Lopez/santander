class Api::V1::UsersController < Api::V1::BaseController
        respond_to :json


  def index
    @users = User.all
    render json: serialize_models(@users)
  end

  def show
    @user = User.find(params[:id])
    render json: serialize_model(@user)
  end

  def create
   # Forma de devolver errores en persistencia para Ember Data
   # TODOS LOS CAMPOS SON OBLIGATORIOS
   ######################################################
   # render json: { errors: [{
   #    "detail": "Error en la validacion del email",  // Mensaje que se mostrará
   #    "source": {
   #      "pointer": "data/attributes/email"           // Atributo que da el error
   #    }
   #  }]
   # }, status: 422    // Siempre status 422 (Unprocessable Entity) para que se detecte como Ember Data error
   ######################################################
    attrs = params[:data][:attributes]
    user = User.new(name: attrs[:name], email: attrs[:email], password: attrs[:password])
    if user.save
      render json: serialize_model(user), status: 201
      #render json: :ok
    else
      render json: serialized_errors(user), status: 422
    end
  end

  def update
     # Forma de devolver errores en persistencia para Ember Data
     # TODOS LOS CAMPOS SON OBLIGATORIOS
     ######################################################
     # render json: { errors: [{
     #    "detail": "Error en la validacion del email",  // Mensaje que se mostrará
     #    "source": {
     #      "pointer": "data/attributes/email"           // Atributo que da el error
     #    }
     #  }]
     # }, status: 422    // Siempre status 422 (Unprocessable Entity) para que se detecte como Ember Data error
     ######################################################
    attrs=params[:data][:attributes]
    user = User.find(params[:id])
    if user.update_attributes(name: attrs[:name], email: attrs[:email], password: attrs[:password])
      # render json: :ok
      render json: serialize_model(user), status: 200
    else
      render json: serialized_errors(user), status: 422
    end
  end

  def destroy
    user = User.find(params[:id])
    user.destroy!
    render json: "", status: 204
  end


  private

end
