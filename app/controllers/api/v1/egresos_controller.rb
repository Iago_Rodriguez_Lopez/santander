class Api::V1::EgresosController < Api::V1::BaseController
        respond_to :json


  def index
    if ( params[:month] and !(params[:month]).empty? and params[:year] and !(params[:year]).empty? )
      period = params[:month]+"-"+params[:year]
      @egresos = Egreso.in_period(period)
    else
      period = Date.today.strftime("%m-%Y")
      @egresos = Egreso.in_period(period)
    end
    render json: serialize_models(@egresos)
  end

  def show
    @egreso = Egreso.find(params[:id])
    render json: serialize_model(@egreso)
  end


  def create
    empleado = Empleado.find_by_rut(params[:data][:attributes][:rut])
    egreso = empleado.egresos.new(egreso_params)
    if egreso.save
      render json: serialize_model(egreso), status: 201
    else
      render json: serialized_errors(egreso), status: 422
    end
  end

  def update
    egreso = Egreso.find(params[:id])
    attrs = params[:data][:attributes]
    if egreso.update_attributes(egreso_params)
      render json: serialize_model(egreso), status: 201
    else
      render json: serialized_errors(egreso), status: 422
    end
  end

  def destroy
    egreso = Egreso.find(params[:id])
    egreso.destroy!
    render json: "", status: 204

  end

  private


    def egreso_params
      params.require(:data).require(:attributes).permit(:tipo_egreso, :periodo, :empleado_id, :rut, :periodo_anualizado, :es_plantilla, :piramide, :tipo_contrato,  :division, :gerencia, :tipo_renta, :cargo, :estado_civil, :num_hijos)
    end



end
