class Api::V1::MovimientosController < Api::V1::BaseController
        respond_to :json


  def index
    if ( params[:month] and !(params[:month]).empty? and params[:year] and !(params[:year]).empty? )
      period = params[:month]+"-"+params[:year]
      @movimientos = Movimiento.in_period(period)
    else
      period = Date.today.strftime("%m-%Y")
      @movimientos = Movimiento.in_period(period)
    end
    render json: serialize_models(@movimientos)
  end

  def show
    @movimiento = Movimiento.find(params[:id])
    render json: serialize_model(@movimiento)
  end

  def create
    empleado = Empleado.find_by_rut(params[:data][:attributes][:rut])
    movimiento = empleado.movimientos.new(movimientos_params)
    if movimiento.save
      render json: serialize_model(movimiento), status: 201
    else
      render json: serialized_errors(movimiento), status: 422
    end
  end

  def update
    movimiento = Movimiento.find(params[:id])
    attrs = params[:data][:attributes]
    if movimiento.update_attributes(movimiento_params)
      render json: serialize_model(movimiento), status: 201
    else
      render json: serialized_errors(movimiento), status: 422
    end
  end

  def destroy
    movimiento = Movimiento.find(params[:id])
    movimiento.destroy!
    render json: "", status: 204

  end

  private


    def movimiento_params
      params.require(:data).require(:attributes).permit(:rut, :tipo_movimiento, :resto_año, :anual, :periodo, :empleado_id, :periodo_anualizado)
    end



end
