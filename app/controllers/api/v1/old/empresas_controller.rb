class Api::V1::EmpresasController < Api::V1::BaseController
        respond_to :json


  def index
    if ( params[:month] and !(params[:month]).empty? and params[:year] and !(params[:year]).empty? )
      period = params[:month]+"-"+params[:year]
      @empresas = Empresa.in_period(period)
    else
      period = Date.today.strftime("%m-%Y")
      @empresas = Empresa.in_period(period)
    end
    render json: serialize_models(@empresas)
  end

  def show
    @empresa = Empresa.find(params[:id])
    render json: serialize_model(@empresa)
  end


  def create

    empleado = Empleado.find_by_rut(params[:data][:attributes][:rut])
    empresa = empleado.empresas.new(empresa_params)

    if empresa.save
      render json: serialize_model(empresa), status: 201
    else
      render json: serialized_errors(empresa), status: 422
    end
  end

  def update
    empresa = Empresa.find(params[:id])
    attrs = params[:data][:attributes]
    if empresa.update_attributes(empresa_params)
      render json: serialize_model(empresa), status: 201
    else
      render json: serialized_errors(empresa), status: 422
    end
  end

  def destroy
    empresa = Empresa.find(params[:id])
    empresa.destroy!
    render json: "", status: 204

  end

  private
#  empleado_id :integer
#  periodo     :string
#  nombre      :string

    def empresa_params
      params.require(:data).require(:attributes).permit(:rut, :nombre, :periodo, :empleado_id)
    end


#  fecha               :date
#  glosa_tipo_empresa :string
#  empleado_id         :integer
#  fecha_inicio        :date
#  fecha_termino       :date
#  dias_totales        :integer
#  dias_corridos       :integer
#  dias_habiles        :integer
#  periodo             :string

end
