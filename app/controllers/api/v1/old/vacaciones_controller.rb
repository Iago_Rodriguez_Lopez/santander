class Api::V1::VacacionesController < Api::V1::BaseController
        respond_to :json

  def index
    if ( params[:month] and !(params[:month]).empty? and params[:year] and !(params[:year]).empty? )
      period = params[:month]+"-"+params[:year]
      @vacaciones = Vacacion.in_period(period)
    else
      period = Date.today.strftime("%m-%Y")
      @vacaciones = Vacacion.in_period(period)
    end
    render json: serialize_models(@vacaciones)
  end

  def show
    @vacacion = Vacacion.find(params[:id])
    render json: serialize_model(@vacacion)
  end

  def create
    empleado = Empleado.find_by_rut(params[:data][:attributes][:rut])
    vacacion = empleado.vacaciones.new(vacacion_params)
    if vacacion.save
      render json: serialize_model(vacacion), status: 201
    else
      render json: serialized_errors(vacacion), status: 422
    end
  end

  def update
    attrs = params[:data][:attributes]
    vacacion = Vacacion.find(params[:id])

    if vacacion.update_attributes(vacacion_params)
      render json: serialize_model(vacacion), status: 200
    else
      render json: serialized_errors(vacacion), status: 422
    end
  end

  def destroy
    vacacion = Vacacion.find(params[:id])
    vacacion.destroy!
    render json: "", status: 204

  end

  private

    def vacacion_params
      params.require(:data).require(:attributes).permit(:conseguidos, :consumidos, :saldo, :empleado_id, :periodo, :rut)
    end

end
