class Api::V1::PrezosController < Api::V1::BaseController
        respond_to :json

  def index
    if ( params[:month] and !(params[:month]).empty? and params[:year] and !(params[:year]).empty? )
      period = params[:month]+"-"+params[:year]
      @prezos = Prezo.in_period(period)
    else
      period = Date.today.strftime("%m-%Y")
      @prezos = Prezo.in_period(period)
    end
    render json: serialize_models(@prezos)
  end

  def show
    @prezo = Prezo.find(params[:id])
    render json: serialize_model(@prezo)
  end


  def create
    empleado = Empleado.find_by_rut(params[:data][:attributes][:rut])
    prezo = empleado.prezos.new(prezo_params)
    if prezo.save
      render json: serialize_model(prezo), status: 201
    else
      render json: serialized_errors(prezo), status: 422
    end
  end

  def update
    prezo = Prezo.find(params[:id])
    attrs = params[:data][:attributes]
    if prezo.update_attributes(prezo_params)
      render json: serialize_model(prezo), status: 201
    else
      render json: serialized_errors(prezo), status: 422
    end
  end

  def destroy
    prezo = Prezo.find(params[:id])
    prezo.destroy!
    render json: "", status: 204

  end

  private

    def prezo_params
      params.require(:data).require(:attributes).permit(:piramide, :tipo_contrato, :glosa_contrato, :division, :gerencia, :area, :subarea, :periodo, :empleado_id, :rut)
    end

end
