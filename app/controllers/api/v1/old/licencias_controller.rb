class Api::V1::LicenciasController < Api::V1::BaseController
        respond_to :json


  def index
    if ( params[:month] and !(params[:month]).empty? and params[:year] and !(params[:year]).empty? )
      period = params[:month]+"-"+params[:year]
      @licencias = Licencia.in_period(period)
    else
      period = Date.today.strftime("%m-%Y")
      @licencias = Licencia.in_period(period)
    end
    render json: serialize_models(@licencias)
  end

  def show
    @licencia = Licencia.find(params[:id])
    render json: serialize_model(@licencia)
  end


  def create
    empleado = Empleado.find_by_rut(params[:data][:attributes][:rut])
    licencia = empleado.licencias.new(licencia_params)
    if licencia.save
      render json: serialize_model(licencia), status: 201
    else
      render json: serialized_errors(licencia), status: 422
    end
  end

  def update
    licencia = Licencia.find(params[:id])
    attrs = params[:data][:attributes]
    if licencia.update_attributes(licencia_params)
      render json: serialize_model(licencia), status: 201
    else
      render json: serialized_errors(licencia), status: 422
    end
  end

  def destroy
    licencia = Licencia.find(params[:id])
    licencia.destroy!
    render json: "", status: 204

  end

  private


    def licencia_params
      params.require(:data).require(:attributes).permit(:fecha, :rut, :glosa_tipo_licencia, :fecha_inicio, :fecha_termino, :dias_totales, :dias_corridos, :dias_habiles, :periodo, :empleado_id, :periodo_anualizado)
    end


#  fecha               :date
#  glosa_tipo_licencia :string
#  empleado_id         :integer
#  fecha_inicio        :date
#  fecha_termino       :date
#  dias_totales        :integer
#  dias_corridos       :integer
#  dias_habiles        :integer
#  periodo             :string

end
