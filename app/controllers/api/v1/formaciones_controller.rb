class Api::V1::FormacionesController < Api::V1::BaseController
        respond_to :json

  def index

    if ( params[:month] and !(params[:month]).empty? and params[:year] and !(params[:year]).empty? )
      period = params[:month]+"-"+params[:year]
      @formaciones = Formacion.in_period(period)
    else
      period = Date.today.strftime("%m-%Y")
      @formaciones = Formacion.in_period(period)
    end
    render json: serialize_models(@formaciones)
  end

  def show
    @formacion = Formacion.find(params[:id])
    render json: serialize_model(@formacion)
  end

  def create
    empleado = Empleado.find_by_rut(params[:data][:attributes][:rut])
    formacion = empleado.formaciones.new(formacion_params)
    if formacion.save
      render json: serialize_model(formacion), status: 201
    else
      render json: serialized_error(formacion), status: 422
    end
  end

  def update
    attrs = params[:data][:attributes]
    formacion = Formacion.find(params[:id])

    if formacion.update_attributes(formacion_params)
      render json: serialize_model(formacion), status: 200
    else
      render json: serialized_error(formacion), status: 422
    end
  end

  def destroy
    formacion = Formacion.find(params[:id])
    formacion.destroy!
    render json: "",status: 204

  end

  private
    def formacion_params
      params.require(:data).require(:attributes).permit(:materia, :nombre_del_curso, :fecha_inicio, :fecha_fin, :horas, :periodo, :modalidad, :empleado_id, :rut, :periodo_anualizado, :asistencia)
    end

end
