class CompaniesController < ApplicationController
  layout "manager"
  before_filter :authenticate_user!
  before_action :set_company, only: [:show, :edit, :update, :destroy]

  before_filter :authorize_admin!, only: [:index, :new, :edit, :create, :update, :destroy]
  before_filter :authorize_it_manager!, only: [:show]


  # GET /companies
  # GET /companies.json
  def index
    @companies = Company.all
    @company = Company.new
  end

  # GET /companies/1
  # GET /companies/1.json
  def show
  end

  # GET /companies/new
  def new
    @company = Company.new
  end

  # GET /companies/1/edit
  def edit
  end

  # POST /companies
  # POST /companies.json
  def create
    @company = Company.new(company_params)
    respond_to do |format|
      if @company.save
        format.html { redirect_to @company, notice: 'Empresa creada.' }
        format.json { render :show, status: :created, location: @company }
      else
        format.html { render :new }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /companies/1
  # PATCH/PUT /companies/1.json
  def update
    respond_to do |format|
      if @company.update(company_params)
        format.html { redirect_to @company, notice: 'Empresa actualizada.' }
        format.json { render :show, status: :ok, location: @company }
      else
        format.html { render :edit }
        format.json { render json: @company.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /companies/1
  # DELETE /companies/1.json
  def destroy
    if @company.status == "activo"
        @company.update_attributes(status: "inactivo")
        redirect_to companies_path, notice: "Empresa desactivado"
    else
        @company.update_attributes(status: "activo")
        redirect_to companies_path, notice: "Empresa activado"
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_company
      @company = Company.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def company_params
      params.require(:company).permit(:name, :telephone, :logo, :company_code)
    end


  def authorize_it_manager!
    if (!current_user.has_role?("admin")) and (!current_user.has_role?("it_manager") and current_user.company and current_user.company == @company)
      redirect_to root_path, alert: "No autorizado"
      return
    end
  end

  def authorize_admin!
    if !current_user.has_role?("admin") 
      redirect_to root_path, alert: "No autorizado"
      return
    end
  end

end
