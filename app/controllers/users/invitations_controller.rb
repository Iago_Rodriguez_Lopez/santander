class Users::InvitationsController < Devise::InvitationsController
  def after_invite_path_for(resource)
    # resource is wrong, resource.is_a?(Admin) should be true
    users_path
  end
end