class Users::SessionsController < Devise::SessionsController
before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  # def new
  #   super
  # end

  # POST /resource/sign_in
  def create

    if params[:cod_usr]
      user = User.find_by_rut(params[:cod_user])
      if user
        sign_in(:user, user)
        redirect_to root_path
      else
        redirect_to root_path, :flash => { :error => "No tienes acceso!" }
      end
    elsif params[:user][:rut]
      user = User.find_by_rut(params[:user][:rut])
      if user
        sign_in(:user, user)
        redirect_to root_path
      else
        redirect_to root_path, :flash => { :error => "No tienes acceso!" }
      end
    else
      super
    end
  end

  # DELETE /resource/sign_out
  # def destroy
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_in_params
    devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  end
end
