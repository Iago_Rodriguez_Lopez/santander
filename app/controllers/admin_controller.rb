class AdminController < ActionController::Base
  # protect_from_forgery with: :exception
  layout "admin"

  #before_action :authenticate_user!
  before_action :ensure_admin

  private

  def ensure_admin
    not_authorized unless current_user.rol > 1
  end

  def not_authorized
    head :unauthorized
  end
end
