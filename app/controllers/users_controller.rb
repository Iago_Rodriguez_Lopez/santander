# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default(0), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  invalid                :boolean
#  company_id             :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#  name                   :string
#  role                   :integer
#  user_image             :string
#  invitation_token       :string
#  invitation_created_at  :datetime
#  invitation_sent_at     :datetime
#  invitation_accepted_at :datetime
#  invitation_limit       :integer
#  invited_by_id          :integer
#  invited_by_type        :string
#  invitations_count      :integer          default(0)
#

class UsersController < ApplicationController
	layout "manager"
  	before_filter :authenticate_user!
  	before_filter :assign_user, only: [:show, :edit, :update, :destroy]


	def index
			@users = User.all
	end

	def show
	end


	def edit
	end


	def update
    	if @user.update(user_params)
    		# sign_in @user, :bypass => true
	      	redirect_to user_path(@user)
	    else
	      render "edit"
    	end
	end


	def destroy
		if @user.invalid != "1"
			@user.update_attributes(invalid: "1")
	      	redirect_to users_path, notice: "User deactivated"
	     else
			@user.update_attributes(invalid: "0")
	      	redirect_to users_path, notice: "User activated"
	      end
	end

	def resend_invite
	  @user = User.find(params[:user_id])
	  @user.invite!
	  respond_to do |fmt|
	  	fmt.html{redirect_to users_path, :notice => "Invitation resent."}
	  end
	end


  private

  def user_params
    params.required(:user).permit(:name, :email, :user_image)
  end

  def assign_user
  	@user = User.find(params[:id])
  end

  def authorize_user!

	end


  def authorize_admin!

  end



  end

