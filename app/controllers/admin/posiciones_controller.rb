class Admin::PosicionesController < AdminController
  before_action -> { @sidebar_menu_item = :posiciones }

  def index
    if params[:month].present? and params[:year].present?
      periodo = "#{params[:month]}-#{params[:year]}"
    else
      periodo = Date.today.strftime("%m-%Y")
    end

    @posiciones = Posicion.in_period(periodo)

    if params[:query].present?
      @posiciones = @posiciones.with_text(params[:query])
    end

    @posiciones = @posiciones.paginate(page: params[:page])
  end

  def show
    @posicion = Posicion.find(params[:id])
  end

  def new
    @empleado = Empleado.find_by!(rut: params[:rut])
    @posicion = @empleado.posiciones.build
  end

  def create
    @empleado = Empleado.find_by!(rut: params[:posicion][:rut])
    @posicion = @empleado.posiciones.build(posicion_params)

    if @posicion.save
      flash[:success] = "Entrada creada exitosamente."
      redirect_to admin_posiciones_path+"?month="+@posicion.periodo[0..1]+"&year="+@posicion.periodo[3..7]
    else
      flash[:danger] = "Por favor corrija los errores indicados antes de continuar."
      render 'new'
    end
  end

  def edit
    @posicion = Posicion.find(params[:id])
  end

  def update
    @posicion = Posicion.find(params[:id])
    if @posicion.update_attributes(posicion_params)
      flash[:success] = "Entrada actualizada exitosamente."
      redirect_to admin_posiciones_path+"?month="+@posicion.periodo[0..1]+"&year="+@posicion.periodo[3..7]
    else
      flash[:danger] = "Por favor corrija los errores indicados antes de continuar."
      render 'edit'

    end

  end

  def destroy
    Posicion.find(params[:id]).destroy

    flash[:success] = "Entrada eliminada exitosamente."

    redirect_to admin_posiciones_path
  end

  private

  def posicion_params
    params.require(:posicion).permit(:piramide, :tipo_contrato, :glosa_contrato, :division, :gerencia, :periodo, :empleado_id, :rut, :es_plantilla, :nombre_empresa, :tipo_renta, :cargo, :estado_civil, :num_hijos, :area, :subarea, :saldo_vacaciones, :periodos_pendientes_vacaciones)
  end
end
