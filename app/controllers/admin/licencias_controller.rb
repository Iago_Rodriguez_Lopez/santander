class Admin::LicenciasController < AdminController
  before_action -> { @sidebar_menu_item = :licencias }

  def index
    if params[:month].present? and params[:year].present?
      periodo = "#{params[:month]}-#{params[:year]}"
    else
      periodo = Date.today.strftime("%m-%Y")
    end

    @licencias = Licencia.in_period(periodo)

    if params[:query].present?
      @licencias = @licencias.with_text(params[:query])
    end

    @licencias = @licencias.paginate(page: params[:page]).order(fecha_inicio: :asc)
  end

  def show
    @licencia = Licencia.find(params[:id])
  end

  def new
    @empleado = Empleado.find_by!(rut: params[:rut])
    @licencia = @empleado.licencias.build
  end

  def create
    @empleado = Empleado.find_by!(rut: params[:licencia][:rut])
    @licencia = @empleado.licencias.build(licencia_params)

    if @licencia.save
      flash[:success] = "Entrada creada exitosamente."

      redirect_to admin_licencias_path
    else
      flash[:danger] = "Por favor corrija los errores indicados antes de continuar."

      render 'new'
    end
  end

  def edit
    @licencia = Licencia.find(params[:id])
  end

  def update
    @licencia = Licencia.find(params[:id])

    if @licencia.update_attributes(licencia_params)
      flash[:success] = "Entrada actualizada exitosamente."
    else
      flash[:danger] = "Por favor corrija los errores indicados antes de continuar."
    end

    render 'edit'
  end

  def destroy
    Licencia.find(params[:id]).destroy

    flash[:success] = "Entrada eliminada exitosamente."

    redirect_to admin_licencias_path
  end

  private

  def licencia_params
    params.require(:licencia).permit(:glosa_tipo_licencia, :dias_totales, :dias_corridos, :dias_habiles, :rut, :periodo_anualizado,:periodo)
  end
end
