class Admin::EgresosController < AdminController
  before_action -> { @sidebar_menu_item = :egresos }

  def index
    if params[:month].present? and params[:year].present?
      periodo = "#{params[:month]}-#{params[:year]}"
    else
      periodo = Date.today.strftime("%m-%Y")
    end

    @egresos = Egreso.in_period(periodo)

    if params[:query].present?
      @egresos = @egresos.with_text(params[:query])
    end

    @egresos = @egresos.paginate(page: params[:page])
  end

  def show
    @egreso = Egreso.find(params[:id])
  end

  def new
    @empleado = Empleado.find_by!(rut: params[:rut])
    @egreso = @empleado.egresos.build
  end

  def create
    @empleado = Empleado.find_by!(rut: params[:egreso][:rut])
    @egreso = @empleado.egresos.build(egreso_params)

    if @egreso.save
      flash[:success] = "Entrada creada exitosamente."

      redirect_to admin_egresos_path
    else
      flash[:danger] = "Por favor corrija los errores indicados antes de continuar."

      render 'new'
    end
  end

  def edit
    @egreso = Egreso.find(params[:id])
  end

  def update
    @egreso = Egreso.find(params[:id])

    if @egreso.update_attributes(egreso_params)
      flash[:success] = "Entrada actualizada exitosamente."
    else
      flash[:danger] = "Por favor corrija los errores indicados antes de continuar."
    end

    render 'edit'
  end

  def destroy
    Egreso.find(params[:id]).destroy

    flash[:success] = "Entrada eliminada exitosamente."

    redirect_to admin_egresos_path
  end

  private

  def egreso_params
    params.require(:egreso).permit(:piramide, :tipo_contrato, :division, :gerencia, :periodo, :empleado_id, :rut, :periodo_anualizado, :tipo_egreso, :tipo_renta, :cargo, :estado_civil, :num_hijos)
  end
end
