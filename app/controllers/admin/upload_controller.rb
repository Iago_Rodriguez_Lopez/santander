class Admin::UploadController < AdminController
  before_action -> { @sidebar_menu_item = params[:model_type] ? params[:model_type].to_sym : "empleados" }

  def create
    file = params[:upload][:file]
    date = Date.new(params[:upload]["periodo(1i)"].to_i, params[:upload]["periodo(2i)"].to_i, params[:upload]["periodo(3i)"].to_i).strftime("%m-%Y")

    # Bajas en realidad deben entrar al modelo empleado
    if params[:model_type] == "bajas"
      result = Empleado.csv_import_bajas(file, date)
    else
      model = params[:model_type].singularize.classify.safe_constantize
      result = model.csv_import(file, date)
    end
    # Analizamos el resultado para entregar el mensaje obtenido.
    if (result.is_a? Enumerable and result.empty?) or result == true
      flash[:success] = "Recibido y procesado correctamente."
    elsif result.is_a? Enumerable and !result.empty?
      flash[:success] = "Recibido y procesado correctamente."
      flash[:warning] = "<strong>Importante:</strong> No se han procesado los siguientes RUTs: "+ result.compact.to_s
    elsif result == 0
      flash[:danger] = "Error al cargar archivo. Posible error de codificación o cabeceras. Revise la documentación de ayuda."
    else
      flash[:danger] = "Error al cargar archivo. El error se encuentra alrededor de la línea #{result.to_s}. Revise el formato de los datos."
    end

    redirect_to :back
  end

  def remove
    date = params[:periodo]
    model = params[:model_type].classify.safe_constantize

    if model.where("periodo = ?", date).delete_all
      flash[:success] = "Eliminados correctamente."
    else
      flash[:danger] = "Error al eliminar."
    end

    redirect_to :back
  end

  def generate
    date = params[:periodo]
    Vacante.fill_vacantes(date)
    flash[:success] = "Vacantes rellenadas exitosamente."
    redirect_to :back
  end
end
