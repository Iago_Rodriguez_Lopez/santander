class Admin::UserLogsController < AdminController
    before_action -> { @sidebar_menu_item = :user_logs }
    
    def index
        @user_logs = UserLog.paginate(:page => params[:page], :per_page =>10).order(created_at: :desc)

    end
    
end
