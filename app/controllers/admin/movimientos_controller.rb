class Admin::MovimientosController < AdminController
  before_action -> { @sidebar_menu_item = :movimientos }

  def index
    if params[:month].present? and params[:year].present?
      periodo = "#{params[:month]}-#{params[:year]}"
    else
      periodo = Date.today.strftime("%m-%Y")
    end

    @movimientos = Movimiento.in_period(periodo)

    if params[:query].present?
      @movimientos = @movimientos.with_text(params[:query])
    end

    @movimientos = @movimientos.paginate(page: params[:page])
  end

  def show
    @movimiento = Movimiento.find(params[:id])
  end

  def new
    @empleado = Empleado.find_by!(rut: params[:rut])
    @movimiento = @empleado.movimientos.build
  end

  def create
    @empleado = Empleado.find_by!(rut: params[:movimiento][:rut])
    @movimiento = @empleado.movimientos.build(movimiento_params)

    if @movimiento.save
      flash[:success] = "Entrada creada exitosamente."

      redirect_to admin_movimientos_path
    else
      flash[:danger] = "Por favor corrija los errores indicados antes de continuar."

      render 'new'
    end
  end

  def edit
    @movimiento = Movimiento.find(params[:id])
  end

  def update
    @movimiento = Movimiento.find(params[:id])

    if @movimiento.update_attributes(movimiento_params)
      flash[:success] = "Entrada actualizada exitosamente."
    else
      flash[:danger] = "Por favor corrija los errores indicados antes de continuar."
    end

    render 'edit'
  end

  def destroy
    Movimiento.find(params[:id]).destroy

    flash[:success] = "Entrada eliminada exitosamente."

    redirect_to admin_movimientos_path
  end

  private

  def movimiento_params
    params.require(:movimiento).permit(:tipo_movimiento, :periodo, :rut, :periodo_anualizado)
  end
end
