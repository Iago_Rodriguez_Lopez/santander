class Admin::VacacionesController < AdminController
  before_action -> { @sidebar_menu_item = :vacaciones }

  def index
    if params[:month].present? and params[:year].present?
      periodo = "#{params[:month]}-#{params[:year]}"
    else
      periodo = Date.today.strftime("%m-%Y")
    end

    @vacaciones = Vacacion.in_period(periodo)

    if params[:query].present?
      @vacaciones = @vacaciones.with_text(params[:query])
    end

    @vacaciones = @vacaciones.paginate(page: params[:page])
  end

  def show
    @vacacion = Vacacion.find(params[:id])
  end

  def new
    @empleado = Empleado.find_by!(rut: params[:rut])
    @vacacion = @empleado.vacaciones.build
  end

  def create
    @empleado = Empleado.find_by!(rut: params[:vacacion][:rut])
    @vacacion = @empleado.vacaciones.build(vacacion_params)

    if @vacacion.save
      flash[:success] = "Entrada creada exitosamente."

      redirect_to admin_vacaciones_path
    else
      flash[:danger] = "Por favor corrija los errores indicados antes de continuar."

      render 'new'
    end
  end

  def edit
    @vacacion = Vacacion.find(params[:id])
  end

  def update
    @vacacion = Vacacion.find(params[:id])

    if @vacacion.update_attributes(vacacion_params)
      flash[:success] = "Entrada actualizada exitosamente."
    else
      flash[:danger] = "Por favor corrija los errores indicados antes de continuar."
    end

    render 'edit'
  end

  def destroy
    Vacacion.find(params[:id]).destroy

    flash[:success] = "Entrada eliminada exitosamente."

    redirect_to admin_vacaciones_path
  end

  private

  def vacacion_params
    params.require(:vacacion).permit(:periodo, :periodo_anualizado,  :rut, :saldo, :conseguidos, :consumidos, :periodos_pendientes, :dias_10)
  end
end
