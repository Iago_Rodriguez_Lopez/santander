class Admin::RetribucionesController < AdminController
  before_action -> { @sidebar_menu_item = :retribuciones }

  def index
    if params[:month].present? and params[:year].present?
      periodo = "#{params[:month]}-#{params[:year]}"
    else
      periodo = Date.today.strftime("%m-%Y")
    end

    @retribuciones = Retribucion.in_period(periodo)

    if params[:query].present?
      @retribuciones = @retribuciones.with_text(params[:query])
    end

    @retribuciones = @retribuciones.paginate(page: params[:page])
  end

  def show
    @retribucion = Retribucion.find(params[:id])
  end

  def new
    @empleado = Empleado.find_by!(rut: params[:rut])
    @retribucion = @empleado.retribuciones.build
  end

  def create
    @empleado = Empleado.find_by!(rut: params[:retribucion][:rut])
    @retribucion = @empleado.retribuciones.build(retribucion_params)

    if @retribucion.save
      flash[:success] = "Entrada creada exitosamente."

      redirect_to admin_retribuciones_path
    else
      flash[:danger] = "Por favor corrija los errores indicados antes de continuar."

      render 'new'
    end
  end

  def edit
    @retribucion = Retribucion.find(params[:id])
  end

  def update
    @retribucion = Retribucion.find(params[:id])

    if @retribucion.update_attributes(retribucion_params)
      flash[:success] = "Entrada actualizada exitosamente."
    else
      flash[:danger] = "Por favor corrija los errores indicados antes de continuar."
    end

    render 'edit'
  end

  def destroy
    Retribucion.find(params[:id]).destroy

    flash[:success] = "Entrada eliminada exitosamente."

    redirect_to admin_retribuciones_path
  end

  private

  def retribucion_params
    params.require(:retribucion).permit(:periodo, :periodo_anualizado, :porcentaje, :anual, :resto_año, :rut, :tipo_movimiento)
  end
end
