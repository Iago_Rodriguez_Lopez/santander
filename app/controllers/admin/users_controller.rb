class Admin::UsersController < AdminController
  before_action -> { @sidebar_menu_item = :usuarios }

  def index
    @users = User.paginate(page: params[:page]).order(created_at: :asc)
  end

  def show
  end

  def new
    @user = User.new
  end

  def log
    @user = User.find(params[:id])
    @user_logs = @user.user_logs.paginate(:page => params[:page], :per_page =>10).order(created_at: :desc)
  end

  def create
    @user = User.new(usuario_create_params)
    if @user.save
      redirect_to admin_users_path
    else
      # flash[:danger] = "Por favor corrija los errores indicados antes de continuar."
      render 'new'
    end
    # flash[:success] = "Usuario <strong>#{@user.nombre_completo}</strong> creado exitosamente. Este puede confirmar su cuenta en la siguiente dirección: <p><a href='#{accept_user_invitation_url(:invitation_token => @user.raw_invitation_token)}' class='alert-link'>#{accept_user_invitation_url(:invitation_token => @user.raw_invitation_token)}</a></p>"

  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])

    if @user.update_attributes(usuario_update_params)
      flash[:success] = "Usuario <strong>#{@user.nombre_completo}</strong> actualizado exitosamente."
    end

    redirect_to admin_users_path

  end

  def destroy
    User.find(params[:id]).destroy

    flash[:success] = "Usuario eliminado exitosamente."

    redirect_to admin_users_path
  end

  private

  def usuario_update_params
    result = params.require(:usuario).permit(:email, :password, :view_gerentes, :rol, :rut, :password_confirmation, accesos: [:division, :gerencia], metadata: [:nombres, :apellidos])

    # Let's parse accesos structure
    result[:accesos] = result[:accesos].map do |item|
      if item[:division].blank?
        ":"
      else
        "#{item[:division]}:*"
      end
    end.uniq

    result[:accesos].delete(":")

    return result.reject{|_, v| v.blank?}
  end

  def usuario_create_params
    result = params.require(:usuario).permit(:email, :rol, :view_gerentes, :metadata, :rut, :password, accesos: [:division, :gerencia]).tap do |whitelisted|
      whitelisted[:metadata] =  params[:usuario][:metadata]
    end

    # Let's parse accesos structure
    result[:accesos] = result[:accesos].map do |item|
      if item[:division].blank?
        ":"
      else
        "#{item[:division]}:*"
      end
    end.uniq

    result[:accesos].delete(":")

    return result.reject{|_, v| v.blank?}
  end
end
