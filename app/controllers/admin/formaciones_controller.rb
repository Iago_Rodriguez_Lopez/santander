class Admin::FormacionesController < AdminController
  before_action -> { @sidebar_menu_item = :formaciones }

  def index
    if params[:month].present? and params[:year].present?
      periodo = "#{params[:month]}-#{params[:year]}"
    else
      periodo = Date.today.strftime("%m-%Y")
    end

    @formaciones = Formacion.in_period(periodo)

    if params[:query].present?
      @formaciones = @formaciones.with_text(params[:query])
    end

    @formaciones = @formaciones.paginate(page: params[:page])
  end

  def show
    @formacion = Formacion.find(params[:id])
  end

  def new
    @empleado = Empleado.find_by!(rut: params[:rut])
    @formacion = @empleado.formaciones.build
  end

  def create
    @empleado = Empleado.find_by!(rut: params[:formacion][:rut])
    @formacion = @empleado.formaciones.build(formacion_params)

    if @formacion.save
      flash[:success] = "Entrada creada exitosamente."

      redirect_to admin_formaciones_path
    else
      flash[:danger] = "Por favor corrija los errores indicados antes de continuar."

      render 'new'
    end
  end

  def edit
    @formacion = Formacion.find(params[:id])
  end

  def update
    @formacion = Formacion.find(params[:id])

    if @formacion.update_attributes(formacion_params)
      flash[:success] = "Entrada actualizada exitosamente."
    else
      flash[:danger] = "Por favor corrija los errores indicados antes de continuar."
    end

    render 'edit'
  end

  def destroy
    Formacion.find(params[:id]).destroy

    flash[:success] = "Entrada eliminada exitosamente."

    redirect_to admin_formaciones_path
  end

  private

  def formacion_params
    params.require(:formacion).permit(:materia, :nombre_del_curso, :fecha_inicio, :fecha_fin, :horas, :periodo, :modalidad, :rut, :periodo_anualizado, :asistencia)
  end
end
