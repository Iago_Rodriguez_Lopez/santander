class Admin::EmpleadosController < AdminController
  before_action -> { @sidebar_menu_item = :empleados }

  def index
    @empleados = Empleado.all

    if params[:query].present?
      @empleados = @empleados.with_text(params[:query])
    end

    @empleados = @empleados.paginate(page: params[:page]).order(apellidos: :asc)
  end

  def show
    @empleado = Empleado.find(params[:id])
  end

  def new
    @empleado = Empleado.new
  end

  def create
    @empleado = Empleado.new(empleado_params)

    if @empleado.save
      flash[:success] = "Empleado #{@empleado.nombre} #{@empleado.apellidos} ingresado exitosamente."
      redirect_to admin_empleados_path
    else
      flash[:danger] = "Por favor corrija los errores antes de continuar."
      render 'new'
    end
  end

  def edit
    @empleado = Empleado.find(params[:id])
  end

  def update
    @empleado = Empleado.find(params[:id])

    if @empleado.update_attributes(empleado_params)
      flash[:success] = "Empleado #{@empleado.nombre} #{@empleado.apellidos} editado exitosamente."
      redirect_to admin_empleados_path
    else
      flash[:danger] = "Por favor corrija los errores antes de continuar."
      render 'new'
    end
  end

  def destroy
    Empleado.find(params[:id]).destroy

    flash[:success] = "Empleado eliminado extiosamente"

    return_to :back
  end

  private

  def empleado_params
    p = params.require(:empleado).permit(:rut, :nombre, :apellidos, :sexo, :fecha_ingreso_grupo, :fecha_nacimiento, :fecha_renuncia, :tipo_renuncia, :motivo_renuncia)

    p[:rut] = p[:rut].rjust(11, "0")
    p[:rut] = p[:rut][0..-2]+"-"+p[:rut][-1]

    return p
  end
end
