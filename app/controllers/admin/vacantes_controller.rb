class Admin::VacantesController < AdminController
  before_action -> { @sidebar_menu_item = :vacantes }

  def index
    if params[:month].present? and params[:year].present?
      periodo = "#{params[:month]}-#{params[:year]}"
    else
      periodo = Date.today.strftime("%m-%Y")
    end

    @vacantes = Vacante.in_period(periodo)

    if params[:query].present?
      @vacantes = @vacantes.with_text(params[:query])
    end

    @vacantes = @vacantes.paginate(page: params[:page])
  end

  def show
    @vacante = Vacante.find(params[:id])
  end

  def new
    @empleado = Empleado.find_by!(rut: params[:rut])
    @vacante = @empleado.vacantes.build
  end

  def create
    @empleado = Empleado.find_by!(rut: params[:rut])
    @vacante = @empleado.vacantes.build(vacante_params)

    if @vacante.save
      flash[:success] = "Entrada creada exitosamente."

      redirect_to admin_vacantes_path
    else
      flash[:danger] = "Por favor corrija los errores indicados antes de continuar."

      render 'new'
    end
  end

  def edit
    @vacante = Vacante.find(params[:id])
  end

  def update
    @vacante = Vacante.find(params[:id])

    if @vacante.update_attributes(vacante_params)
      flash[:success] = "Entrada actualizada exitosamente."
    else
      flash[:danger] = "Por favor corrija los errores indicados antes de continuar."
    end

    render 'edit'
  end

  def destroy
    Vacante.find(params[:id]).destroy
    flash[:success] = "Entrada eliminada exitosamente."
    redirect_to admin_vacantes_path
  end



  def crear_orden_gerencias
  end


  def crear_orden_divisiones
    Vacante.where(division: params[:vacante][:division]).update_all(division_order: params[:vacante][:division_order])
  end

  private

  def vacante_params
    params.require(:vacante).permit(:division, :gerencia, :periodo, :tipo_area, :activo)
  end
end
