class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  #before_action :configure_permitted_parameters, if: :devise_controller?

  # protect_from_forgery 
  before_action :divisiones_asignadas
  #before_action :configure_permitted_parameters, if: :devise_controller?

  def isWebRequest?
    request.path[0..4] != "/api/"
  end


  def index
    if current_user == nil
      redirect_to root_path
    else
      render nothing: true
    end
  end


  def divisiones_asignadas
    if current_user.nil? == false
      if isWebRequest?
        if params[:periodo] == nil
          @periodo = (Date.today - 1.month).strftime("%m-%Y")
        else
          @periodo = params[:periodo]
        end

        if current_user.role == "Divisional"
          @divisiones = Vacante.active.in_user(current_user).in_period(@periodo).pluck(:division).uniq
        else
          @divisiones = Vacante.active.in_period(@periodo).pluck(:division).uniq
        end
      end
    end
    # @periodo = (Date.today - 1.month).strftime("%m-%Y")
    # @divisiones = []

  end

  def not_authorized
    head 401
  end

    protected

  def configure_permitted_parameters
  #   devise_parameter_sanitizer.for(:sign_up) << :rut
  #   devise_parameter_sanitizer.for(:invite) << :name
  end
end
