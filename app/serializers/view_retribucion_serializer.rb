# == Schema Information
#
# Table name: retribucion_view
#
#  id                  :integer
#  rut                 :string
#  sexo                :string
#  fecha_ingreso_grupo :date
#  fecha_nacimiento    :date
#  fecha_renuncia      :date
#  tipo_renuncia       :string
#  nombre              :string
#  apellidos           :string
#  division            :string
#  gerencia            :string
#  periodo             :string
#  piramide            :string
#  tipo_contrato       :string
#  nombre_empresa      :string
#  es_plantilla        :boolean
#  tipo_renta          :string
#  cargo               :string
#  estado_civil        :string
#  num_hijos           :integer
#  periodo_retribucion :string
#  tipo_movimiento     :string
#  porcentaje          :decimal(2, 2)
#  retribucion_anual   :decimal(10, 2)
#

require 'jsonapi-serializers'

class ViewRetribucionSerializer
  include JSONAPI::Serializer

  attributes :id, :rut, :sexo, :fecha_ingreso_grupo, :fecha_nacimiento, :division,
  :gerencia, :periodo, :piramide, :tipo_contrato,
  :tipo_area, :nombre_programa, :estado_programa, :nombre_empresa,
  :periodo_retribucion, :tipo_movimiento, :retribucion_anual, :fecha_nacimiento, :fecha_ingreso_grupo,
  :nombre, :apellidos
  def fecha_nacimiento
    object.fecha_nacimiento.strftime("%Y-%m-%d")
  end
  def fecha_ingreso_grupo
    object.fecha_ingreso_grupo.strftime("%Y-%m-%d")
  end
end
