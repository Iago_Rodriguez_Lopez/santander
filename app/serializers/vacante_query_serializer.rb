class VacanteQuerySerializer < ActiveModel::Serializer
  attributes :id, :division, :gerencia, :vacantes, :empleados, :tipo_area, :activo, :periodo
end
