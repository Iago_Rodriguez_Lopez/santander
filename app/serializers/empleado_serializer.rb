# == Schema Information
#
# Table name: empleados
#
#  id                  :integer          not null, primary key
#  rut                 :string
#  sexo                :string
#  fecha_ingreso_grupo :date
#  fecha_nacimiento    :date
#  fecha_renuncia      :date
#  tipo_renuncia       :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  motivo_renuncia     :string
#  nombre              :string
#  apellidos           :string
#

require 'jsonapi-serializers'

class EmpleadoSerializer < BaseSerializer
  include JSONAPI::Serializer

  attribute :rut
	attribute :sexo
  attribute :fecha_ingreso_grupo
  attribute :fecha_nacimiento
  attribute :fecha_renuncia
  attribute :tipo_renuncia
  attribute :motivo_renuncia
  attribute :nombre
  attribute :apellidos

end
