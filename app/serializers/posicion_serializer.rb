# == Schema Information
#
# Table name: posiciones
#
#  id             :integer          not null, primary key
#  piramide       :string
#  tipo_contrato  :string
#  glosa_contrato :string
#  division       :string
#  gerencia       :string
#  area           :string
#  subarea        :string
#  periodo        :string
#  empleado_id    :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  rut            :string
#  nombre_empresa :string
#  es_plantilla   :boolean          default(TRUE)
#  tipo_renta     :string
#  cargo          :string
#  estado_civil   :string
#  num_hijos      :integer
#

require 'jsonapi-serializers'

class PosicionSerializer < BaseSerializer
  include JSONAPI::Serializer

  attributes :piramide, :tipo_contrato, :glosa_contrato, :division, :gerencia, :periodo, :empleado_id, :rut, :es_plantilla, :nombre_empresa, :tipo_renta, :cargo, :estado_civil, :num_hijos#, :area, :subarea

  has_one :empleado


end
