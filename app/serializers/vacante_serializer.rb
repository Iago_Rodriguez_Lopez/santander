# == Schema Information
#
# Table name: vacantes
#
#  id             :integer          not null, primary key
#  division       :string
#  gerencia       :string
#  vacantes       :integer
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#  empleados      :integer
#  tipo_area      :string
#  activo         :boolean
#  periodo        :string
#  division_order :integer
#  gerencia_order :integer
#

# require 'jsonapi-serializers'

# class VacanteSerializer < BaseSerializer
#   include JSONAPI::Serializer

#   attributes  :division, :gerencia, :vacantes, :empleados, :tipo_area, :activo, :periodo#, :rut, :area, :subarea


# end


class VacanteSerializer < ActiveModel::Serializer
  attributes :id, :division, :gerencia, :vacantes, :empleados, :tipo_area, :activo, :periodo
end
