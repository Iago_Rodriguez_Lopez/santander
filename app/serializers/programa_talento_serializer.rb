# == Schema Information
#
# Table name: programa_talentos
#
#  id          :integer          not null, primary key
#  empleado_id :integer
#  periodo     :string
#  nombre      :string
#  estado      :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  rut         :string
#

require 'jsonapi-serializers'

class ProgramaTalentoSerializer < BaseSerializer
  include JSONAPI::Serializer

  attributes :empleado_id, :periodo, :nombre, :estado, :rut

  has_one :empleado

end
