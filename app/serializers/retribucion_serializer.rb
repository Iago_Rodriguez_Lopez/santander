# == Schema Information
#
# Table name: retribuciones
#
#  id                 :integer          not null, primary key
#  empleado_id        :integer
#  periodo            :string
#  periodo_anualizado :string
#  tipo_movimiento    :string
#  resto_año          :decimal(10, 2)
#  anual              :decimal(10, 2)
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  rut                :string
#  porcentaje         :decimal(2, 2)
#

require 'jsonapi-serializers'

class RetribucionSerializer < BaseSerializer
  include JSONAPI::Serializer

  attributes  :periodo, :periodo_anualizado, :tipo_movimiento, :resto_año, :anual, :empleado_id, :rut#, :area, :subarea
  has_one :empleado


end
