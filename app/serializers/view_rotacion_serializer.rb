# == Schema Information
#
# Table name: egreso_view
#
#  id                  :integer
#  rut                 :string
#  sexo                :string
#  fecha_ingreso_grupo :date
#  fecha_nacimiento    :date
#  nombre              :string
#  apellidos           :string
#  division            :string
#  gerencia            :string
#  periodo             :string
#  periodo_egreso      :string
#  piramide            :string
#  tipo_contrato       :string
#  es_plantilla        :boolean
#  tipo_renta          :string
#  cargo               :string
#  estado_civil        :string
#  num_hijos           :integer
#  tipo_egreso         :string
#

require 'jsonapi-serializers'

class ViewRotacionSerializer
  include JSONAPI::Serializer

  attributes :id, :rut, :sexo, :fecha_ingreso_grupo, :fecha_nacimiento, :division,
  :gerencia, :periodo_egreso, :piramide, :tipo_contrato,
  :tipo_area, :tipo_egreso, :nombre, :apellidos


  def fecha_nacimiento
    object.fecha_nacimiento.strftime("%Y-%m-%d")
  end
  def fecha_ingreso_grupo
    object.fecha_ingreso_grupo.strftime("%Y-%m-%d")
  end
end
