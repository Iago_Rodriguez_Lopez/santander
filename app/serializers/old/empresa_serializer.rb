# == Schema Information
#
# Table name: empresas
#
#  id          :integer          not null, primary key
#  empleado_id :integer
#  periodo     :string
#  nombre      :string
#  rut         :string
#  activo      :boolean
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#



require 'jsonapi-serializers'

class EmpresaSerializer < BaseSerializer
  include JSONAPI::Serializer

  attributes  :nombre, :periodo, :empleado_id, :rut

  has_one :empleado

end
