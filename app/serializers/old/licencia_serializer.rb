# == Schema Information
#
# Table name: licencias
#
#  id                  :integer          not null, primary key
#  fecha               :date
#  glosa_tipo_licencia :string
#  empleado_id         :integer
#  fecha_inicio        :date
#  fecha_termino       :date
#  dias_totales        :integer
#  dias_corridos       :integer
#  dias_habiles        :integer
#  periodo             :string
#  created_at          :datetime         not null
#  updated_at          :datetime         not null
#  rut                 :string
#  periodo_anualizado  :string
#  tipo_licencia       :string
#

require 'jsonapi-serializers'

class LicenciaSerializer < BaseSerializer
  include JSONAPI::Serializer

  attributes  :glosa_tipo_licencia, :periodo, :rut, :dias_totales, :dias_corridos, :dias_habiles, :periodo_anualizado#, :area, :subarea

  has_one :empleado

end
