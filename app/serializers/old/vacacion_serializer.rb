# == Schema Information
#
# Table name: vacaciones
#
#  id          :integer          not null, primary key
#  conseguidos :decimal(10, 2)
#  consumidos  :decimal(10, 2)
#  saldo       :decimal(10, 2)
#  empleado_id :integer
#  periodo     :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  rut         :string
#

require 'jsonapi-serializers'

class VacacionSerializer < BaseSerializer
  include JSONAPI::Serializer

  attribute :conseguidos
  attribute :consumidos
  attribute :saldo
  attribute :periodo
  attribute :rut

  has_one   :empleado
end
