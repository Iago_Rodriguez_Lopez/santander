# == Schema Information
#
# Table name: prezos
#
#  id          :integer          not null, primary key
#  empleado_id :integer
#  periodo     :string
#  pre         :decimal(10, 2)
#  zona        :string
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  rut         :string
#

require 'jsonapi-serializers'

class PrezoSerializer < BaseSerializer
  include JSONAPI::Serializer

  attributes :empleado_id, :periodo, :zona, :pre, :periodo, :rut

  has_one :empleado

end
