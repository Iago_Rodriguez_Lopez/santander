class BaseSerializer
  include JSONAPI::Serializer

  def self_link
    "/dashboard/api/v1#{super}"
  end
end
