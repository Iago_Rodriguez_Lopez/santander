# == Schema Information
#
# Table name: formacion_view
#
#  id                  :integer
#  rut                 :string
#  sexo                :string
#  fecha_ingreso_grupo :date
#  fecha_nacimiento    :date
#  fecha_renuncia      :date
#  tipo_renuncia       :string
#  nombre              :string
#  apellidos           :string
#  division            :string
#  gerencia            :string
#  periodo             :string
#  piramide            :string
#  tipo_contrato       :string
#  nombre_empresa      :string
#  tipo_renta          :string
#  cargo               :string
#  estado_civil        :string
#  num_hijos           :integer
#  periodo_formacion   :string
#  materia             :string
#  horas               :integer
#  nombre_del_curso    :string
#  modalidad           :string
#  asistencia          :string
#  es_plantilla        :boolean
#

require 'jsonapi-serializers'

class ViewFormacionSerializer
  include JSONAPI::Serializer

  attributes :rut, :sexo, :fecha_ingreso_grupo, :fecha_nacimiento,
  :division, :gerencia, :periodo, :piramide,
  :tipo_contrato, :tipo_area,
  :estado_programa, :nombre_empresa,
  :periodo_formacion, :materia, :horas,
  #:nombre_programa, :asistencia, :modalidad, :nombre_del_curso,
  :fecha_nacimiento, :fecha_ingreso_grupo,
  :nombre, :apellidos
  def fecha_nacimiento
    object.fecha_nacimiento.strftime("%Y-%m-%d")
  end
  def fecha_ingreso_grupo
    object.fecha_ingreso_grupo.strftime("%Y-%m-%d")
  end

end
