# == Schema Information
#
# Table name: egresos
#
#  id                 :integer          not null, primary key
#  empleado_id        :integer
#  periodo            :string
#  periodo_anualizado :string
#  tipo_egreso        :string
#  rut                :string
#  division           :string
#  gerencia           :string
#  piramide           :string
#  tipo_contrato      :string
#  es_plantilla       :boolean          default(TRUE)
#  tipo_renta         :string
#  cargo              :string
#  estado_civil       :string
#  num_hijos          :integer
#

require 'jsonapi-serializers'

class EgresoSerializer < BaseSerializer
  include JSONAPI::Serializer

  attributes :piramide, :tipo_contrato, :division, :gerencia, :periodo, :empleado_id, :rut, :periodo_anualizado, :tipo_egreso, :tipo_renta, :cargo, :estado_civil, :num_hijos

  has_one :empleado


end
