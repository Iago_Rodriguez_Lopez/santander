# == Schema Information
#
# Table name: formaciones
#
#  id                 :integer          not null, primary key
#  materia            :string
#  nombre_del_curso   :string
#  fecha_inicio       :date
#  fecha_fin          :date
#  horas              :integer
#  periodo            :string
#  modalidad          :string
#  empleado_id        :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  rut                :string
#  periodo_anualizado :string
#  asistencia         :string
#

require 'jsonapi-serializers'

class FormacionSerializer < BaseSerializer
  include JSONAPI::Serializer

  attribute :materia
  attribute :nombre_del_curso
  #attribute :fecha_inicio   ## LAS DOS FECHAS DAN PROBLEMAS
  #attribute :fecha_fin
  attribute :horas
  attribute :periodo
  attribute :modalidad
  attribute :periodo_anualizado
  attribute :asistencia
  attribute :rut
  has_one   :empleado
end
