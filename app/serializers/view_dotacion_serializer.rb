# == Schema Information
#
# Table name: dotacion_view
#
#  id                  :integer
#  rut                 :string
#  nombre              :string
#  apellidos           :string
#  sexo                :string
#  fecha_ingreso_grupo :date
#  fecha_nacimiento    :date
#  fecha_renuncia      :date
#  tipo_renuncia       :string
#  division            :string
#  gerencia            :string
#  periodo             :string
#  piramide            :string
#  tipo_contrato       :string
#  nombre_empresa      :string
#  tipo_renta          :string
#  cargo               :string
#  estado_civil        :string
#  num_hijos           :integer
#  tipo_area           :string
#  es_plantilla        :boolean
#

require 'postgres_ext/serializers'

class ViewDotacionSerializer
  include JSONAPI::Serializer

  attributes :rut, :nombre, :apellidos, :sexo, :fecha_ingreso_grupo, :fecha_nacimiento, :division, :gerencia, :periodo, :piramide, :tipo_contrato, :tipo_area, :nombre_empresa

  def fecha_nacimiento
    object.fecha_nacimiento.strftime("%Y-%m-%d")
  end
  def fecha_ingreso_grupo
    object.fecha_ingreso_grupo.strftime("%Y-%m-%d")
  end
end
