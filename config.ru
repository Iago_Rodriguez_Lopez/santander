# This file is used by Rack-based servers to start the application.

require ::File.expand_path('../config/environment',  __FILE__)


if Rails.env.production?

Rails.application.config.relative_url_root = '/dashboard'

map Rails.application.config.relative_url_root || "/dashboard" do
  run Rails.application
end

else

require ::File.expand_path('../config/environment',  __FILE__)
run Rails.application

end
