# Be sure to restart your server when you modify this file.

# Add new inflection rules using the following format. Inflections
# are locale specific, and you may define rules for as many different
# locales as you wish. All of these examples are active by default:
ActiveSupport::Inflector.inflections(:en) do |inflect|
  inflect.irregular 'posicion', 'posiciones'
  inflect.irregular 'vacacion', 'vacaciones'
  inflect.irregular 'formacion', 'formaciones'
  inflect.irregular 'licencia', 'licencias'
  inflect.irregular 'programa_talento', 'programa_talentos'
  inflect.irregular 'retribucion', 'retribuciones'

end


# These inflection rules are supported but not enabled by default:
# ActiveSupport::Inflector.inflections(:en) do |inflect|
#   inflect.acronym 'RESTful'
# end
