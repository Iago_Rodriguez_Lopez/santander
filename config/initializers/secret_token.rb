# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Santander::Application.config.secret_key_base = 'ae9f43defa16ebb0c3312991ad7ae9d7fc4b2313884105851491fddcd95139153e8a80e8d2bb696d46d74685f692e945780f74a810820ae6b56a69af2b5d8260'
