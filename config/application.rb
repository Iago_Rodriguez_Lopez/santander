require File.expand_path('../boot', __FILE__)

require 'csv'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Santander
  class Application < Rails::Application
        config.middleware.use Rack::Deflater

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.
    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    config.i18n.default_locale = :es

    # config.to_prepare do
    #     Devise::SessionsController.layout "manager_simplified"
    #     Devise::RegistrationsController.layout "manager_simplified"
    #     Devise::ConfirmationsController.layout "manager_simplified"
    #     Devise::UnlocksController.layout "manager_simplified"
    #     Devise::PasswordsController.layout "manager_simplified"
    #     Devise::InvitationsController.layout "manager"
    # end

   
    config.assets.prefix = "/dashboard/assets/"
    

    config.active_job.queue_adapter = :delayed_job
    # config.browserify_rails.commandline_options = "-t [ babelify --presets [ es2015 react ] ]"
    ActionView::Base.field_error_proc = Proc.new do |html_tag, instance|
      html = %(<div class="field_with_errors">#{html_tag}</div>).html_safe
      # add nokogiri gem to Gemfile
      form_fields = [
        'textarea',
        'input',
        'select'
      ]
      elements = Nokogiri::HTML::DocumentFragment.parse(html_tag).css "label, " + form_fields.join(', ')
      elements.each do |e|
        if e.node_name.eql? 'label'
          html = %(<div class="control-group error validate-has-error">#{e}</div>).html_safe
        elsif form_fields.include? e.node_name
          if instance.error_message.kind_of?(Array)
            html = %(<div class="control-group error validate-has-error">#{html_tag}<span class="validate-has-error">&nbsp;#{instance.error_message.uniq.join(', ')}</span></div>).html_safe
          else
            html = %(<div class="control-group error validate-has-error">#{html_tag}<span class="validate-has-error">&nbsp;#{instance.error_message}</span></div>).html_safe
          end
        end
      end
      html
    end

  end
end
