Rails.application.routes.draw do

  # Vistas para el divisional

  # get '/altas', to: 'managers#altas', as: 'altas'
  # get '/bajas', to: 'managers#bajas', as: 'bajas'
  get '/dotaciones', to: 'managers#dotaciones', as: 'dotaciones'
  get '/manager', to: 'managers#dotaciones', as: 'manager'
  get '/movimientos', to: 'managers#movimientos', as: 'movimientos'
  get '/formaciones', to: 'managers#formaciones', as: 'formaciones'
  get '/rotaciones', to: 'managers#rotaciones', as: 'rotaciones'
  get '/remuneraciones', to: 'managers#remuneraciones', as: 'remuneraciones'
  get '/vacaciones', to: 'managers#vacaciones', as: 'vacaciones'
  get '/saldo-vacaciones', to: 'managers#saldo', as: 'saldo'
  # get '/personas', to: 'managers#personas', as: 'personas'
  get '/licencias', to: 'managers#licencias', as: 'licencias'
  #post '/entrada', to: 'managers#entrada', as: 'entrada'
  
  devise_scope :user do
    get 'login', to: 'users/sessions#create'
  end
  get '/users/entrada', to: 'managers#entrada', as: 'user_with_params'
  # post '/users/entrada', to: 'managers#entrada', as: 'user_with_params'

  devise_for :users, controllers: {
    sessions: 'users/sessions'
  }
  root 'managers#index'

  namespace :admin do
    post ':model_type/upload', to: 'upload#create', as: :upload
    post ':model_type/remove', to: 'upload#remove', as: :remove
    post '/generate', to: 'upload#generate', as: :generate
    get '/ordenar_gerencias', to: 'vacantes#ordenar_gerencias', as: :ordenar_gerencias
    get '/ordenar_divisiones', to: 'vacantes#ordenar_divisiones', as: :ordenar_divisiones
    post '/crear_orden_gerencias', to: 'vacantes#crear_orden_gerencias', as: :crear_orden_gerencias
    post '/crear_orden_divisiones', to: 'vacantes#crear_orden_divisiones', as: :crear_orden_divisiones
    get '/user/:id/log', to: 'users#log', as: :log_user
    resources :formaciones
    resources :licencias
    resources :empleados
    resources :users
    resources :movimientos
    resources :egresos
    resources :posiciones
    resources :retribuciones
    resources :vacaciones
    resources :vacantes
    resources :user_logs

    root "empleados#index", as: :home
  end

  post "update_role" => "roles#update", as: "update_role"
  resources :users do
    get 'resend_invite', to: "users#resend_invite", as: "resend_invite"
  end

  namespace :api, :defaults => {:format => :json} do
    namespace :v1 do
      post 'upload' => 'upload#create'
      get 'remove' => 'upload#remove'
      get 'generate' => 'upload#generate'
      get 'programa-talentos' => 'programa_talentos#index'
      get 'programa-talentos/:id' => 'programa_talentos#show'
      post 'programa-talentos' => 'programa_talentos#create'
      put 'programa-talentos/:id' => 'programa_talentos#update'
      delete 'programa-talentos/:id' => 'programa_talentos#destroy'
      
      resources :vacantes

      get 'dotaciones-data', to: 'data#dotaciones', as: "dotaciones_data"
      # get 'altas-data' => 'data#altas'
      # get 'bajas-data' => 'data#bajas'
      get 'rotaciones-data', to: 'data#rotaciones', as: "rotaciones_data"
      get 'movimientos-data' => 'data#movimientos', as: "movimientos_data"
      get 'formaciones-data' => 'data#formaciones', as: "formaciones_data"
      get 'remuneraciones-data' => 'data#remuneraciones', as: "remuneraciones_data"
      get 'licencias-data' => 'data#licencias', as: "licencias_data"
      get 'vacaciones-data' => 'data#vacaciones', as: "vacaciones_data"
      get 'saldo-data' => 'data#saldo', as: "saldo_data"

      get 'estructura-data' => 'data#estructura', as: "estructura_data"
      get 'estructura_licencias' => 'data#estructura_licencias', as: "estructura_licencias"

      post 'user_logs', to: 'data#user_logs', as: 'user_logs'
      
    end
  end
end
